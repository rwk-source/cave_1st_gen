/**
 * Author:
 * Rick Wertenbroek
 *
 * Description:
 * For testing inputs on DoDonPachi on the Pynq-Z1 with an input device
 *
 * Compile:
 * gcc joystick.c -o joystick
 *
 * Run:
 * ./joystick /dev/input/...
 *
 */
#include <errno.h>
#include <fcntl.h>
#include <linux/input.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>

#define DDP_BASE_ADDR 0x43C10000
#define DDP_JOY_OFFSET 0x1C

/**
 * Reads a an input event from an input device.
 */
int read_event(int fd, struct input_event *event)
{
    ssize_t bytes;

    bytes = read(fd, event, sizeof(*event));

    if (bytes == sizeof(*event))
        return 0;

    /* Error, could not read full event. */
    printf("Failed reading event bytes read : %d\n", bytes);
    return -1;
}

int main(int argc, char *argv[])
{
    const char *device;
    int js;
    struct input_event event;

    int top = 0;
    int bottom = 0;
    int left = 0;
    int right = 0;
    int b1 = 0;
    int b2 = 0;
    int b3 = 0;
    int s1 = 0;
    int c1 = 0;

    int input_reg = 0;
    int input_reg_old = 0;

    int fd = open("/dev/mem", O_RDWR);
    if (fd < 0) {
        printf("Failed to open /dev/mem\n");
        return -1;
    }

    void* ptr = mmap(NULL, 4096, PROT_READ|PROT_WRITE,
                     MAP_SHARED, fd, DDP_BASE_ADDR);
    volatile uint32_t* joystick_reg = (uint32_t*)(ptr + DDP_JOY_OFFSET);

    if (ptr == NULL) {
        printf("mmap() failed\n");
        return -1;
    }

    if (argc > 1) {
        device = argv[1];
    } else {
        perror("Usage is : ./joystick </dev entry>");
        return -1;
    }

    js = open(device, O_RDONLY);

    if (js == -1)
        perror("Could not open joystick");

    /* This loop will exit if the controller is unplugged. */
    while (read_event(js, &event) == 0)
    {
        switch (event.type)
        {
            case EV_KEY:
                //printf("Button %u %s\n", event.code, event.value ? "pressed" : "released");
                if (event.code == BTN_TOP)
                    b1 = event.value ? 1 : 0;
                if (event.code == BTN_THUMB2)
                    b2 = event.value ? 1 : 0;
                if (event.code == BTN_THUMB)
                    b3 = event.value ? 1 : 0;
                if (event.code == BTN_BASE3)
                    c1 = event.value ? 1 : 0;
                if (event.code == BTN_BASE4)
                    s1 = event.value ? 1 : 0;
                break;
            case EV_ABS:
                if (event.code == ABS_X) {
                    if (event.value == 0) {
                        right = 0;
                        left  = 1;
                    } else if (event.value == 255) {
                        right = 1;
                        left  = 0;
                    } else if (event.value = 127) {
                        right = 0;
                        left  = 0;
                    }
                }
                if (event.code == ABS_Y) {
                    if (event.value == 0) {
                        top    = 1;
                        bottom = 0;
                    } else if (event.value == 255) {
                        top    = 0;
                        bottom = 1;
                    } else if (event.value = 127) {
                        top    = 0;
                        bottom = 0;
                    }
                }
                //printf("Top : %d, Bottom : %d, Right : %d, Left : %d\n", top, bottom, right, left);
            break;
            default:
                /* Ignore init events. */
                break;
        }

        input_reg_old = input_reg;
        input_reg = 0 | top | (bottom << 1) | (left << 2) | (right << 3) | (b1 << 4) | (b2 << 5) | (b3 << 6) | (s1 << 7) | (c1 << 8);

        if (input_reg != input_reg_old) {
            //printf ("Inputs changed to : %04X\n", input_reg);
            *joystick_reg = input_reg;
        }

    }

    munmap(ptr, 4096);
    close(js);
    return 0;
}
