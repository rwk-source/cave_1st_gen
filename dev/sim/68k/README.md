# Simulation of dodonpachi code running on TG68k core

This is a simulation running the 68k CPU with the program ROM attached as well
as all the necessary RAM chips and registers.

## Requirements
* This simulation requires the files `b1.u27` and `b2.u26` in this directory. Theses
files are the binary dumps of the Dodonpachi program roms.    
**I do not own these files.**    
You can use any 68k compiled binary code as long as the MSB byte is in b1
and the LSB byte is in b2.
* Modelsim / Questasim (`vsim`)

## Launching the simulation
Once the two files `b1.u27` and `b2.u26` have been copied in this directory launch
the `runsim.sh` script. This will generate the necessary files if not present
and launch the simulation.

## Evaluating results
You can compare the results of the core running the code with
[MAME](http://mamedev.org/) running in debug mode.

```
./mame64 -debug ddonpach
```

## Known Issues / TODO
- There are no interrupt lines connected yet. So the CPU will not run the interrupt
  routines (e.g., Sound/Graphic interrupts).
- Dodonpachi ROM will access out of memory map regions e.g., 0x5FFF80 and since
  there is no ack (dtack) on the memory bus the processor will wait indefinitely.
