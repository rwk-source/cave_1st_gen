#!/bin/bash

# Copyright (c) 2019 Rick Wertenbroek

SCRIPT_LOCATION=$(realpath $(dirname "${BASH_SOURCE[0]}"))
ROM_LOCATION=$(realpath $SCRIPT_LOCATION/../../../roms)

cd $ROM_LOCATION

if [ ! -f b1.txt ]
then
    echo "Generating b1.txt from b1.u27"
    if [ -f b1.u27 ]
    then
       xxd -c 1 -g 1 b1.u27 | cut -d " " -f 2 > b1.txt
    else
        echo "ERROR : b1.u27 is missing !"
        exit 1
    fi
fi

if [ ! -f b2.txt ]
then
   echo "Generating b2.txt from b2.u26"
   if [ -f b2.u26 ]
   then
       xxd -c 1 -g 1 b2.u26 | cut -d " " -f 2 > b2.txt
   else
       echo "ERROR : b2.u26 is missing !"
       exit 1
   fi
fi

cd $SCRIPT_LOCATION

ln -s $ROM_LOCATION/b1.txt -f
ln -s $ROM_LOCATION/b2.txt -f

if ! [ -x "$(command -v vsim)" ]
then
    echo 'Error: vsim is not installed !' >&2
    exit 1
fi

vsim -do sim.do
