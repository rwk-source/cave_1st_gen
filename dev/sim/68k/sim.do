# Packages
vcom -work work ../../src_vhdl/log_pkg.vhd

# Entities
vcom -work work ../../src_vhdl/68k/TG68_fast.vhd
vcom -work work ../../src_vhdl/68k/TG68.vhd

# Simulation Entities
vcom -work work -2008 ../../src_tb/68k/memory_from_file.vhd
vcom -work work -2008 ../../src_tb/68k/ram.vhd
vcom -work work -2008 ../../src_tb/68k/dual_ram.vhd

# Test Bench
vcom -work work -2008 ../../src_tb/68k/TG68_tb.vhd

#vsim -t 1ns -novopt TG68_tb
# Disabling optimizations would render this simulation extremly slow !
vsim -t 1ns TG68_tb
add wave sim:/DUT/clk
add wave sim:/DUT/reset
add wave sim:/DUT/data_in
add wave sim:/DUT/IPL
add wave sim:/DUT/dtack
add wave sim:/DUT/addr
add wave sim:/DUT/data_out
add wave sim:/DUT/as
add wave sim:/DUT/uds
add wave sim:/DUT/lds
add wave sim:/DUT/rw
#add wave sim:/DUT/*
add wave sim:/read_memory_s
add wave sim:/write_memory_s
add wave sim:/low_s
add wave sim:/high_s
add wave sim:/*_enable_s
add wave sim:/DUT/TG68_fast_inst/regfile_low
wave refresh

set StdArithNoWarnings 1

run -all
