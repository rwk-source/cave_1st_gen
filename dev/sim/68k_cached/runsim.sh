#!/bin/bash

SCRIPT_LOCATION=$(realpath $(dirname "${BASH_SOURCE[0]}"))
ROM_LOCATION=$(realpath $SCRIPT_LOCATION/../../../roms)

cd $ROM_LOCATION

if [ ! -f b1b2.txt ]
then

    echo "Generating b1b2.txt from b1.txt and b2.txt"

    if [ ! -f b1.txt ]
    then
        echo "Generating b1.txt from b1.u27"
        if [ -f b1.u27 ]
        then
            xxd -c 1 -g 1 b1.u27 | cut -d " " -f 2 > b1.txt
        else
            echo "ERROR : b1.u27 is missing !"
            exit 1
        fi
    fi

    if [ ! -f b2.txt ]
    then
        echo "Generating b2.txt from b2.u26"
        if [ -f b2.u26 ]
        then
            xxd -c 1 -g 1 b2.u26 | cut -d " " -f 2 > b2.txt
        else
            echo "ERROR : b2.u26 is missing !"
            exit 1
        fi
    fi

    # Interleave the two files to make a single memory
    # b2.txt has the LSB and b1.txt has the MSB byte of each word
    paste -d '\n' b2.txt b1.txt > b1b2.txt

fi

cd $SCRIPT_LOCATION

ln -s $ROM_LOCATION/b1b2.txt -f

if ! [ -x "$(command -v vsim)" ]
then
    echo 'Error: vsim is not installed !' >&2
    exit 1
fi

vsim -do sim.do
