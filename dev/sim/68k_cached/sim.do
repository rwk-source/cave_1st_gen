# Packages
vcom -work work ../../src_vhdl/log_pkg.vhd
vcom -work work ../../src_vhdl/DoDonPachi/dodonpachi_pkg.vhd

# Simulation Entities
vcom -work work -2008 ../../src_tb/68k_cached/rom_from_file.vhd
vcom -work work -2008 ../../src_tb/68k_cached/clk_dodonpachi_68k.vhd

# IPs
#vlib fifo_generator_v13_2_1
#vmap fifo_generator_v13_2_1 fifo_generator_v13_2_1
vlog -64 -incr -work work ../../src_tb/xilinx/fifo_generator_vlog_beh.v
vlog -64 -work work ../../src_tb/xilinx/fifo_generator_v13_2_rfs.v
vlog -64 -incr -work work ../../src_tb/xilinx/sprite_tile_fifo.v

# 68k processor
vcom -work work ../../src_vhdl/68k/TG68_fast.vhd
vcom -work work ../../src_vhdl/68k/TG68.vhd

# Entities
vcom -work work -2008 ../../src_vhdl/simple_bram.vhd
vcom -work work -2008 ../../src_vhdl/true_dual_port_bram.vhd
vcom -work work -2008 ../../src_vhdl/data_freezer.vhd
vcom -work work -2008 ../../src_vhdl/Cache_Memory/cache_memory.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/main_ram.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/dual_port_ram.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/sprite_blitter_pipeline.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/sprite_processor.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/graphic_processor.vhd
vcom -work work -2008 ../../src_vhdl/eeprom_93c46.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/dodonpachi.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/dodonpachi_top.vhd

# Test Bench
vcom -work work -2008 ../../src_tb/68k_cached/dodonpachi_top_tb.vhd

#vsim -t 100ps -novopt dodonpachi_top_tb
# Disabling optimizations would render this simulation extremly slow !

#vopt -O5 dodonpachi_top_tb -o ddp_opt_top_tb
#vsim -t 100ps ddp_opt_top_tb

vsim -t 100ps dodonpachi_top_tb
#add wave sim:/*
add wave \
sim:/dodonpachi_top_tb/DUT/dodonpachi/main_68k/data_in \
sim:/dodonpachi_top_tb/DUT/dodonpachi/main_68k/addr \
sim:/dodonpachi_top_tb/DUT/dodonpachi/main_68k/data_out \
sim:/dodonpachi_top_tb/DUT/dodonpachi/main_68k/as_s \
sim:/dodonpachi_top_tb/DUT/dodonpachi/main_68k/uds_s \
sim:/dodonpachi_top_tb/DUT/dodonpachi/main_68k/lds_s \
sim:/dodonpachi_top_tb/DUT/dodonpachi/main_68k/rw_s
add wave \
sim:/dodonpachi_top_tb/DUT/clk_68k_s \
sim:/dodonpachi_top_tb/DUT/dodonpachi/addr_68k_s \
sim:/dodonpachi_top_tb/DUT/dodonpachi/n_rst_68k_s \
sim:/dodonpachi_top_tb/DUT/dodonpachi/memory_bus_data_s \
sim:/dodonpachi_top_tb/DUT/dodonpachi/n_data_ack_68k_s \
sim:/dodonpachi_top_tb/DUT/dodonpachi/n_addr_strobe_68k_s \
sim:/dodonpachi_top_tb/DUT/dodonpachi/read_n_write_68k_s

set StdArithNoWarnings 1
set NumericStdNoWarnings 1
#add wave sim:/DUT/dodonpachi/*
run 1000 ms

##add wave -position insertpoint  \
#sim:/dodonpachi_top_tb/DUT/dodonpachi/main_68k/TG68_fast_inst/regfile_low \
#sim:/dodonpachi_top_tb/DUT/dodonpachi/main_68k/TG68_fast_inst/regfile_high

add wave sim:/DUT/dodonpachi/*

add wave -position insertpoint  \
sim:/dodonpachi_top_tb/DUT/cache_memory/req_counter_o \
sim:/dodonpachi_top_tb/DUT/cache_memory/miss_counter_o

set StdArithNoWarnings 1
set NumericStdNoWarnings 1

run -all
