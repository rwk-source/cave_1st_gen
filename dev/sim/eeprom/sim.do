# DUT
vcom -work work -2008 ../../src_vhdl/eeprom_93c46.vhd

# TB
vlog -sv ../../src_tb/eeprom/eeprom_tb.sv

vsim -voptargs=+acc work.eeprom_tb

# Add waves
#add wave -position insertpoint sim:/eeprom_tb/dut_eeprom/*
add wave -r -position insertpoint sim:/eeprom_tb/*

# Run
run -all