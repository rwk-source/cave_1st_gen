# Packages
vcom -work work ../../src_vhdl/log_pkg.vhd
vcom -work work ../../src_vhdl/DoDonPachi/dodonpachi_pkg.vhd

# Simulation Entities
vcom -work work -2008 ../../src_tb/68k_cached/rom_from_file.vhd
vcom -work work -2008 ../../src_tb/graphic_processor/sprite_rom.vhd

# IPs
#vlib fifo_generator_v13_2_1
#vmap fifo_generator_v13_2_1 fifo_generator_v13_2_1
vlog -64 -incr -work work ../../src_tb/xilinx/fifo_generator_vlog_beh.v
#vcom -64 -93 -work work ../../src_tb/xilinx/fifo_generator_v13_2_rfs.vhd
vlog -64 -work work ../../src_tb/xilinx/fifo_generator_v13_2_rfs.v
vlog -64 -incr -work work ../../src_tb/xilinx/sprite_tile_fifo.v

# Entities
vcom -work work -2008 ../../src_vhdl/DoDonPachi/sprite_blitter_pipeline.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/sprite_processor.vhd

# Test Bench
vcom -work work -2008 ../../src_tb/graphic_processor/sprite_processor_tb.vhd

# Simulation
vsim -t 10ns -novopt sprite_processor_tb

# Waves
add wave sim:/*
add wave -noupdate -divider -height 28 Sprite_Processor
add wave sim:/DUT/*
add wave -noupdate -divider -height 28 Sprite_Blitter_Pipeline
add wave sim:/DUT/sprite_blitter_pipeline_inst/*

# very fat wave
#add wave sim:/sprite_processor_tb/frame_buffer_block/frame_buffer_s

# Run
run -all
