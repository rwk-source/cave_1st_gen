# Packages
vcom -work work ../../src_vhdl/log_pkg.vhd
vcom -work work ../../src_vhdl/DoDonPachi/dodonpachi_pkg.vhd

# Simulation Entities
vcom -work work -2008 ../../src_tb/68k_cached/rom_from_file.vhd
vcom -work work -2008 ../../src_tb/graphic_processor/layer_rom.vhd

# IPs
#vlib fifo_generator_v13_2_1
#vmap fifo_generator_v13_2_1 fifo_generator_v13_2_1
vlog -64 -incr -work work ../../src_tb/xilinx/fifo_generator_vlog_beh.v
#vcom -64 -93 -work work ../../src_tb/xilinx/fifo_generator_v13_2_rfs.vhd
vlog -64 -work work ../../src_tb/xilinx/fifo_generator_v13_2_rfs.v
vlog -64 -incr -work work ../../src_tb/xilinx/sprite_tile_fifo.v
vlog -64 -work work ../../src_tb/xilinx/blk_mem_gen_v8_4.v
vlog -64 -incr -work work ../../src_tb/xilinx/sim_priority_ram.v

# Entities
vcom -work work -2008 ../../src_vhdl/true_dual_port_bram.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/sprite_blitter_pipeline.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/sprite_processor.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/layer_pipeline.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/layer_processor.vhd
vcom -work work -2008 ../../src_vhdl/DoDonPachi/graphic_processor.vhd

# Test Bench
vcom -work work -2008 ../../src_tb/graphic_processor/graphic_processor_tb.vhd

# Simulation
vsim -t 10ns -novopt graphic_processor_tb

# Waves
#add wave sim:/*
do wave_gpu.do
#add wave -noupdate -divider -height 28 Layer_Processor
#add wave sim:/layer_processor_tb/dut_block/DUT/*
#add wave -noupdate -divider -height 28 Layer_Pipeline
#add wave sim:/layer_processor_tb/dut_block/DUT/layer_pipeline_inst/*

# very fat wave
#add wave sim:/sprite_processor_tb/frame_buffer_block/frame_buffer_s

# Run
run -all
