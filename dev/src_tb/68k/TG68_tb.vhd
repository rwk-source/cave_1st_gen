-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- Title      : Testbench for design "TG68"
-- Project    : DoDonPachi FPGA
-------------------------------------------------------------------------------
-- File       : TG68_tb.vhd
-- Author     : Rick Wertenbroek
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2019
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity TG68_tb is

end entity TG68_tb;

-------------------------------------------------------------------------------

architecture tb of TG68_tb is

    -- component ports
    signal clk        : std_logic;
    signal reset      : std_logic;
    signal clkena_in  : std_logic                    := '1';
    signal data_in    : std_logic_vector(15 downto 0);
    signal IPL        : std_logic_vector(2 downto 0) := "111";
    signal dtack      : std_logic;
    signal addr       : std_logic_vector(31 downto 0);
    signal data_out   : std_logic_vector(15 downto 0);
    signal as         : std_logic;
    signal uds        : std_logic;
    signal lds        : std_logic;
    signal rw         : std_logic;
    signal drive_data : std_logic;

    signal read_memory_s            : std_logic;
    signal write_memory_s           : std_logic;
    signal old_as_s                 : std_logic;
    signal as_strobe_s              : std_logic;
    signal memory_bus_ack_s         : std_logic;
    -- ROM
    constant ROM_LOG_SIZE_C         : natural := 20;  -- 1MB
    signal rom_low_ack_s            : std_logic;
    signal rom_high_ack_s           : std_logic;
    signal rom_data_o_s             : std_logic_vector(15 downto 0);
    signal rom_enable_s             : std_logic;
    -- RAM
    constant RAM_LOG_SIZE_C         : natural := 16;  -- 64kB
    signal ram_enable_s             : std_logic;
    signal ram_ack_s                : std_logic;
    signal ram_data_o_s             : std_logic_vector(15 downto 0);
    -- YMZ RAM
    constant YMZ_RAM_LOG_SIZE_C     : natural := 2;   -- 4B
    signal ymz_ram_enable_s         : std_logic;
    signal ymz_ram_ack_s            : std_logic;
    signal ymz_ram_data_o_s         : std_logic_vector(15 downto 0);
    -- Sprite RAM
    constant SPRITE_RAM_LOG_SIZE_C  : natural := 16;  -- 64kB
    signal sprite_ram_enable_s      : std_logic;
    signal sprite_ram_ack_s         : std_logic;
    signal sprite_ram_data_o_s      : std_logic_vector(15 downto 0);
    -- Layer 0 RAM
    constant LAYER_0_RAM_LOG_SIZE_C : natural := 15;  -- 32kB
    signal layer_0_ram_enable_s     : std_logic;
    signal layer_0_ram_ack_s        : std_logic;
    signal layer_0_ram_data_o_s     : std_logic_vector(15 downto 0);
    -- Layer 1 RAM
    constant LAYER_1_RAM_LOG_SIZE_C : natural := 15;  -- 32kB
    signal layer_1_ram_enable_s     : std_logic;
    signal layer_1_ram_ack_s        : std_logic;
    signal layer_1_ram_data_o_s     : std_logic_vector(15 downto 0);
    -- Layer 2 RAM
    constant LAYER_2_RAM_LOG_SIZE_C : natural := 16;  -- 32kB
    signal layer_2_ram_enable_s     : std_logic;
    signal layer_2_ram_ack_s        : std_logic;
    signal layer_2_ram_data_o_s     : std_logic_vector(15 downto 0);
    -- Video Registers (should not necessarily be ram) (write only)
    constant VIDEO_REGS_LOG_SIZE_C  : natural := 7;   -- 128B
    signal video_regs_enable_s      : std_logic;
    signal video_regs_ack_s         : std_logic;
    -- IRQ Cause (read only)
    signal irq_cause_enable_s       : std_logic;
    signal irq_cause_ack_s          : std_logic;
    signal irq_cause_data_o_s       : std_logic_vector(15 downto 0);
    -- Video Control Registers 0
    constant V_CTRL_0_LOG_SIZE_C    : natural := 3;   -- 8B (actually 6B)
    signal v_ctrl_0_enable_s        : std_logic;
    signal v_ctrl_0_ack_s           : std_logic;
    signal v_ctrl_0_data_o_s        : std_logic_vector(15 downto 0);
    -- Video Control Registers 1
    constant V_CTRL_1_LOG_SIZE_C    : natural := 3;   -- 8B (actually 6B)
    signal v_ctrl_1_enable_s        : std_logic;
    signal v_ctrl_1_ack_s           : std_logic;
    signal v_ctrl_1_data_o_s        : std_logic_vector(15 downto 0);
    -- Video Control Registers 2
    constant V_CTRL_2_LOG_SIZE_C    : natural := 3;   -- 8B (actually 6B)
    signal v_ctrl_2_enable_s        : std_logic;
    signal v_ctrl_2_ack_s           : std_logic;
    signal v_ctrl_2_data_o_s        : std_logic_vector(15 downto 0);
    -- Palette RAM
    constant PAL_RAM_LOG_SIZE_C     : natural := 16;  -- 64kB
    signal pal_ram_enable_s         : std_logic;
    signal pal_ram_ack_s            : std_logic;
    signal pal_ram_data_o_s         : std_logic_vector(15 downto 0);
    -- Inputs 0
    constant IN_0_LOG_SIZE_C        : natural := 1;   -- 2B
    signal in_0_enable_s            : std_logic;
    signal in_0_ack_s               : std_logic;
    signal in_0_data_o_s            : std_logic_vector(15 downto 0);
    -- Inputs 1
    constant IN_1_LOG_SIZE_C        : natural := 1;   -- 2B
    signal in_1_enable_s            : std_logic;
    signal in_1_ack_s               : std_logic;
    signal in_1_data_o_s            : std_logic_vector(15 downto 0);
    -- EEPROM
    constant EEPROM_LOG_SIZE_C      : natural := 1;   -- 2B
    signal eeprom_enable_s          : std_logic;
    signal eeprom_ack_s             : std_logic;
    signal eeprom_data_o_s          : std_logic_vector(15 downto 0);

    signal low_s, high_s : std_logic;

    -- clock
    signal clk_sim : std_logic := '1';

begin  -- architecture tb

    -- process(clk) is
    -- begin
    --     if rising_edge(clk) then
    --         old_as_s <= as;
    --     end if;
    -- end process;

    -- Positive logic (Upper Data Strobe and Lower Data Strobe are negative logic)
    high_s <= not uds;
    low_s  <= not lds;

    -- Decode bus signals

    -- ROM
    rom_enable_s <= '1' when addr(31 downto ROM_LOG_SIZE_C) = x"000" else
                    '0';
    -- RAM
    ram_enable_s <= '1' when addr(31 downto RAM_LOG_SIZE_C) = x"0010" else
                    '0';
    -- YMZ RAM
    ymz_ram_enable_s <= '1' when addr(31 downto YMZ_RAM_LOG_SIZE_C) = x"0030000" & "00" else
                        '0';
    -- Sprite RAM
    sprite_ram_enable_s <= '1' when addr(31 downto SPRITE_RAM_LOG_SIZE_C) = x"0040" else
                           '0';
    -- Layer 0 RAM
    layer_0_ram_enable_s <= '1' when addr(31 downto LAYER_0_RAM_LOG_SIZE_C) = x"0050" & "0" else
                            '0';
    -- Layer 1 RAM
    layer_1_ram_enable_s <= '1' when addr(31 downto LAYER_1_RAM_LOG_SIZE_C) = x"0060" & "0" else
                            '0';
    -- Layer 2 RAM
    layer_2_ram_enable_s <= '1' when addr(31 downto LAYER_2_RAM_LOG_SIZE_C) = x"0070" else
                            '0';
    -- Video Registers (should not necessarily be ram) (maybe remove read/write
    -- check redundancy).
    video_regs_enable_s <= '1' when (addr(31 downto VIDEO_REGS_LOG_SIZE_C) = x"008000" & "0") and (read_memory_s = '0') else
                           '0';
    -- IRQ Cause (same about redundancy)
    irq_cause_enable_s <= '1' when (addr(31 downto 3) = x"0080000" & "0") and (read_memory_s = '1') else
                          '0';
    -- Video Control Registers 0
    v_ctrl_0_enable_s <= '1' when addr(31 downto V_CTRL_0_LOG_SIZE_C) = x"0090000" & "0" else
                         '0';
    -- Video Control Registers 1
    v_ctrl_1_enable_s <= '1' when addr(31 downto V_CTRL_1_LOG_SIZE_C) = x"00a0000" & "0" else
                         '0';
    -- Video Control Registers 2
    v_ctrl_2_enable_s <= '1' when addr(31 downto V_CTRL_2_LOG_SIZE_C) = x"00b0000" & "0" else
                         '0';
    -- Palette RAM
    pal_ram_enable_s <= '1' when addr(31 downto PAL_RAM_LOG_SIZE_C) = x"00c0" else
                        '0';
    -- Inputs 0
    in_0_enable_s <= '1' when addr(31 downto IN_0_LOG_SIZE_C) = x"00d0000" & "000" else
                     '0';
    -- Inputs 1
    in_1_enable_s <= '1' when addr(31 downto IN_1_LOG_SIZE_C) = x"00d0000" & "001" else
                     '0';
    -- EEPROM
    eeprom_enable_s <= '1' when addr(31 downto EEPROM_LOG_SIZE_C) = x"00e0000" & "000" else
                       '0';

    -- Operation on falling edge of as (address strobe)
    -- as_strobe_s <= '1' when (old_as_s = '1') and (as = '0') else
    --                '0';

    -- read_memory_s <= '1' when (as_strobe_s = '1') and (rw = '1') else
    --                  '0';

    -- write_memory_s <= '1' when (as_strobe_s = '1') and (rw = '0') else
    --                   '0';

    read_memory_s <= '1' when (as = '0') and (rw = '1') else
                     '0';

    write_memory_s <= '1' when (as = '0') and (rw = '0') else
                      '0';

    memory_bus_ack_s <= rom_low_ack_s or
                        rom_high_ack_s or
                        ram_ack_s or
                        ymz_ram_ack_s or
                        sprite_ram_ack_s or
                        layer_0_ram_ack_s or
                        layer_1_ram_ack_s or
                        layer_2_ram_ack_s or
                        video_regs_ack_s or
                        irq_cause_ack_s or
                        v_ctrl_0_ack_s or
                        v_ctrl_1_ack_s or
                        v_ctrl_2_ack_s or
                        pal_ram_ack_s or
                        in_0_ack_s or
                        in_1_ack_s or
                        eeprom_ack_s;

    -- OR'ed bus
    data_in <= rom_data_o_s or
               ram_data_o_s or
               irq_cause_data_o_s;      -- or ...

    -- Data ack (active low, should maybe not be orred when uds and lds will be
    -- used).
    dtack <= not memory_bus_ack_s;


    ------------------------------
    -- Memory Mapped Components --
    ------------------------------

    -- ROM
    memory_from_file_1 : entity work.memory_from_file
        generic map (
            FILE_G            => "b2.txt",
            MEMORY_LOG_SIZE_G => ROM_LOG_SIZE_C-1)  -- 512k
        port map (
            clk_i    => clk,
            addr_i   => addr(ROM_LOG_SIZE_C-1 downto 1),
            data_o   => rom_data_o_s(7 downto 0),
            enable_i => rom_enable_s,
            read_i   => read_memory_s,
            ack_o    => rom_low_ack_s);

    memory_from_file_2 : entity work.memory_from_file
        generic map (
            FILE_G            => "b1.txt",
            MEMORY_LOG_SIZE_G => ROM_LOG_SIZE_C-1)  -- 512k
        port map (
            clk_i    => clk,
            addr_i   => addr(ROM_LOG_SIZE_C-1 downto 1),
            data_o   => rom_data_o_s(15 downto 8),
            enable_i => rom_enable_s,
            read_i   => read_memory_s,
            ack_o    => rom_high_ack_s);

    -- RAM
    ram_1 : entity work.ram
        generic map (
            MEMORY_LOG_SIZE_G => RAM_LOG_SIZE_C)
        port map (
            clk_i    => clk,
            enable_i => ram_enable_s,
            addr_i   => addr(RAM_LOG_SIZE_C-1 downto 0),
            data_o   => ram_data_o_s,
            read_i   => read_memory_s,
            data_i   => data_out,
            write_i  => write_memory_s,
            high_i   => high_s,
            low_i    => low_s,
            ack_o    => ram_ack_s);

    -- YMZ RAM
    ram_YMZ : entity work.ram
        generic map (
            MEMORY_LOG_SIZE_G => YMZ_RAM_LOG_SIZE_C)
        port map (
            clk_i    => clk,
            enable_i => ymz_ram_enable_s,
            addr_i   => addr(YMZ_RAM_LOG_SIZE_C-1 downto 0),
            data_o   => ymz_ram_data_o_s,
            read_i   => read_memory_s,
            data_i   => data_out,
            write_i  => write_memory_s,
            high_i   => high_s,
            low_i    => low_s,
            ack_o    => ymz_ram_ack_s);

    -- Sprite RAM
    sprite_ram_1 : entity work.dual_ram
        generic map (
            MEMORY_LOG_SIZE_G => SPRITE_RAM_LOG_SIZE_C)
        port map (
            clk_i      => clk,
            enable_i   => sprite_ram_enable_s,
            addr_i     => addr(SPRITE_RAM_LOG_SIZE_C-1 downto 0),
            data_o     => sprite_ram_data_o_s,
            read_i     => read_memory_s,
            data_i     => data_out,
            write_i    => write_memory_s,
            high_i     => high_s,
            low_i      => low_s,
            ack_o      => sprite_ram_ack_s,
            smi_addr_i => (others => '0'),
            smi_read_i => '0',
            smi_data_o => open);

    -- Layer 0 RAM
    layer_0_ram_1 : entity work.dual_ram
        generic map (
            MEMORY_LOG_SIZE_G => LAYER_0_RAM_LOG_SIZE_C)
        port map (
            clk_i      => clk,
            enable_i   => layer_0_ram_enable_s,
            addr_i     => addr(LAYER_0_RAM_LOG_SIZE_C-1 downto 0),
            data_o     => layer_0_ram_data_o_s,
            read_i     => read_memory_s,
            data_i     => data_out,
            write_i    => write_memory_s,
            high_i     => high_s,
            low_i      => low_s,
            ack_o      => layer_0_ram_ack_s,
            smi_addr_i => (others => '0'),
            smi_read_i => '0',
            smi_data_o => open);

    -- Layer 1 RAM
    layer_1_ram_1 : entity work.dual_ram
        generic map (
            MEMORY_LOG_SIZE_G => LAYER_1_RAM_LOG_SIZE_C)
        port map (
            clk_i      => clk,
            enable_i   => layer_1_ram_enable_s,
            addr_i     => addr(LAYER_1_RAM_LOG_SIZE_C-1 downto 0),
            data_o     => layer_1_ram_data_o_s,
            read_i     => read_memory_s,
            data_i     => data_out,
            write_i    => write_memory_s,
            high_i     => high_s,
            low_i      => low_s,
            ack_o      => layer_1_ram_ack_s,
            smi_addr_i => (others => '0'),
            smi_read_i => '0',
            smi_data_o => open);

    -- Layer 2 RAM
    layer_2_ram_1 : entity work.dual_ram
        generic map (
            MEMORY_LOG_SIZE_G => LAYER_2_RAM_LOG_SIZE_C)
        port map (
            clk_i      => clk,
            enable_i   => layer_2_ram_enable_s,
            addr_i     => addr(LAYER_2_RAM_LOG_SIZE_C-1 downto 0),
            data_o     => layer_2_ram_data_o_s,
            read_i     => read_memory_s,
            data_i     => data_out,
            write_i    => write_memory_s,
            high_i     => high_s,
            low_i      => low_s,
            ack_o      => layer_2_ram_ack_s,
            smi_addr_i => (others => '0'),
            smi_read_i => '0',
            smi_data_o => open);

    -- Video Registers (should be dual ported ram) + write only
    video_regs_ram_1 : entity work.ram
        generic map (
            MEMORY_LOG_SIZE_G => VIDEO_REGS_LOG_SIZE_C)
        port map (
            clk_i    => clk,
            enable_i => video_regs_enable_s,
            addr_i   => addr(VIDEO_REGS_LOG_SIZE_C-1 downto 0),
            data_o   => open,
            read_i   => read_memory_s,
            data_i   => data_out,
            write_i  => write_memory_s,
            high_i   => high_s,
            low_i    => low_s,
            ack_o    => video_regs_ack_s);

    -- IRQ Cause
    irq_cause_process : process(clk) is
    begin
        if rising_edge(clk) then
            irq_cause_ack_s <= '0';
            if irq_cause_enable_s = '1' then
                if read_memory_s = '1' then
                    irq_cause_data_o_s <= x"0003";
                    irq_cause_ack_s    <= '1';
                end if;
            else
                irq_cause_data_o_s <= (others => '0');
            end if;  -- Enable
        end if;  -- Rising Edge Clock
    end process irq_cause_process;

    -- Video Control 0 (should be dual ported ram)
    v_ctrl_0_ram_1 : entity work.ram
        generic map (
            MEMORY_LOG_SIZE_G => V_CTRL_0_LOG_SIZE_C)
        port map (
            clk_i    => clk,
            enable_i => v_ctrl_0_enable_s,
            addr_i   => addr(V_CTRL_0_LOG_SIZE_C-1 downto 0),
            data_o   => v_ctrl_0_data_o_s,
            read_i   => read_memory_s,
            data_i   => data_out,
            write_i  => write_memory_s,
            high_i   => high_s,
            low_i    => low_s,
            ack_o    => v_ctrl_0_ack_s);

    -- Video Control 1 (should be dual ported ram)
    v_ctrl_1_ram_1 : entity work.ram
        generic map (
            MEMORY_LOG_SIZE_G => V_CTRL_1_LOG_SIZE_C)
        port map (
            clk_i    => clk,
            enable_i => v_ctrl_1_enable_s,
            addr_i   => addr(V_CTRL_1_LOG_SIZE_C-1 downto 0),
            data_o   => v_ctrl_1_data_o_s,
            read_i   => read_memory_s,
            data_i   => data_out,
            write_i  => write_memory_s,
            high_i   => high_s,
            low_i    => low_s,
            ack_o    => v_ctrl_1_ack_s);

    -- Video Control 2 (should be dual ported ram)
    v_ctrl_2_ram_1 : entity work.ram
        generic map (
            MEMORY_LOG_SIZE_G => V_CTRL_2_LOG_SIZE_C)
        port map (
            clk_i    => clk,
            enable_i => v_ctrl_2_enable_s,
            addr_i   => addr(V_CTRL_2_LOG_SIZE_C-1 downto 0),
            data_o   => v_ctrl_2_data_o_s,
            read_i   => read_memory_s,
            data_i   => data_out,
            write_i  => write_memory_s,
            high_i   => high_s,
            low_i    => low_s,
            ack_o    => v_ctrl_2_ack_s);

    -- Palette RAM (should be dual ported ram)
    palette_ram_1 : entity work.ram
        generic map (
            MEMORY_LOG_SIZE_G => PAL_RAM_LOG_SIZE_C)
        port map (
            clk_i    => clk,
            enable_i => pal_ram_enable_s,
            addr_i   => addr(PAL_RAM_LOG_SIZE_C-1 downto 0),
            data_o   => pal_ram_data_o_s,
            read_i   => read_memory_s,
            data_i   => data_out,
            write_i  => write_memory_s,
            high_i   => high_s,
            low_i    => low_s,
            ack_o    => pal_ram_ack_s);

    -- Input Ports
    in_process : process(clk) is
    begin
        if rising_edge(clk) then
            in_0_ack_s <= '0';
            in_1_ack_s <= '0';
            if read_memory_s = '1' then
                if in_0_enable_s = '1' then
                    in_0_data_o_s <= x"FFFF";  -- Active low
                    in_0_ack_s    <= '1';
                end if;
                if in_1_enable_s = '1' then
                    in_1_data_o_s <= x"FFFF";  -- Active low
                    in_1_ack_s    <= '1';
                end if;
            else
                in_0_data_o_s <= (others => '0');
                in_1_data_o_s <= (others => '0');
            end if;  -- Enable
        end if;  -- Rising Edge Clock
    end process in_process;

    -- EEPROM
    eeprom_process : process(clk) is
    begin
        if rising_edge(clk) then
            eeprom_ack_s <= '0';
            if eeprom_enable_s = '1' then
                if read_memory_s = '1' then
                    eeprom_data_o_s <= x"0000";
                    eeprom_ack_s    <= '1';
                end if;
            else
                eeprom_data_o_s <= (others => '0');
            end if;  -- Enable
        end if;  -- Rising Edge Clock
    end process eeprom_process;

    --------------------
    -- Main Processor --
    --------------------

    DUT : entity work.TG68
        port map (
            clk        => clk,
            reset      => reset,
            --clkena_in  => clkena_in,
            data_in    => data_in,
            --IPL        => IPL,
            dtack      => dtack,
            addr       => addr,
            data_out   => data_out,
            as         => as,
            uds        => uds,
            lds        => lds,
            rw         => rw,
            drive_data => drive_data);

    -- clock generation
    clk_sim <= not clk_sim after 10 ns;
    clk     <= clk_sim;
    reset   <= '0', '1'    after 100 ns;

    -- waveform generation
    WaveGen_Proc : process
    begin
        -- Default values

        wait;
    --wait until clk_sim = '1';
    end process WaveGen_Proc;

end architecture tb;
