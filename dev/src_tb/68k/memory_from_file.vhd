-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : memory_from_file.vhd
-- Description  : A simple memory from a file (each line of the file contains a
--                byte of the real file in hex, two characters). This is a
--                simple way to simulate a ROM.
--                Use the following command to genrate the text file contents :
--                xxd -c 1 -g 1 file.bin | cut -d " " -f 2 > file.txt
--
-- Author       : Rick Wertenbroek
-- Version      : 0.0
--
-- Dependencies : log_pkg.vhd
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use work.log_pkg.all;

entity memory_from_file is
    generic (
        FILE_G            : string;
        MEMORY_LOG_SIZE_G : natural
        );
    port (
        clk_i    : in  std_logic;
        addr_i   : in  std_logic_vector(MEMORY_LOG_SIZE_G-1 downto 0);
        data_o   : out std_logic_vector(7 downto 0);
        enable_i : in  std_logic;
        read_i   : in  std_logic;
        ack_o    : out std_logic
        );
end memory_from_file;

architecture tb of memory_from_file is

    -----------
    -- Types --
    -----------
    type rom_array_t is array ((2**MEMORY_LOG_SIZE_G)-1 downto 0) of std_logic_vector(7 downto 0);

    ---------------
    -- Functions --
    ---------------
    impure function rom_init(filename : string) return rom_array_t is
        file rom_file_f      : text open read_mode is filename;
        variable rom_line_v  : line;
        variable rom_value_v : unsigned(7 downto 0);
        variable temp_v      : rom_array_t;
        variable rom_index_v : natural;
    begin

        temp_v := (others => (others => '0'));

        while (not endfile(rom_file_f) and (rom_index_v < 2**MEMORY_LOG_SIZE_G)) loop
            readline(rom_file_f, rom_line_v);
            hread(rom_line_v, rom_value_v);
            temp_v(rom_index_v) := std_logic_vector(rom_value_v);
            rom_index_v         := rom_index_v + 1;
        end loop;

        return temp_v;
    end function;

    ---------------
    -- Constants --
    ---------------
    constant rom_c : rom_array_t := rom_init(FILE_G);

begin

    process(clk_i) is
    begin
        if rising_edge(clk_i) then
            ack_o <= '0';
            if enable_i = '1' then
                -- Data
                if read_i = '1' then
                    data_o <= rom_c(to_integer(unsigned(addr_i)));
                    -- Assert the ack one cycle after the read
                    ack_o  <= '1';
                end if; -- Read
            else
                data_o <= (others => '0'); -- Or'ed bus
            end if; -- Enable
        end if; -- Rising Edge Clock

    end process;

end architecture tb;
