-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : ram.vhd
-- Description  : RAM that is to be connected on an "OR'ed" bus
--
-- Author       : Rick
-- Version      : 0.0
--
-- Dependencies : log_pkg.vhd
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.log_pkg.all;

entity ram is
    generic (
        MEMORY_LOG_SIZE_G : natural
        );
    port (
        clk_i        : in  std_logic;
        enable_i     : in  std_logic;
        addr_i       : in  std_logic_vector(MEMORY_LOG_SIZE_G-1 downto 0);
        data_o       : out std_logic_vector(15 downto 0);
        read_i       : in  std_logic;
        data_i       : in  std_logic_vector(15 downto 0);
        write_i      : in  std_logic;
        high_i       : in  std_logic;
        low_i        : in  std_logic;
        ack_o        : out std_logic
        );
end ram;

architecture rtl of ram is

    -----------
    -- Types --
    -----------
    type ram_array_t is array ((2**MEMORY_LOG_SIZE_G)-1 downto 0) of std_logic_vector(7 downto 0);

    -------------
    -- Signals --
    -------------
    signal ram_s : ram_array_t;

begin

    process(clk_i) is
    begin
        if rising_edge(clk_i) then
            ack_o  <= '0';
            if enable_i = '1' then
                if write_i = '1' then
                    if high_i = '1' then
                        ram_s(to_integer(unsigned(addr_i)+1)) <= data_i(15 downto 8);
                        ack_o  <= '1';
                    end if;
                    if low_i = '1' then
                        ram_s(to_integer(unsigned(addr_i))) <= data_i(7 downto 0);
                        ack_o  <= '1';
                    end if;

                    data_o <= (others => '0'); -- Orred bus (do not provide
                                               -- data while being written).
                elsif read_i = '1' then
                    -- Warning this could fail if reading the last byte
                    data_o <= ram_s(to_integer(unsigned(addr_i)+1)) & ram_s(to_integer(unsigned(addr_i)));
                    ack_o <= '1';
                end if; -- Write/Read
            else
                data_o <= (others => '0'); -- Orred bus
            end if; -- Enable
        end if; -- Rising Edge Clock

    end process;

end architecture rtl;
