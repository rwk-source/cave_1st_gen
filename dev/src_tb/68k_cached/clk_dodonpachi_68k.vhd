-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : clk_dodonpachi_68k.vhd
-- Description  : Simulates a PLL that outputs 16MHz
--
-- Author       : Rick Wertenbroek
-- Version      : 0.1
--
-- VHDL std     : 2008
-- Dependencies :
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clk_dodonpachi_68k is
    port (
        clk_out1 : out std_logic;
        locked   : out std_logic;
        clk_in1  : in  std_logic
        );
end entity clk_dodonpachi_68k;

architecture behave of clk_dodonpachi_68k is

    constant LOCKED_CYCLES : natural   := 20;

    signal cycle_counter_s : natural   := 0;
    signal clock_16MHz_s   : std_logic := '0';

begin

    process(clk_in1)
    begin
        if rising_edge(clk_in1) then
            if cycle_counter_s < LOCKED_CYCLES then
                cycle_counter_s <= cycle_counter_s + 1;
            end if;
        end if;
    end process;

    clock_16MHz_s <= not clock_16MHz_s after 62500 ps;

    locked <= '0' when cycle_counter_s < LOCKED_CYCLES else
              '1';

    clk_out1 <= clock_16MHz_s when locked = '1' else
                '0';

end behave;
