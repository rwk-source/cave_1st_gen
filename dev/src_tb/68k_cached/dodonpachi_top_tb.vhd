-------------------------------------------------------------------------------
-- Title      : Testbench for design "dodonpachi_top"
-- Project    :
-------------------------------------------------------------------------------
-- File       : dodonpachi_top_tb.vhd
-- Author     : Rick Wertenbroek  <rick@rick-ThinkPad-P50>
-- Company    :
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2019
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.dodonpachi_pkg.all;

-------------------------------------------------------------------------------

entity dodonpachi_top_tb is
    generic (
        LOCK_STEP_G : std_logic := '0'
    );
end entity dodonpachi_top_tb;

-------------------------------------------------------------------------------

architecture tb of dodonpachi_top_tb is

    -------------
    -- Signals --
    -------------
    signal clk_s                 : std_logic := '1';
    signal rst_s                 : std_logic;
    signal rst_68k_s             : std_logic;
    signal rom_addr_68k_cache_s  : std_logic_vector(DDP_ROM_LOG_SIZE_C-1 downto 0);
    signal rom_read_68k_cache_s  : std_logic;
    signal rom_valid_68k_cache_s : std_logic;
    signal rom_data_68k_cache_s  : std_logic_vector(DDP_ROM_CACHE_LINE_WIDTH-1 downto 0);
    signal rom_addr_gfx_s        : std_logic_vector(31 downto 0);
    signal rom_read_gfx_s        : std_logic;
    signal rom_valid_gfx_s       : std_logic;
    signal rom_data_gfx_s        : std_logic_vector(7 downto 0);

    signal step_s                : std_logic;

    signal tg68_pc_obs           : std_logic_vector(31 downto 0);
    signal tg68_pcw_obs          : std_logic;
    signal pc                    : std_logic_vector(31 downto 0);

begin  -- architecture tb

    -- component instantiation
    DUT: entity work.dodonpachi_top
        generic map (
            SIMULATION_G                => true,
            INCLUDE_GRAPHIC_PROCESSOR_G => false) -- For simulation speed
        port map (
            clk_i                 => clk_s,
            rst_i                 => rst_s,
            --
            rst_68k_i             => rst_68k_s,
            --
            rom_addr_68k_cache_o  => rom_addr_68k_cache_s,
            rom_read_68k_cache_o  => rom_read_68k_cache_s,
            rom_valid_68k_cache_i => rom_valid_68k_cache_s,
            rom_data_68k_cache_i  => rom_data_68k_cache_s,
            --
            rom_addr_gfx_o        => open,
            rom_burst_read_gfx_o  => open,
            rom_data_valid_gfx_i  => '0',
            rom_data_gfx_i        => (others => '0'),
            rom_burst_done_gfx_i  => '0',
            --
            frame_buffer_addr_o   => open,
            frame_buffer_data_o   => open,
            frame_buffer_write_o  => open,
            --
            frame_i               => '0',
            --
            start_fb_dma_o        => open,
            --
            lock_step_i           => LOCK_STEP_G,
            step_i                => step_s,
            mem_bus_68k_o         => open,
            TG68_PC_o             => tg68_pc_obs,
            TG68_PCW_o            => tg68_pcw_obs);

    -- Clock generation
    clk_s <= not clk_s after 10 ns; -- 100MHz
    -- Reset
    rst_s <= '1', '0' after 100 ns;
    -- 68k Reset
    rst_68k_s <= '1', '0' after 1000 ns;

    rom_from_file_1: entity work.rom_from_file
        generic map (
            FILE_G                       => "b1b2.txt",
            MEMORY_LOG_SIZE_G            => 15,
            MEMORY_DATA_WIDTH_IN_BYTES_G => DDP_ROM_CACHE_LINE_WIDTH/8,
            LATENCY_G                    => 20)
        port map (
            clk_i   => clk_s,
            addr_i  => rom_addr_68k_cache_s(rom_addr_68k_cache_s'high downto rom_addr_68k_cache_s'length - 15),
            read_i  => rom_read_68k_cache_s,
            data_o  => rom_data_68k_cache_s,
            valid_o => rom_valid_68k_cache_s);


    -- This process shows the execution trace
    process (tg68_pcw_obs)
    begin
        if rising_edge(tg68_pcw_obs) then
            --report "PC : " & to_hstring(tg68_pc_obs);
            pc <= tg68_pc_obs;
        end if;
    end process;

    process (pc)
    begin
        --report "PC : " & to_hstring(pc);
        if pc = x"0005FFFF" then
            report "Error !" severity error;
        end if;
    end process;

    -- Test step mode
    step_mode_process : process
    begin

        -- This will aply stepping to simulate manual steps
        -- This does nothing if stepping is not activated
        step_s <= '0';

        for i in 0 to 100 loop
            for j in 0 to 100 loop
                wait until falling_edge(clk_s);
            end loop;
            step_s <= '1';
            for j in 0 to 100 loop
                wait until falling_edge(clk_s);
            end loop;
            step_s <= '0';
        end loop;

        wait;
    end process step_mode_process;

end architecture tb;
