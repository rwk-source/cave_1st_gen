-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : rom_from_file.vhd
-- Description  : Simulates a ROM from a file.
--                This file is for simulation only.
--
-- Author       : Rick Wertenbroek
-- Version      : 0.1
--
-- VHDL std     : 2008
-- Dependencies : log_pkg.vhd
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use work.log_pkg.all;

entity rom_from_file is
    generic (
        FILE_G                       : string;
        MEMORY_LOG_SIZE_G            : natural;
        MEMORY_DATA_WIDTH_IN_BYTES_G : natural;
        LATENCY_G                    : natural := 1
        );
    port (
        clk_i   : in  std_logic;
        addr_i  : in  std_logic_vector(MEMORY_LOG_SIZE_G-1 downto 0);
        read_i  : in  std_logic;
        data_o  : out std_logic_vector(MEMORY_DATA_WIDTH_IN_BYTES_G*8-1 downto 0);
        valid_o : out std_logic
        );
end rom_from_file;

architecture tb of rom_from_file is

    -----------
    -- Types --
    -----------
    type rom_array_t is array ((2**MEMORY_LOG_SIZE_G)-1 downto 0) of std_logic_vector(MEMORY_DATA_WIDTH_IN_BYTES_G*8-1 downto 0);

    ---------------
    -- Functions --
    ---------------
    impure function rom_init(filename : string) return rom_array_t is
        file rom_file_f        : text open read_mode is filename;
        variable rom_line_v    : line;
        variable rom_value_v   : unsigned(7 downto 0);
        variable temp_v        : rom_array_t;
        variable file_index_v  : natural := 0;
        variable rom_index_v   : natural := 0;
        variable small_index_v : natural := 0;
    begin

        temp_v := (others => (others => '0'));

        while (not endfile(rom_file_f) and (rom_index_v < 2**MEMORY_LOG_SIZE_G)) loop

            readline(rom_file_f, rom_line_v);
            hread(rom_line_v, rom_value_v);
            temp_v(rom_index_v)((small_index_v+1)*8-1 downto small_index_v*8) := std_logic_vector(rom_value_v);

            file_index_v  := file_index_v + 1;
            rom_index_v   := file_index_v / MEMORY_DATA_WIDTH_IN_BYTES_G;
            small_index_v := file_index_v mod MEMORY_DATA_WIDTH_IN_BYTES_G;

        end loop;

        return temp_v;
    end function;

    ---------------
    -- Constants --
    ---------------
    constant rom_c : rom_array_t := rom_init(FILE_G);

begin

    latency_gen : if LATENCY_G = 0 generate

        process(all) is
        begin
            data_o  <= rom_c(to_integer(unsigned(addr_i)));
            valid_o <= read_i;
        end process;

    elsif LATENCY_G = 1 generate

        process(clk_i) is
        begin
            if rising_edge(clk_i) then
                data_o  <= rom_c(to_integer(unsigned(addr_i)));
                valid_o <= read_i;
            end if;
        end process;

    else generate

        latency_block : block
            signal counter_s : natural := LATENCY_G;
            signal addr_s    : natural := 0;
        begin
            process(clk_i) is
            begin
                if rising_edge(clk_i) then
                    valid_o <= '0';
                    data_o <= (others => '0');

                    if read_i = '1' then
                        counter_s <= 0;
                        addr_s    <= to_integer(unsigned(addr_i));
                    end if;

                    if counter_s < LATENCY_G - 1 then
                        counter_s <= counter_s + 1;
                    elsif counter_s = LATENCY_G - 1 then
                        data_o    <= rom_c(addr_s);
                        valid_o   <= '1';
                        counter_s <= LATENCY_G;
                    end if;
                end if;
            end process;
        end block latency_block;

    end generate;

end architecture tb;
