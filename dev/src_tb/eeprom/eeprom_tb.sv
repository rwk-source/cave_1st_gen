//-----------------------------------------------------------------------------
//
// Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
//-----------------------------------------------------------------------------
// File         : eeprom_tb.sv
// Description  :
//
// Author       : Rick Wertenbroek
// Version      : 0.0
//
// Dependencies :
//-----------------------------------------------------------------------------

module eeprom_tb;

    ///////////////
    // Constants //
    ///////////////

    /////////////
    // Signals //
    /////////////
    logic simulation_tick = 0;
    logic cs_sti;
    logic ci_sti;
    logic di_sti;
    logic do_obs;

    logic [5:0] addr = 6'b001101;
    logic [15:0] word = 16'hAA55;

    /////////////
    // Stimuli //
    /////////////

    always #5 simulation_tick <= ~simulation_tick;

    default clocking cb @(posedge simulation_tick);
    endclocking // cb

    task apply_ci_edge();
        ##1;
        ci_sti <= 'b1;
        ##1;
        ci_sti <= 'b0;
    endtask // apply_ci_edge

    task stimuli();
        // Default - all low
        di_sti <= 'b0;
        ci_sti <= 'b0;
        cs_sti <= 'b0;

        ##5;

        // Write
        cs_sti <= 'b1;
        ##1;
        di_sti <= 'b1; // Start bit
        apply_ci_edge();
        di_sti <= 'b0; // OP1
        apply_ci_edge();
        di_sti <= 'b1; // OP2
        apply_ci_edge();
        for (int i = 0; i < 6; ++i) begin
            di_sti <= addr[i];
            apply_ci_edge();
        end // Addr
        for (int i = 0; i < 16; ++i) begin
            di_sti <= word[i];
            apply_ci_edge();
        end // Word
        ##5;
        cs_sti <= 'b0;
        ##5;

        // Read
        cs_sti <= 'b1;
        ##1;
        di_sti <= 'b1; // Start bit
        apply_ci_edge();
        di_sti <= 'b1; // OP1
        apply_ci_edge();
        di_sti <= 'b0; // OP2
        apply_ci_edge();
        for (int i = 0; i < 6; ++i) begin
            di_sti <= addr[i];
            apply_ci_edge();
        end // Addr
        for (int i = 0; i < 16; ++i) begin
            di_sti <= word[i]; // This should be unused by the DUT but serve as a comparison
            apply_ci_edge();
        end // Word
        ##5;
        cs_sti <= 'b0;
        ##5;

        $finish;

    endtask // stimuli

    initial begin
        stimuli();
    end

    /////////
    // DUT //
    /////////

    eeprom_93c46 dut_eeprom
        (.clk_i(ci_sti),
         .cs_i(cs_sti),
         .data_i(di_sti),
         .data_o(do_obs));

endmodule // eeprom_tb
