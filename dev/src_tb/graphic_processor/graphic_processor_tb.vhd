-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : graphic_processor_tb.vhd
-- Description  : Test Bench for the graphic processor
--
-- Author       : Rick Wertenbroek
-- Version      : 0.0
--
-- VHDL std     : 2008
-- Dependencies :
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.dodonpachi_pkg.all;

-------------------------------------------------------------------------------

entity graphic_processor_tb is

end entity graphic_processor_tb;

-------------------------------------------------------------------------------

architecture tb of graphic_processor_tb is

    -- component ports
    signal clk_sti                   : std_logic := '1';
    signal rst_sti                   : std_logic;
    signal start_sti                 : std_logic;
    --
    signal busy_obs                  : std_logic;
    --signal done_obs                  : std_logic;
    --
    signal sprite_ram_addr_s         : sprite_ram_info_access_t;
    signal sprite_ram_info_s         : sprite_ram_line_t;
    --
    signal layer_0_ram_addr_s        : layer_ram_info_access_t;
    signal layer_0_ram_info_s        : layer_ram_line_t;
    signal layer_1_ram_addr_s        : layer_ram_info_access_t;
    signal layer_1_ram_info_s        : layer_ram_line_t;
    signal layer_2_ram_addr_s        : layer_ram_info_access_t;
    signal layer_2_ram_info_s        : layer_ram_line_t;
    --
    signal rom_addr_s                : std_logic_vector(31 downto 0);
    signal tiny_burst_s              : std_logic;
    signal rom_burst_read_s          : std_logic;
    signal rom_data_s                : std_logic_vector(31 downto 0);
    signal rom_data_valid_s          : std_logic;
    signal rom_data_burst_done_s     : std_logic;
    --
    signal palette_ram_addr_s        : palette_ram_addr_t;
    signal palette_ram_data_s        : std_logic_vector(DDP_WORD_WIDTH-1 downto 0);
    --
    signal frame_buffer_addr_s       : frame_buffer_addr_t;
    signal frame_buffer_color_s      : color_t;
    signal frame_buffer_write_s      : std_logic;

begin -- architecture tb

    ----------------
    -- Sprite RAM --
    ----------------
    sprite_ram_sim : entity work.rom_from_file
        generic map (
            -- To get sprite_ram_dump.txt do :
            -- in MAME : dump sprite_ram_dump.bin, 0x400000, 0x10000
            -- xxd -p -c1 sprite_ram_dump.bin > sprite_ram_dump.txt
            --
            -- Note : separating each byte by a newline is not very
            -- efficient, memory-wise (since it will use 3x as much memory)
            -- but this makes it easier to read in VHDL (and since the
            -- sizes are reasonably small this is not so much of a problem).
            --
            -- Note : rom_from_file is little-endian, so the file must be
            -- converted to little-endian first (since the 68k is
            -- big-endian) This can be done with :
            -- awk '{getline x;print x;}1' file
            --
            -- Node : dd can swap bytes with conv=swab option.
            FILE_G                       => "sprite_ram_dump2.txt",
            MEMORY_LOG_SIZE_G            => DDP_SPRITE_RAM_LINE_ADDR_WIDTH,
            MEMORY_DATA_WIDTH_IN_BYTES_G => DDP_SPRITE_RAM_INFO_WORDS * 2,
            LATENCY_G                    => 1) -- BRAM
        port map (
            clk_i   => clk_sti,
            addr_i  => sprite_ram_addr_s,
            read_i  => '1',
            data_o  => sprite_ram_info_s,
            valid_o => open);


    ----------------
    -- Layer RAMs --
    ----------------

    -- To get layer0_ram_dump.txt do :
    -- in MAME : dump layer0_ram_dump.bin, 0x500000, 0x8000
    --
    -- Then switch bytes for endianness (motorola representation is
    -- big endian 16-bits)
    -- dd if=ram_dump_for_image/ram_dump_for_image/layer0_ram_dump.bin of=layer0_ram_dump_swab.bin conv=swab bs=1
    -- xxd -p -c1 layer0_ram_dump_swap.bin > layer0_ram_dump.txt

    layer_0_ram_sim : entity work.rom_from_file
        generic map (
            FILE_G                       => "layer0_ram_dump.txt",
            MEMORY_LOG_SIZE_G            => DDP_LAYER_TILE_RAM_LINE_ADDR_WIDTH,
            MEMORY_DATA_WIDTH_IN_BYTES_G => DDP_LAYER_TILE_RAM_INFO_WORDS * 2,
            LATENCY_G                    => 1) -- BRAM
        port map (
            clk_i   => clk_sti,
            addr_i  => layer_0_ram_addr_s,
            read_i  => '1',
            data_o  => layer_0_ram_info_s,
            valid_o => open);

    layer_1_ram_sim : entity work.rom_from_file
        generic map (
            FILE_G                       => "layer1_ram_dump.txt",
            MEMORY_LOG_SIZE_G            => DDP_LAYER_TILE_RAM_LINE_ADDR_WIDTH,
            MEMORY_DATA_WIDTH_IN_BYTES_G => DDP_LAYER_TILE_RAM_INFO_WORDS * 2,
            LATENCY_G                    => 1) -- BRAM
        port map (
            clk_i   => clk_sti,
            addr_i  => layer_1_ram_addr_s,
            read_i  => '1',
            data_o  => layer_1_ram_info_s,
            valid_o => open);

    layer_2_ram_sim : entity work.rom_from_file
        generic map (
            FILE_G                       => "layer2_ram_dump.txt",
            MEMORY_LOG_SIZE_G            => DDP_LAYER_TILE_RAM_LINE_ADDR_WIDTH,
            MEMORY_DATA_WIDTH_IN_BYTES_G => DDP_LAYER_TILE_RAM_INFO_WORDS * 2,
            LATENCY_G                    => 1) -- BRAM
        port map (
            clk_i   => clk_sti,
            addr_i  => layer_2_ram_addr_s,
            read_i  => '1',
            data_o  => layer_2_ram_info_s,
            valid_o => open);

    ---------
    -- ROM --
    ---------
    rom_sim : entity work.layer_rom
        generic map (
            -- To get layer0_rom.txt do :
            -- xxd -p -c1 u60.bin > layer0_rom.txt
            -- cat sprite_rom_2x_swab.bin layer0_test_swab.dmp layer1_test_swab.dmp layer2_test_swab.dmp > sprite_x2_layer_0_1_2_roms.bin
            -- xxd -p -c1 sprite_x2_layer_0_1_2_roms.bin > sprite_x2_layer_0_1_2_roms.txt

            --FILE_G                  => "layer0_rom.txt",
            --FILE_G                  => "sprite_x2_layer_0_1_2_roms.txt",
            FILE_G                  => "test_rom_gpu.txt",
            MEMORY_LOG_SIZE_BYTES_G => 25, -- TODO Check and remove magic number
            LATENCY_G               => 10, -- Just put something ...
            BURST_SIZE_4_BYTES      => 32) -- 16x16 tiles is 16x16x4bits = 16x8x8bits
                                           -- = 16x2x32bits = 32x32bits
        port map (
            clk_i        => clk_sti,
            addr_i       => rom_addr_s(24 downto 0), -- TODO
            burst_read_i => rom_burst_read_s,
            tiny_burst_i => tiny_burst_s,
            data_o       => rom_data_s,
            valid_o      => rom_data_valid_s,
            burst_done_o => rom_data_burst_done_s);

    -----------------
    -- Palette RAM --
    -----------------
    palette_ram_block : block
        signal addr_to_palette_ram_s   : palette_ram_addr_t;
        signal data_from_palette_ram_s : std_logic_vector(DDP_WORD_WIDTH-1 downto 0);
    begin
        addr_to_palette_ram_s <= palette_ram_addr_s;
        palette_ram_sim : entity work.rom_from_file
            generic map (
                -- To get palette_ram_dump.txt do :
                -- in MAME : dump palette_ram_dump.bin, 0xc00000, 0x10000
                -- xxd -p -c1 palette_ram_dump.bin > palette_ram_dump.txt
                FILE_G                       => "palette_ram_dump2.txt",
                MEMORY_LOG_SIZE_G            => DDP_PALETTE_RAM_ADDR_WIDTH,
                MEMORY_DATA_WIDTH_IN_BYTES_G => 2,
                LATENCY_G                    => 1)  -- BRAM
            port map (
                clk_i   => clk_sti,
                addr_i  => addr_to_palette_ram_s,
                read_i  => '1',
                data_o  => data_from_palette_ram_s,
                valid_o => open);
        palette_ram_data_s <= data_from_palette_ram_s;
    end block palette_ram_block;

    ------------------
    -- Frame Buffer --
    ------------------

    -- This may need an extra attribute so that it is not pruned
    frame_buffer_block : block
        type frame_buffer_t is array (2**DDP_FRAME_BUFFER_ADDR_BITS-1 downto 0) of std_logic_vector(DDP_WORD_WIDTH-1 downto 0);
        signal frame_buffer_s : frame_buffer_t := (others => (others => '0'));
    begin
        frame_buffer_process : process(clk_sti) is
        begin
            if rising_edge(clk_sti) then
                if frame_buffer_write_s = '1' then
                    frame_buffer_s(to_integer(unsigned(frame_buffer_addr_s))) <= "0" & frame_buffer_color_s.r & frame_buffer_color_s.g & frame_buffer_color_s.b;
                end if;
            end if;
        end process frame_buffer_process;
    end block frame_buffer_block;

    -------------------
    -- Log / Display --
    -------------------
    process(clk_sti) is
    begin
        if rising_edge(clk_sti) then
            if frame_buffer_write_s = '1' then
                report "GRB : " & to_hstring(unsigned("0" & frame_buffer_color_s.g & frame_buffer_color_s.r & frame_buffer_color_s.b)) & " at position " & integer'image(to_integer(unsigned(frame_buffer_addr_s(16 downto 8)))) & " " & integer'image(to_integer(unsigned(frame_buffer_addr_s(7 downto 0))));
            end if;
        end if;
    end process;

    ---------
    -- DUT --
    ---------
    dut_block : block
        --constant vctrl_reg_0_s : layer_info_t := generate_layer_settings(0, 1, 386, 0, 1, 16, 10, 0, 1);
        --constant vctrl_reg_1_s : layer_info_t := generate_layer_settings(0, 1, 385, 0, 1, 16, 10, 0, 1);
        --constant vctrl_reg_2_s : layer_info_t := generate_layer_settings(0, 0, 395, 0, 0, 8, 495, 0, 1);
        constant vctrl_reg_0_s : layer_info_line_t := x"0001A20A8382";
        constant vctrl_reg_1_s : layer_info_line_t := x"0001A20A8381";
        constant vctrl_reg_2_s : layer_info_line_t := x"000181EF818B";
    begin

        DUT : entity work.graphic_processor
            generic map (
                INCLUDE_LAYER_PROCESOR_G => true,
                SIMULATION_RAM_G         => true)
            port map (
                clk_i                 => clk_sti,
                rst_i                 => rst_sti,
                generate_frame_i      => start_sti,
                buffer_select_i       => '0',
                busy_o                => busy_obs,
                layer_processor_en_i  => '1',
                --
                sprite_ram_addr_o     => sprite_ram_addr_s,
                sprite_ram_info_i     => sprite_ram_info_s,
                --
                layer_0_ram_addr_o    => layer_0_ram_addr_s,
                layer_0_ram_info_i    => layer_0_ram_info_s,
                layer_1_ram_addr_o    => layer_1_ram_addr_s,
                layer_1_ram_info_i    => layer_1_ram_info_s,
                layer_2_ram_addr_o    => layer_2_ram_addr_s,
                layer_2_ram_info_i    => layer_2_ram_info_s,
                vctrl_reg_0_i         => vctrl_reg_0_s,
                vctrl_reg_1_i         => vctrl_reg_1_s,
                vctrl_reg_2_i         => vctrl_reg_2_s,
                --
                rom_addr_o            => rom_addr_s,
                tiny_burst_gfx_o      => tiny_burst_s,
                rom_burst_read_o      => rom_burst_read_s,
                rom_data_i            => rom_data_s,
                rom_data_valid_i      => rom_data_valid_s,
                rom_data_burst_done_i => rom_data_burst_done_s,
                --
                palette_ram_addr_o    => palette_ram_addr_s,
                palette_ram_data_i    => palette_ram_data_s,
                --
                frame_buffer_addr_o   => frame_buffer_addr_s,
                frame_buffer_color_o  => frame_buffer_color_s,
                frame_buffer_write_o  => frame_buffer_write_s,
                start_fb_dma_o        => open);

    end block dut_block;

    -- clock generation
    clk_sti <= not clk_sti after 10 ns;

    -- waveform generation
    WaveGen_Proc : process
    begin

        rst_sti           <= '1';
        start_sti         <= '0';

        -- Wait some
        for i in 0 to 9 loop
            wait until falling_edge(clk_sti);
        end loop;

        rst_sti           <= '0';

        -- Wait some
        for i in 0 to 9 loop
            wait until falling_edge(clk_sti);
        end loop;

        start_sti <= '1';
        wait until falling_edge(clk_sti);
        start_sti <= '0';

        wait until falling_edge(clk_sti) and busy_obs = '1';
        wait until falling_edge(clk_sti) and busy_obs = '0';

        report "Image generated !" severity note;

        start_sti <= '1';
        wait until falling_edge(clk_sti);
        start_sti <= '0';

        wait until falling_edge(clk_sti) and busy_obs = '1';
        wait until falling_edge(clk_sti) and busy_obs = '0';

        report "Image generated !" severity note;

        start_sti <= '1';
        wait until falling_edge(clk_sti);
        start_sti <= '0';

        wait until falling_edge(clk_sti) and busy_obs = '1';
        wait until falling_edge(clk_sti) and busy_obs = '0';

        report "Image generated !" severity note;

        wait;

    end process WaveGen_Proc;

end architecture tb;
