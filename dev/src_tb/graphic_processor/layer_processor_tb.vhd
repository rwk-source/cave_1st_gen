-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : layer_processor_tb.vhd
-- Description  : Test Bench for the layer processor
--
-- Author       : Rick Wertenbroek
-- Version      : 0.0
--
-- VHDL std     : 2008
-- Dependencies :
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.dodonpachi_pkg.all;

-------------------------------------------------------------------------------

entity layer_processor_tb is

end entity layer_processor_tb;

-------------------------------------------------------------------------------

architecture tb of layer_processor_tb is

    function generate_layer_settings(flip_x_en, row_scroll_en, scroll_x, flip_y_en, row_select_en, tile_size, scroll_y, layer_disabled, layer_priority : integer) return layer_info_t is
        variable layer_info_v : layer_info_t;
    begin
        -- (flip_x_en, row_scroll_en, scroll_x, flip_y_en, row_select_en, tile_size, scroll_y, layer_disabled, layer_priority)
        -- (0, 1, 386, 0, 1, 16, 10, 0, 1)
        layer_info_v.priority           := to_unsigned(layer_priority, layer_info_v.priority'length);
        if (tile_size = 8) then
            layer_info_v.small_tile     := '1';
        else
            layer_info_v.small_tile     := '0';
        end if;
        layer_info_v.disabled           := to_unsigned(layer_disabled, 1)(0);
        layer_info_v.flip_x             := to_unsigned(flip_x_en, 1)(0);
        layer_info_v.flip_y             := to_unsigned(flip_y_en, 1)(0);
        layer_info_v.row_scroll_enabled := to_unsigned(row_scroll_en, 1)(0);
        layer_info_v.row_select_enabled := to_unsigned(row_select_en, 1)(0);
        layer_info_v.scroll_x           := to_unsigned(scroll_x, layer_info_v.scroll_x'length);
        layer_info_v.scroll_y           := to_unsigned(scroll_y, layer_info_v.scroll_y'length);

        return layer_info_v;
    end generate_layer_settings;

    -- Input your settings and test files here !
    constant RAM_FILE_C              : string := "layer0_ram_dump.txt";
    constant ROM_FILE_C              : string := "layer0_rom.txt";
    constant layer_info_sti          : layer_info_t := generate_layer_settings(0, 1, 386, 0, 1, 16, 10, 0, 1);
    --constant RAM_FILE_C              : string := "layer2_ram_dump.txt";
    --constant ROM_FILE_C              : string := "layer2_rom.txt";
    --constant layer_info_sti          : layer_info_t := generate_layer_settings(0, 0, 395, 0, 0, 8, 495, 0, 1);

    -- component ports
    signal clk_sti                   : std_logic := '1';
    signal rst_sti                   : std_logic;
    signal start_sti                 : std_logic;
    --
    signal busy_obs                  : std_logic;
    signal done_obs                  : std_logic;
    signal layer_ram_addr_s          : layer_ram_info_access_t;
    signal layer_ram_info_s          : layer_ram_line_t;
    signal layer_rom_addr_s          : std_logic_vector(31 downto 0);
    signal layer_burst_read_s        : std_logic;
    signal layer_data_s              : std_logic_vector(31 downto 0);
    signal layer_data_valid_s        : std_logic;
    signal layer_data_burst_done_s   : std_logic;
    signal tiny_burst_s              : std_logic;
    signal priority_ram_read_addr_s  : priority_ram_addr_t;
    signal priority_ram_read_data_s  : priority_t;
    signal priority_ram_write_addr_s : priority_ram_addr_t;
    signal priority_ram_write_data_s : priority_t;
    signal priority_ram_write_s      : std_logic;
    signal palette_ram_addr_s        : palette_ram_addr_t;
    signal palette_ram_data_s        : color_t;
    signal frame_buffer_addr_s       : frame_buffer_addr_t;
    signal frame_buffer_color_s      : color_t;
    signal frame_buffer_write_s      : std_logic;

begin -- architecture tb

    layer_ram_block : block
    begin
        layer_ram_sim : entity work.rom_from_file
            generic map (
                -- To get layer0_ram_dump.txt do :
                -- in MAME : dump layer0_ram_dump.bin, 0x500000, 0x8000
                --
                -- Then switch bytes for endianness (motorola representation is
                -- big endian 16-bits)
                -- dd if=ram_dump_for_image/ram_dump_for_image/layer0_ram_dump.bin of=layer0_ram_dump_swab.bin conv=swab bs=1
                -- xxd -p -c1 layer0_ram_dump_swap.bin > layer0_ram_dump.txt
                --FILE_G                       => "layer0_ram_dump.txt",
                FILE_G                       => RAM_FILE_C,
                MEMORY_LOG_SIZE_G            => DDP_LAYER_TILE_RAM_LINE_ADDR_WIDTH,
                MEMORY_DATA_WIDTH_IN_BYTES_G => DDP_LAYER_TILE_RAM_INFO_WORDS * 2,
                LATENCY_G                    => 1) -- BRAM
            port map (
                clk_i   => clk_sti,
                addr_i  => layer_ram_addr_s,
                read_i  => '1',
                data_o  => layer_ram_info_s,
                valid_o => open);
    end block layer_ram_block;

    layer_rom_1 : entity work.layer_rom
        generic map (
            -- To get layer0_rom.txt do :
            -- xxd -p -c1 u60.bin > layer0_rom.txt

            --FILE_G                  => "layer0_rom.txt",
            FILE_G                  => ROM_FILE_C,
            MEMORY_LOG_SIZE_BYTES_G => 21, -- TODO Check and remove magic number
            LATENCY_G               => 10, -- Just put something ...
            BURST_SIZE_4_BYTES      => 32) -- 16x16 tiles is 16x16x4bits = 16x8x8bits
                                           -- = 16x2x32bits = 32x32bits
        port map (
            clk_i        => clk_sti,
            addr_i       => layer_rom_addr_s(20 downto 0),
            burst_read_i => layer_burst_read_s,
            tiny_burst_i => tiny_burst_s,
            data_o       => layer_data_s,
            valid_o      => layer_data_valid_s,
            burst_done_o => layer_data_burst_done_s);

    palette_ram_block : block
        signal addr_to_palette_ram_s   : palette_ram_addr_t;
        signal data_from_palette_ram_s : std_logic_vector(DDP_WORD_WIDTH-1 downto 0);
    begin
        addr_to_palette_ram_s <= palette_ram_addr_s;
        palette_ram_sim : entity work.rom_from_file
            generic map (
                -- To get palette_ram_dump.txt do :
                -- in MAME : dump palette_ram_dump.bin, 0xc00000, 0x10000
                -- xxd -p -c1 palette_ram_dump.bin > palette_ram_dump.txt
                FILE_G                       => "palette_ram_dump2.txt",
                MEMORY_LOG_SIZE_G            => DDP_PALETTE_RAM_ADDR_WIDTH,
                MEMORY_DATA_WIDTH_IN_BYTES_G => 2,
                LATENCY_G                    => 1)  -- BRAM
            port map (
                clk_i   => clk_sti,
                addr_i  => addr_to_palette_ram_s,
                read_i  => '1',
                data_o  => data_from_palette_ram_s,
                valid_o => open);
        palette_ram_data_s <= extract_color_from_palette_data(data_from_palette_ram_s);
    end block palette_ram_block;

    priority_buffer_block : block
        type priority_buffer_t is array (2**DDP_FRAME_BUFFER_ADDR_BITS-1 downto 0) of priority_t;
        signal priority_buffer_s : priority_buffer_t := (others => (others => '0'));
    begin
        priority_buffer_process : process(clk_sti) is
        begin
            if rising_edge(clk_sti) then
                -- Read
                priority_ram_read_data_s <= priority_buffer_s(to_integer(unsigned(priority_ram_read_addr_s)));
                -- Write
                if priority_ram_write_s = '1' then
                    priority_buffer_s(to_integer(unsigned(priority_ram_write_addr_s))) <= priority_ram_write_data_s;
                end if;
            end if;
        end process priority_buffer_process;
    end block priority_buffer_block;

    -- This may need an extra attribute so that it is not pruned
    frame_buffer_block : block
        type frame_buffer_t is array (2**DDP_FRAME_BUFFER_ADDR_BITS-1 downto 0) of std_logic_vector(DDP_WORD_WIDTH-1 downto 0);
        signal frame_buffer_s : frame_buffer_t := (others => (others => '0'));
    begin
        frame_buffer_process : process(clk_sti) is
        begin
            if rising_edge(clk_sti) then
                if frame_buffer_write_s = '1' then
                    frame_buffer_s(to_integer(unsigned(frame_buffer_addr_s))) <= "0" & frame_buffer_color_s.r & frame_buffer_color_s.g & frame_buffer_color_s.b;
                end if;
            end if;
        end process frame_buffer_process;
    end block frame_buffer_block;

    process(clk_sti) is
    begin
        if rising_edge(clk_sti) then
            if frame_buffer_write_s = '1' then
                report "GRB : " & to_hstring(unsigned("0" & frame_buffer_color_s.g & frame_buffer_color_s.r & frame_buffer_color_s.b)) & " at position " & integer'image(to_integer(unsigned(frame_buffer_addr_s(16 downto 8)))) & " " & integer'image(to_integer(unsigned(frame_buffer_addr_s(7 downto 0))));
            end if;
        end if;
    end process;

    dut_block : block
        --signal layer_info_sti : layer_info_t;
    begin

        -- (flip_x_en, row_scroll_en, scroll_x, flip_y_en, row_select_en, tile_size, scroll_y, layer_disabled, layer_priority)
        -- (0, 1, 386, 0, 1, 16, 10, 0, 1)
        --layer_info_sti.priority           <= "01";
        --layer_info_sti.small_tile         <= '0';
        --layer_info_sti.disabled           <= '0';
        --layer_info_sti.flip_x             <= '0';
        --layer_info_sti.flip_y             <= '0';
        --layer_info_sti.row_scroll_enabled <= '1';
        --layer_info_sti.row_select_enabled <= '1';
        --layer_info_sti.scroll_x           <= to_unsigned(386, layer_info_sti.scroll_x'length);
        --layer_info_sti.scroll_y           <= to_unsigned(10, layer_info_sti.scroll_y'length);

        -- component instantiation
        DUT : entity work.layer_processor
            port map (
                clk_i                     => clk_sti,
                rst_i                     => rst_sti,
                start_i                   => start_sti,
                busy_o                    => busy_obs,
                done_o                    => done_obs,
                first_layer_i             => '1',
                layer_info_i              => layer_info_sti,
                --
                layer_ram_addr_o          => layer_ram_addr_s,
                layer_ram_info_i          => layer_ram_info_s,
                --
                layer_rom_addr_o          => layer_rom_addr_s,
                tiny_burst_o              => tiny_burst_s,
                layer_burst_read_o        => layer_burst_read_s,
                layer_data_i              => layer_data_s,
                layer_data_valid_i        => layer_data_valid_s,
                layer_data_burst_done_i   => layer_data_burst_done_s,
                --
                priority_ram_read_addr_o  => priority_ram_read_addr_s,
                priority_ram_data_i       => priority_ram_read_data_s,
                priority_ram_write_addr_o => priority_ram_write_addr_s,
                priority_ram_data_o       => priority_ram_write_data_s,
                priority_ram_write_o      => priority_ram_write_s,
                --
                palette_ram_addr_o        => palette_ram_addr_s,
                palette_ram_data_i        => palette_ram_data_s,
                --
                frame_buffer_addr_o       => frame_buffer_addr_s,
                frame_buffer_color_o      => frame_buffer_color_s,
                frame_buffer_write_o      => frame_buffer_write_s);

    end block dut_block;

    -- clock generation
    clk_sti <= not clk_sti after 10 ns;

    -- waveform generation
    WaveGen_Proc : process
    begin

        rst_sti           <= '1';
        start_sti         <= '0';

        -- Wait some
        for i in 0 to 9 loop
            wait until falling_edge(clk_sti);
        end loop;

        rst_sti           <= '0';

        -- Wait some
        for i in 0 to 9 loop
            wait until falling_edge(clk_sti);
        end loop;

        start_sti <= '1';
        wait until falling_edge(clk_sti);
        start_sti <= '0';

        wait until falling_edge(clk_sti) and busy_obs = '1';
        wait until falling_edge(clk_sti) and done_obs = '1';
        --wait until falling_edge(clk_sti) and busy_obs = '0';

        report "Image generated !" severity note;

        wait;

    end process WaveGen_Proc;

end architecture tb;
