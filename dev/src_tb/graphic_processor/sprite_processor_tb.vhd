-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : sprite_processor_tb.vhd
-- Description  : Test Bench for the sprite processor
--
-- Author       : Rick Wertenbroek
-- Version      : 0.0
--
-- VHDL std     : 2008
-- Dependencies :
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.dodonpachi_pkg.all;

-------------------------------------------------------------------------------

entity sprite_processor_tb is

end entity sprite_processor_tb;

-------------------------------------------------------------------------------

architecture tb of sprite_processor_tb is

    -- component ports
    signal clk_sti                   : std_logic := '1';
    signal rst_sti                   : std_logic;
    signal start_sti                 : std_logic;
    signal buffer_select_sti         : std_logic;
    signal busy_obs                  : std_logic;
    signal done_obs                  : std_logic;
    signal sprite_ram_addr_s         : sprite_ram_info_access_t;
    signal sprite_ram_info_s         : sprite_ram_line_t;
    signal sprite_rom_addr_s         : std_logic_vector(DDP_SPRITE_ROM_ADDR_WIDTH-1 downto 0);
    signal sprite_burst_read_s       : std_logic;
    signal sprite_data_s             : std_logic_vector(31 downto 0);
    signal sprite_data_valid_s       : std_logic;
    signal sprite_data_burst_done_s  : std_logic;
    signal priority_ram_read_addr_s  : priority_ram_addr_t;
    signal priority_ram_read_data_s  : priority_t;
    signal priority_ram_write_addr_s : priority_ram_addr_t;
    signal priority_ram_write_data_s : priority_t;
    signal priority_ram_write_s      : std_logic;
    signal palette_ram_addr_s        : palette_ram_addr_t;
    signal palette_ram_data_s        : color_t;
    signal frame_buffer_addr_s       : frame_buffer_addr_t;
    signal frame_buffer_color_s      : color_t;
    signal frame_buffer_write_s      : std_logic;

begin -- architecture tb

    sprite_ram_block : block
    begin
        sprite_ram_sim : entity work.rom_from_file
            generic map (
                -- To get sprite_ram_dump.txt do :
                -- in MAME : dump sprite_ram_dump.bin, 0x400000, 0x10000
                -- xxd -p -c1 sprite_ram_dump.bin > sprite_ram_dump.txt
                --
                -- Note : separating each byte by a newline is not very
                -- efficient, memory-wise (since it will use 3x as much memory)
                -- but this makes it easier to read in VHDL (and since the
                -- sizes are reasonably small this is not so much of a problem).
                --
                -- Note : rom_from_file is little-endian, so the file must be
                -- converted to little-endian first (since the 68k is
                -- big-endian) This can be done with :
                -- awk '{getline x;print x;}1' file
                --
                -- Node : dd can swap bytes with conv=swab option.
                FILE_G                       => "sprite_ram_dump2.txt",
                MEMORY_LOG_SIZE_G            => DDP_SPRITE_RAM_LINE_ADDR_WIDTH,
                MEMORY_DATA_WIDTH_IN_BYTES_G => DDP_SPRITE_RAM_INFO_WORDS * 2,
                LATENCY_G                    => 1) -- BRAM
            port map (
                clk_i   => clk_sti,
                addr_i  => sprite_ram_addr_s,
                read_i  => '1',
                data_o  => sprite_ram_info_s,
                valid_o => open);
    end block sprite_ram_block;

    sprite_rom_1 : entity work.sprite_rom
        generic map (
            -- To get sprite_rom.txt do :
            -- cat u50.bin u51.bin u52.bin u53.bin > sprite_rom.bin
            -- xxd -p -c1 sprite_rom.bin > sprite_rom.txt

            -- This needs to have the endianness changed
            -- MAME source : ROM_LOAD16_WORD_SWAP
            -- Change with : awk '{getline x;print x;}1' file
            FILE_G                  => "sprite_rom2.txt",
            MEMORY_LOG_SIZE_BYTES_G => 23, -- TODO Check and remove magic number
            LATENCY_G               => 10, -- Just put something ...
            BURST_SIZE_4_BYTES      => 32) -- 16x16 tiles is 16x16x4bits = 16x8x8bits
                                           -- = 16x2x32bits = 32x32bits
        port map (
            clk_i        => clk_sti,
            addr_i       => sprite_rom_addr_s,
            burst_read_i => sprite_burst_read_s,
            data_o       => sprite_data_s,
            valid_o      => sprite_data_valid_s,
            burst_done_o => sprite_data_burst_done_s);

    palette_ram_block : block
        signal addr_to_palette_ram_s   : palette_ram_addr_t;
        signal data_from_palette_ram_s : std_logic_vector(DDP_WORD_WIDTH-1 downto 0);
    begin
        addr_to_palette_ram_s <= palette_ram_addr_s;
        palette_ram_sim : entity work.rom_from_file
            generic map (
                -- To get palette_ram_dump.txt do :
                -- in MAME : dump palette_ram_dump.bin, 0xc00000, 0x10000
                -- xxd -p -c1 palette_ram_dump.bin > palette_ram_dump.txt
                FILE_G                       => "palette_ram_dump2.txt",
                MEMORY_LOG_SIZE_G            => DDP_PALETTE_RAM_ADDR_WIDTH,
                MEMORY_DATA_WIDTH_IN_BYTES_G => 2,
                LATENCY_G                    => 1)  -- BRAM
            port map (
                clk_i   => clk_sti,
                addr_i  => addr_to_palette_ram_s,
                read_i  => '1',
                data_o  => data_from_palette_ram_s,
                valid_o => open);
        palette_ram_data_s <= extract_color_from_palette_data(data_from_palette_ram_s);
    end block palette_ram_block;

    priority_buffer_block : block
        type priority_buffer_t is array (2**DDP_FRAME_BUFFER_ADDR_BITS-1 downto 0) of priority_t;
        signal priority_buffer_s : priority_buffer_t := (others => (others => '0'));
    begin
        priority_buffer_process : process(clk_sti) is
        begin
            if rising_edge(clk_sti) then
                -- Read
                priority_ram_read_data_s <= priority_buffer_s(to_integer(unsigned(priority_ram_read_addr_s)));
                -- Write
                if priority_ram_write_s = '1' then
                    priority_buffer_s(to_integer(unsigned(priority_ram_write_addr_s))) <= priority_ram_write_data_s;
                end if;
            end if;
        end process priority_buffer_process;
    end block priority_buffer_block;

    -- This may need an extra attribute so that it is not pruned
    frame_buffer_block : block
        type frame_buffer_t is array (2**DDP_FRAME_BUFFER_ADDR_BITS-1 downto 0) of std_logic_vector(DDP_WORD_WIDTH-1 downto 0);
        signal frame_buffer_s : frame_buffer_t := (others => (others => '0'));
    begin
        frame_buffer_process : process(clk_sti) is
        begin
            if rising_edge(clk_sti) then
                if frame_buffer_write_s = '1' then
                    frame_buffer_s(to_integer(unsigned(frame_buffer_addr_s))) <= "0" & frame_buffer_color_s.r & frame_buffer_color_s.g & frame_buffer_color_s.b;
                end if;
            end if;
        end process frame_buffer_process;
    end block frame_buffer_block;

    process(clk_sti) is
    begin
        if rising_edge(clk_sti) then
            if frame_buffer_write_s = '1' then
                report "GRB : " & to_hstring(unsigned("0" & frame_buffer_color_s.g & frame_buffer_color_s.r & frame_buffer_color_s.b)) & " at position " & integer'image(to_integer(unsigned(frame_buffer_addr_s(16 downto 8)))) & " " & integer'image(to_integer(unsigned(frame_buffer_addr_s(7 downto 0))));
            end if;
        end if;
    end process;

    -- component instantiation
    DUT : entity work.sprite_processor
        port map (
            -- Control
            clk_i                     => clk_sti,
            rst_i                     => rst_sti,
            start_i                   => start_sti,
            buffer_select_i           => buffer_select_sti,
            busy_o                    => busy_obs,
            done_o                    => done_obs,
            -- Sprite RAM
            sprite_ram_addr_o         => sprite_ram_addr_s,
            sprite_ram_info_i         => sprite_ram_info_s,
            -- Sprite ROM
            sprite_rom_addr_o         => sprite_rom_addr_s,
            sprite_burst_read_o       => sprite_burst_read_s,
            sprite_data_i             => sprite_data_s,
            sprite_data_valid_i       => sprite_data_valid_s,
            sprite_data_burst_done_i  => sprite_data_burst_done_s,
            -- Priority RAM
            priority_ram_read_addr_o  => priority_ram_read_addr_s,
            priority_ram_data_i       => priority_ram_read_data_s,
            priority_ram_write_addr_o => priority_ram_write_addr_s,
            priority_ram_data_o       => priority_ram_write_data_s,
            priority_ram_write_o      => priority_ram_write_s,
            -- Palette RAM
            palette_ram_addr_o        => palette_ram_addr_s,
            palette_ram_data_i        => palette_ram_data_s,
            -- Frame Buffer
            frame_buffer_addr_o       => frame_buffer_addr_s,
            frame_buffer_color_o      => frame_buffer_color_s,
            frame_buffer_write_o      => frame_buffer_write_s);

    -- clock generation
    clk_sti <= not clk_sti after 10 ns;

    -- waveform generation
    WaveGen_Proc : process
    begin

        rst_sti           <= '1';
        start_sti         <= '0';
        buffer_select_sti <= '0';

        -- Wait some
        for i in 0 to 9 loop
            wait until falling_edge(clk_sti);
        end loop;

        rst_sti           <= '0';

        -- Wait some
        for i in 0 to 9 loop
            wait until falling_edge(clk_sti);
        end loop;

        start_sti <= '1';
        wait until falling_edge(clk_sti);
        start_sti <= '0';

        wait until falling_edge(clk_sti) and busy_obs = '1';
        wait until falling_edge(clk_sti) and done_obs = '1';
        --wait until falling_edge(clk_sti) and busy_obs = '0';

        report "Image generated !" severity note;

        wait;

    end process WaveGen_Proc;

end architecture tb;
