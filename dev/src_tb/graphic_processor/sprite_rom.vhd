-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : sprite_rom.vhd
-- Description  : Simulates a memory capable of burst reads of 32-bit words
--
-- Author       : Rick Wertenbroek
-- Version      : 0.0
--
-- VHDL std     : 2008
-- Dependencies : dodonpachi_pkg.vhd
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

use work.dodonpachi_pkg.all;

entity sprite_rom is
    generic (
        -- The input file for the ROM (one byte per line)
        FILE_G                  : string;
        -- The log of the depth of the memory (in bytes)
        MEMORY_LOG_SIZE_BYTES_G : natural;
        -- The number of cycles to wait before burst arrives
        LATENCY_G               : natural := 1;
        -- The number of 32 bit words bursted
        BURST_SIZE_4_BYTES      : natural := 4
        );
    port (
        clk_i        : in  std_logic;
        addr_i       : in  std_logic_vector(DDP_SPRITE_ROM_ADDR_WIDTH-1 downto 0);
        burst_read_i : in  std_logic;
        data_o       : out std_logic_vector(31 downto 0);
        valid_o      : out std_logic;
        burst_done_o : out std_logic
        );
end entity sprite_rom;

architecture tb of sprite_rom is

    ---------------
    -- Constants --
    ---------------
    constant MEMORY_DEPTH_BYTES : natural := 2**MEMORY_LOG_SIZE_BYTES_G;

    -----------
    -- Types --
    -----------
    type rom_array_t is array (MEMORY_DEPTH_BYTES-1 downto 0) of std_logic_vector(DDP_BYTE_WIDTH-1 downto 0);

    ---------------
    -- Functions --
    ---------------
    impure function rom_init(filename : string) return rom_array_t is
        file dump_file_f      : text open read_mode is filename;
        variable file_line_v  : line;
        variable line_value_v : unsigned(DDP_BYTE_WIDTH-1 downto 0);
        variable temp_v       : rom_array_t;
        variable index_v      : natural := 0;
    begin

        temp_v := (others => (others => '0'));

        while (not endfile(dump_file_f) and (index_v < MEMORY_DEPTH_BYTES)) loop

            readline(dump_file_f, file_line_v);
            hread(file_line_v, line_value_v);
            temp_v(index_v) := std_logic_vector(line_value_v);

            index_v := index_v + 1;

        end loop;

        return temp_v;
    end function;

    --------------------
    -- The sprite ROM --
    --------------------
    constant sprite_rom_c : rom_array_t := rom_init(FILE_G);

begin

    burst_block : block
        signal latency_counter_s : natural := 0;
        signal burst_counter_s   : natural := 0;
        signal addr_s            : natural := 0;
        signal request_s         : std_logic := '0';
    begin
        process(clk_i) is
        begin
            if rising_edge(clk_i) then
                data_o       <= (others => '0');
                valid_o      <= '0';
                burst_done_o <= '0';

                if (burst_read_i = '1') and (request_s = '0') then
                    request_s         <= '1';
                    addr_s            <= to_integer(unsigned(addr_i));
                    latency_counter_s <= 0;
                    burst_counter_s   <= 0;
                elsif latency_counter_s = LATENCY_G then
                    if burst_counter_s < BURST_SIZE_4_BYTES then
                        -- Data as a 32bit word
                        data_o          <= sprite_rom_c(addr_s+3) & sprite_rom_c(addr_s+2) & sprite_rom_c(addr_s+1) & sprite_rom_c(addr_s);
                        valid_o         <= '1';
                        burst_counter_s <= burst_counter_s + 1;
                        addr_s          <= addr_s + 4;
                        if burst_counter_s = (BURST_SIZE_4_BYTES - 1) then
                            burst_done_o <= '1';
                        end if;
                    else
                        data_o    <= (others => '0');
                        valid_o   <= '0';
                        request_s <= '0';
                    end if;
                elsif request_s = '1' then
                    latency_counter_s <= latency_counter_s + 1;
                end if;
            end if;
        end process;

    end block burst_block;

end tb;
