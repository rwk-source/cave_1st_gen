`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Xilinx Inc
// Design Name: PYNQ
// Module Name: top
// Project Name: PYNQ
// Target Devices: ZC7020
// Tool Versions: 2016.1
// Description:
//////////////////////////////////////////////////////////////////////////////////

`default_nettype none

module top(
    DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    Vaux0_v_n,
    Vaux0_v_p,
    Vaux12_v_n,
    Vaux12_v_p,
    Vaux8_v_n,
    Vaux8_v_p,
    Vaux13_v_n,
    Vaux13_v_p,
    Vaux15_v_n,
    Vaux15_v_p,
    Vaux1_v_n,
    Vaux1_v_p,
    Vaux5_v_n,
    Vaux5_v_p,
    Vaux6_v_n,
    Vaux6_v_p,
    Vaux9_v_n,
    Vaux9_v_p,
    Vp_Vn_v_n,
    Vp_Vn_v_p,
    btns_4bits_tri_i,
    gpio_shield_sw_a5_a0_d13_d0_tri_io,
    ck_an_tri_io,
    ck_gpio_tri_io,
    hdmi_in_clk_n,
    hdmi_in_clk_p,
    hdmi_in_data_n,
    hdmi_in_data_p,
    hdmi_in_ddc_scl_io,
    hdmi_in_ddc_sda_io,
    hdmi_in_hpd,
    hdmi_out_clk_n,
    hdmi_out_clk_p,
    hdmi_out_data_n,
    hdmi_out_data_p,
    hdmi_out_ddc_scl_io,
    hdmi_out_ddc_sda_io,
    hdmi_out_hpd,
    iic_sw_shield_scl_io,
    iic_sw_shield_sda_io,
    leds_4bits_tri_o,
    spi_sw_shield_io0_io,
    spi_sw_shield_io1_io,
    spi_sw_shield_sck_io,
    spi_sw_shield_ss_io,
    pmodJA,
    pmodJB,
    pdm_audio_shutdown,
    pdm_m_clk,
    pdm_m_data_i,
    pwm_audio_o,
    rgbleds_6bits_tri_o,
    sws_2bits_tri_i);

    inout [14:0] DDR_addr;
    inout [2:0]  DDR_ba;
    inout        DDR_cas_n;
    inout        DDR_ck_n;
    inout        DDR_ck_p;
    inout        DDR_cke;
    inout        DDR_cs_n;
    inout [3:0]  DDR_dm;
    inout [31:0] DDR_dq;
    inout [3:0]  DDR_dqs_n;
    inout [3:0]  DDR_dqs_p;
    inout        DDR_odt;
    inout        DDR_ras_n;
    inout        DDR_reset_n;
    inout        DDR_we_n;
    inout        FIXED_IO_ddr_vrn;
    inout        FIXED_IO_ddr_vrp;
    inout [53:0] FIXED_IO_mio;
    inout        FIXED_IO_ps_clk;
    inout        FIXED_IO_ps_porb;
    inout        FIXED_IO_ps_srstb;
    input        Vaux0_v_n;
    input        Vaux0_v_p;
    input        Vaux12_v_n;
    input        Vaux12_v_p;
    input        Vaux8_v_n;
    input        Vaux8_v_p;
    input        Vaux13_v_n;
    input        Vaux13_v_p;
    input        Vaux15_v_n;
    input        Vaux15_v_p;
    input        Vaux1_v_n;
    input        Vaux1_v_p;
    input        Vaux5_v_n;
    input        Vaux5_v_p;
    input        Vaux6_v_n;
    input        Vaux6_v_p;
    input        Vaux9_v_n;
    input        Vaux9_v_p;
    input        Vp_Vn_v_n;
    input        Vp_Vn_v_p;
    input [3:0]  btns_4bits_tri_i;
    inout [19:0] gpio_shield_sw_a5_a0_d13_d0_tri_io;
    inout [5:0]  ck_an_tri_io;
    inout [15:0] ck_gpio_tri_io;
    input        hdmi_in_clk_n;
    input        hdmi_in_clk_p;
    input [2:0]  hdmi_in_data_n;
    input [2:0]  hdmi_in_data_p;
    inout        hdmi_in_ddc_scl_io;
    inout        hdmi_in_ddc_sda_io;
    output [0:0] hdmi_in_hpd;
    output       hdmi_out_clk_n;
    output       hdmi_out_clk_p;
    output [2:0] hdmi_out_data_n;
    output [2:0] hdmi_out_data_p;
    inout        hdmi_out_ddc_scl_io;
    inout        hdmi_out_ddc_sda_io;
    output [0:0] hdmi_out_hpd;
    inout        iic_sw_shield_scl_io;
    inout        iic_sw_shield_sda_io;
    output [3:0] leds_4bits_tri_o;
    input [1:0]  sws_2bits_tri_i;
    inout        spi_sw_shield_io0_io;
    inout        spi_sw_shield_io1_io;
    inout        spi_sw_shield_sck_io;
    inout        spi_sw_shield_ss_io;
    inout [7:0]  pmodJA;
    inout [7:0]  pmodJB;
    output [0:0] pdm_audio_shutdown;
    output [0:0] pdm_m_clk;
    input        pdm_m_data_i;
    output [5:0] rgbleds_6bits_tri_o;
    output [0:0] pwm_audio_o;

    wire [14:0]  DDR_addr;
    wire [2:0]   DDR_ba;
    wire         DDR_cas_n;
    wire         DDR_ck_n;
    wire         DDR_ck_p;
    wire         DDR_cke;
    wire         DDR_cs_n;
    wire [3:0]   DDR_dm;
    wire [31:0]  DDR_dq;
    wire [3:0]   DDR_dqs_n;
    wire [3:0]   DDR_dqs_p;
    wire         DDR_odt;
    wire         DDR_ras_n;
    wire         DDR_reset_n;
    wire         DDR_we_n;
    wire         FIXED_IO_ddr_vrn;
    wire         FIXED_IO_ddr_vrp;
    wire [53:0]  FIXED_IO_mio;
    wire         FIXED_IO_ps_clk;
    wire         FIXED_IO_ps_porb;
    wire         FIXED_IO_ps_srstb;
    wire         Vaux0_v_n;
    wire         Vaux0_v_p;
    wire         Vaux12_v_n;
    wire         Vaux12_v_p;
    wire         Vaux8_v_n;
    wire         Vaux8_v_p;
    wire         Vaux13_v_n;
    wire         Vaux13_v_p;
    wire         Vaux15_v_n;
    wire         Vaux15_v_p;
    wire         Vaux1_v_n;
    wire         Vaux1_v_p;
    wire         Vaux5_v_n;
    wire         Vaux5_v_p;
    wire         Vaux6_v_n;
    wire         Vaux6_v_p;
    wire         Vaux9_v_n;
    wire         Vaux9_v_p;
    wire         Vp_Vn_v_n;
    wire         Vp_Vn_v_p;
    wire [3:0]   btns_4bits_tri_i;
    wire [19:0]  gpio_shield_sw_a5_a0_d13_d0_tri_io;
    wire [15:0]  ck_gpio_tri_i;
    wire [15:0]  ck_gpio_tri_io;
    wire [15:0]  ck_gpio_tri_o;
    wire [15:0]  ck_gpio_tri_t;
    wire         hdmi_in_clk_n;
    wire         hdmi_in_clk_p;
    wire [2:0]   hdmi_in_data_n;
    wire [2:0]   hdmi_in_data_p;
    wire         hdmi_in_ddc_scl_i;
    wire         hdmi_in_ddc_scl_io;
    wire         hdmi_in_ddc_scl_o;
    wire         hdmi_in_ddc_scl_t;
    wire         hdmi_in_ddc_sda_i;
    wire         hdmi_in_ddc_sda_io;
    wire         hdmi_in_ddc_sda_o;
    wire         hdmi_in_ddc_sda_t;
    wire [0:0]   hdmi_in_hpd;
    wire         hdmi_out_clk_n;
    wire         hdmi_out_clk_p;
    wire [2:0]   hdmi_out_data_n;
    wire [2:0]   hdmi_out_data_p;
    wire         hdmi_out_ddc_scl_i;
    wire         hdmi_out_ddc_scl_io;
    wire         hdmi_out_ddc_scl_o = 0;
    wire         hdmi_out_ddc_scl_t;
    wire         hdmi_out_ddc_sda_i;
    wire         hdmi_out_ddc_sda_io;
    wire         hdmi_out_ddc_sda_o = 0;
    wire         hdmi_out_ddc_sda_t;
    wire [0:0]   hdmi_out_hpd = {1'b0};
    wire         shield2sw_scl_i_in;
    wire         shield2sw_sda_i_in;
    wire         sw2shield_scl_o_out;
    wire         sw2shield_scl_t_out;
    wire         sw2shield_sda_o_out;
    wire         sw2shield_sda_t_out;

    wire         iic_sw_shield_scl_io;
    wire         iic_sw_shield_sda_io;
    wire [3:0]   leds_4bits_tri_o;
    wire [7:0]   pmodJA_data_in;
    wire [7:0]   pmodJA_data_out;
    wire [7:0]   pmodJA_tri_out;
    wire [7:0]   pmodJB_data_in;
    wire [7:0]   pmodJB_data_out;
    wire [7:0]   pmodJB_tri_out;
    wire         spi_sw_shield_io0_i;
    wire         spi_sw_shield_io0_io;
    wire         spi_sw_shield_io0_o;
    wire         spi_sw_shield_io0_t;
    wire         spi_sw_shield_io1_i;
    wire         spi_sw_shield_io1_io;
    wire         spi_sw_shield_io1_o;
    wire         spi_sw_shield_io1_t;
    wire         spi_sw_shield_sck_i;
    wire         spi_sw_shield_sck_io;
    wire         spi_sw_shield_sck_o;
    wire         spi_sw_shield_sck_t;
    wire         spi_sw_shield_ss_i;
    wire         spi_sw_shield_ss_io;
    wire         spi_sw_shield_ss_o;
    wire         spi_sw_shield_ss_t;
    wire [1:0]   sws_2bits_tri_i;
    wire [7:0]   pmodJA;
    wire [7:0]   pmodJB;
    wire [0:0]   pdm_audio_shutdown;
    wire [0:0]   pdm_m_clk;
    wire         pdm_m_data_i;
    wire [5:0]   rgbleds_6bits_tri_o;
    wire [0:0]   pwm_audio_o;

    wire [19:0]  shield2sw_data_i;
    wire [19:0]  sw2shield_data_o;
    wire [19:0]  sw2shield_tri_o;

    // User signals
    wire         User_Design_Clk;
    wire         User_Reset;
    wire         From_User_HDMI_TX_Clk;
    wire         From_User_HDMI_TX_R;
    wire         From_User_HDMI_TX_G;
    wire         From_User_HDMI_TX_B;
    wire [31:0]  User_Design_Control;
    wire [31:0]  addr_to_user_w;
    wire [31:0]  sprite_rom_addr_to_user;
    wire [31:0]  data_from_User;
    wire [31:0]  tg68_addr_32b;
    wire [9:0]   tg68_addr;
    wire [31:0]  tg68_data;
    wire         tg68_write;
    wire [3:0]   tg68_writeb;
    wire         tg68_clk;
    // User AXI to DDR
    wire [31:0]  User_S00_AXI_0_araddr;
    wire [1:0]   User_S00_AXI_0_arburst;
    wire [3:0]   User_S00_AXI_0_arcache;
    wire [7:0]   User_S00_AXI_0_arlen;
    wire [0:0]   User_S00_AXI_0_arlock;
    wire [2:0]   User_S00_AXI_0_arprot;
    wire [3:0]   User_S00_AXI_0_arqos;
    wire         User_S00_AXI_0_arready;
    wire [3:0]   User_S00_AXI_0_arregion;
    wire [2:0]   User_S00_AXI_0_arsize;
    wire         User_S00_AXI_0_arvalid;
    wire [31:0]  User_S00_AXI_0_rdata;
    wire         User_S00_AXI_0_rlast;
    wire         User_S00_AXI_0_rready;
    wire [1:0]   User_S00_AXI_0_rresp;
    wire         User_S00_AXI_0_rvalid;

    wire         Reset_VGA;

    reg          Sync_Reset;
    reg          Sync_Reset_68k;
    reg          Sync_Enable_68k;
    reg          Sync_Lock_Step_68k;
    reg          Sync_Step_68k;

    wire [31:0]  break_point_addr;
    reg          break_point_enabled;

    wire [31:0]  rom_addr_68k_cache;
    wire         rom_read_68k_cache;
    wire         rom_valid_68k_cache;
    wire [255:0] rom_data_68k_cache;

    wire [15:0]  frame_fifo_data;
    wire         frame_fifo_empty;
    wire         frame_fifo_read;
    wire         frame_fifo_clk;
    wire         draw_hi_frame_n_lo_frame;

    wire [31:0]  gfx_addr;
    wire         gfx_tiny_burst;
    wire         gfx_burst_read;
    wire         gfx_burst_done;
    wire [31:0]  gfx_data;
    wire         gfx_data_valid;

    wire         fb_dma_transfer;
    wire         fb_dma_busy;
    wire         fb_dma_hi_nlo;
    wire [15:0]  fb_addr;
    wire [31:0]  fb_data;

    wire [31:0]  joystick_inputs;
    wire [31:0]  joystick_to_top;

    // Debug from linux
    wire         dma_from_axi_linux;
    wire         dma_hi_nlo_axi_linux;

    reg          layer_processor_enabled;

  // ChipKit related header signals
  genvar i;
  generate
	for (i=0; i < 16; i=i+1)
	begin: ck_gpio_iobuf
		IOBUF ck_gpio_tri_iobuf_i(
			.I(ck_gpio_tri_o[i] ),
			.IO(ck_gpio_tri_io[i]),
			.O(ck_gpio_tri_i[i]),
			.T(ck_gpio_tri_t[i])
			);
	end
  endgenerate

// for HDMI in
//IOBUF hdmi_in_ddc_scl_iobuf
// (.I(hdmi_in_ddc_scl_o),
//  .IO(hdmi_in_ddc_scl_io),
//  .O(hdmi_in_ddc_scl_i),
//  .T(hdmi_in_ddc_scl_t));
//IOBUF hdmi_in_ddc_sda_iobuf
// (.I(hdmi_in_ddc_sda_o),
//  .IO(hdmi_in_ddc_sda_io),
//  .O(hdmi_in_ddc_sda_i),
//  .T(hdmi_in_ddc_sda_t));
//// for HDMI out
//IOBUF hdmi_out_ddc_scl_iobuf
//   (.I(hdmi_out_ddc_scl_o),
//    .IO(hdmi_out_ddc_scl_io),
//    .O(hdmi_out_ddc_scl_i),
//    .T(hdmi_out_ddc_scl_t));
//IOBUF hdmi_out_ddc_sda_iobuf
//   (.I(hdmi_out_ddc_sda_o),
//    .IO(hdmi_out_ddc_sda_io),
//    .O(hdmi_out_ddc_sda_i),
//    .T(hdmi_out_ddc_sda_t));

    // HDMI OUT
    OBUFDS #(
        .IOSTANDARD("TMDS_33"),   // Specify the output I/O standard
        .SLEW("FAST")             // Specify the output slew rate
    ) OBUFDS_HDMI_TX_CLK (
        .O(hdmi_out_clk_p),       // Diff_p output (connect directly to top-level port)
        .OB(hdmi_out_clk_n),      // Diff_n output (connect directly to top-level port)
        .I(From_User_HDMI_TX_Clk) // Buffer input
    );

    OBUFDS #(
        .IOSTANDARD("TMDS_33"),  // Specify the output I/O standard
        .SLEW("FAST")            // Specify the output slew rate
    ) OBUFDS_HDMI_TX_R (
        .O(hdmi_out_data_p[2]),  // Diff_p output (connect directly to top-level port)
        .OB(hdmi_out_data_n[2]), // Diff_n output (connect directly to top-level port)
        .I(From_User_HDMI_TX_R)  // Buffer input
    );

    OBUFDS #(
        .IOSTANDARD("TMDS_33"),  // Specify the output I/O standard
        .SLEW("FAST")            // Specify the output slew rate
    ) OBUFDS_HDMI_TX_G (
        .O(hdmi_out_data_p[1]),  // Diff_p output (connect directly to top-level port)
        .OB(hdmi_out_data_n[1]), // Diff_n output (connect directly to top-level port)
        .I(From_User_HDMI_TX_G)  // Buffer input
    );

    OBUFDS #(
        .IOSTANDARD("TMDS_33"),  // Specify the output I/O standard
        .SLEW("FAST")            // Specify the output slew rate
    ) OBUFDS_HDMI_TX_B (
        .O(hdmi_out_data_p[0]),  // Diff_p output (connect directly to top-level port)
        .OB(hdmi_out_data_n[0]), // Diff_n output (connect directly to top-level port)
        .I(From_User_HDMI_TX_B)  // Buffer input
    );

// pmodJB related iobufs
    generate
        for (i=0; i < 8; i=i+1)
        begin: pmodJB_iobuf
            IOBUF pmodJB_data_iobuf_i(
                .I(pmodJB_data_out[i]),
                .IO(pmodJB[i]),
                .O(pmodJB_data_in[i]),
                .T(pmodJB_tri_out[i])
                );
        end
    endgenerate
// pmodJA related iobufs
    generate
        for (i=0; i < 8; i=i+1)
        begin: pmodJA_iobuf
            IOBUF pmodJA_data_iobuf_i(
                .I(pmodJA_data_out[i]),
                .IO(pmodJA[i]),
                .O(pmodJA_data_in[i]),
                .T(pmodJA_tri_out[i])
                );
        end
    endgenerate

// Arduino shield related iobufs
    generate
        for (i=0; i < 20; i=i+1)
        begin: gpio_shield_sw_a5_a0_iobuf
            IOBUF gpio_shield_sw_a5_a0_d13_d0_iobuf_i(
                .I(sw2shield_data_o[i]),
                .IO(gpio_shield_sw_a5_a0_d13_d0_tri_io[i]),
                .O(shield2sw_data_i[i]),
                .T(sw2shield_tri_o[i])
                );
        end
    endgenerate

// Dedicated Arduino IIC shield2sw_scl_i_in
IOBUF iic_sw_shield_scl_iobuf
     (.I(sw2shield_scl_o_out),
      .IO(iic_sw_shield_scl_io),
      .O(shield2sw_scl_i_in),
      .T(sw2shield_scl_t_out));
IOBUF iic_sw_shield_sda_iobuf
     (.I(sw2shield_sda_o_out),
      .IO(iic_sw_shield_sda_io),
      .O(shield2sw_sda_i_in),
      .T(sw2shield_sda_t_out));
// Dedicated Arduino SPI
IOBUF spi_sw_shield_io0_iobuf
     (.I(spi_sw_shield_io0_o),
      .IO(spi_sw_shield_io0_io),
      .O(spi_sw_shield_io0_i),
      .T(spi_sw_shield_io0_t));
IOBUF spi_sw_shield_io1_iobuf
     (.I(spi_sw_shield_io1_o),
      .IO(spi_sw_shield_io1_io),
      .O(spi_sw_shield_io1_i),
      .T(spi_sw_shield_io1_t));
IOBUF spi_sw_shield_sck_iobuf
     (.I(spi_sw_shield_sck_o),
      .IO(spi_sw_shield_sck_io),
      .O(spi_sw_shield_sck_i),
      .T(spi_sw_shield_sck_t));
IOBUF spi_sw_shield_ss_iobuf
     (.I(spi_sw_shield_ss_o),
      .IO(spi_sw_shield_ss_io),
      .O(spi_sw_shield_ss_i),
      .T(spi_sw_shield_ss_t));

system system_i
   (.User_Design_Clk(User_Design_Clk),
    .User_Reset(User_Reset),
    .Reset_VGA(Reset_VGA),
    .User_Design_Control(User_Design_Control),
    .addr_to_User(addr_to_user_w),
    .sprite_rom_addr_to_user(sprite_rom_addr_to_user),
    .cache_address_i(rom_addr_68k_cache),
    //.cache_burst_busy_o(),
    .cache_data_o(rom_data_68k_cache),
    .cache_data_valid_o(rom_valid_68k_cache),
    .cache_read_burst_i(rom_read_68k_cache),
    .gfx_address_i(gfx_addr),
    .gfx_tiny_burst_i(gfx_tiny_burst),
    .gfx_burst_done_o(gfx_burst_done),
    .gfx_data_o(gfx_data),
    .gfx_data_valid_o(gfx_data_valid),
    .gfx_read_i(gfx_burst_read),
    .FRAME_FIFO_READ_empty(frame_fifo_empty),
    .FRAME_FIFO_READ_rd_data(frame_fifo_data),
    .FRAME_FIFO_READ_rd_en(frame_fifo_read),
    .frame_fifo_rd_clk(frame_fifo_clk),
    .draw_hi_frame_n_lo_frame(draw_hi_frame_n_lo_frame),
    .transfer_i(fb_dma_transfer),
    //.transfer_i(dma_from_axi_linux),
    .hi_nlo_i(fb_dma_hi_nlo),
    //.hi_nlo_i(dma_hi_nlo_axi_linux),
    .fb_data_i(fb_data),
    .fb_addr_o(fb_addr),
    .fb_dma_busy_o(fb_dma_busy),
    .joystick_inputs(joystick_inputs),
    .break_point_addr(break_point_addr),
    //.BRAM_PORT_TG68_addr(tg68_addr_32b),
    .BRAM_PORT_TG68_addr(tg68_addr),
    .BRAM_PORT_TG68_clk(tg68_clk),
    .BRAM_PORT_TG68_din(tg68_data),
    .BRAM_PORT_TG68_we(tg68_writeb),
    .BRAM_PORT_TG68_dout(), // NC
    .BRAM_PORT_TG68_en(1),
    //.BRAM_PORT_TG68_rst(User_Reset), // Unused, Required ?
    .DDR_addr(DDR_addr),
    .DDR_ba(DDR_ba),
    .DDR_cas_n(DDR_cas_n),
    .DDR_ck_n(DDR_ck_n),
    .DDR_ck_p(DDR_ck_p),
    .DDR_cke(DDR_cke),
    .DDR_cs_n(DDR_cs_n),
    .DDR_dm(DDR_dm),
    .DDR_dq(DDR_dq),
    .DDR_dqs_n(DDR_dqs_n),
    .DDR_dqs_p(DDR_dqs_p),
    .DDR_odt(DDR_odt),
    .DDR_ras_n(DDR_ras_n),
    .DDR_reset_n(DDR_reset_n),
    .DDR_we_n(DDR_we_n),
    .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
    .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
    .FIXED_IO_mio(FIXED_IO_mio),
    .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
    .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
    .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
//    .Vaux0_v_n(Vaux0_v_n),
//    .Vaux0_v_p(Vaux0_v_p),
//    .Vaux12_v_n(Vaux12_v_n),
//    .Vaux12_v_p(Vaux12_v_p),
//    .Vaux13_v_n(Vaux13_v_n),
//    .Vaux13_v_p(Vaux13_v_p),
//    .Vaux15_v_n(Vaux15_v_n),
//    .Vaux15_v_p(Vaux15_v_p),
//    .Vaux1_v_n(Vaux1_v_n),
//    .Vaux1_v_p(Vaux1_v_p),
//    .Vaux5_v_n(Vaux5_v_n),
//    .Vaux5_v_p(Vaux5_v_p),
//    .Vaux6_v_n(Vaux6_v_n),
//    .Vaux6_v_p(Vaux6_v_p),
//    .Vaux8_v_n(Vaux8_v_n),
//    .Vaux8_v_p(Vaux8_v_p),
//    .Vaux9_v_n(Vaux9_v_n),
//    .Vaux9_v_p(Vaux9_v_p),
//    .Vp_Vn_v_n(Vp_Vn_v_n),
//    .Vp_Vn_v_p(Vp_Vn_v_p),
    .btns_4bits_tri_i(btns_4bits_tri_i),
//    .ck_gpio_tri_i({ck_gpio_tri_i[15],ck_gpio_tri_i[14],ck_gpio_tri_i[13],ck_gpio_tri_i[12],ck_gpio_tri_i[11],ck_gpio_tri_i[10],ck_gpio_tri_i[9],ck_gpio_tri_i[8],ck_gpio_tri_i[7],ck_gpio_tri_i[6],ck_gpio_tri_i[5],ck_gpio_tri_i[4],ck_gpio_tri_i[3],ck_gpio_tri_i[2],ck_gpio_tri_i[1],ck_gpio_tri_i[0]}),
//    .ck_gpio_tri_o({ck_gpio_tri_o[15],ck_gpio_tri_o[14],ck_gpio_tri_o[13],ck_gpio_tri_o[12],ck_gpio_tri_o[11],ck_gpio_tri_o[10],ck_gpio_tri_o[9],ck_gpio_tri_o[8],ck_gpio_tri_o[7],ck_gpio_tri_o[6],ck_gpio_tri_o[5],ck_gpio_tri_o[4],ck_gpio_tri_o[3],ck_gpio_tri_o[2],ck_gpio_tri_o[1],ck_gpio_tri_o[0]}),
//    .ck_gpio_tri_t({ck_gpio_tri_t[15],ck_gpio_tri_t[14],ck_gpio_tri_t[13],ck_gpio_tri_t[12],ck_gpio_tri_t[11],ck_gpio_tri_t[10],ck_gpio_tri_t[9],ck_gpio_tri_t[8],ck_gpio_tri_t[7],ck_gpio_tri_t[6],ck_gpio_tri_t[5],ck_gpio_tri_t[4],ck_gpio_tri_t[3],ck_gpio_tri_t[2],ck_gpio_tri_t[1],ck_gpio_tri_t[0]}),
//    .hdmi_in_clk_n(hdmi_in_clk_n),
//    .hdmi_in_clk_p(hdmi_in_clk_p),
//    .hdmi_in_data_n(hdmi_in_data_n),
//    .hdmi_in_data_p(hdmi_in_data_p),
//    .hdmi_in_ddc_scl_i(hdmi_in_ddc_scl_i),
//    .hdmi_in_ddc_scl_o(hdmi_in_ddc_scl_o),
//    .hdmi_in_ddc_scl_t(hdmi_in_ddc_scl_t),
//    .hdmi_in_ddc_sda_i(hdmi_in_ddc_sda_i),
//    .hdmi_in_ddc_sda_o(hdmi_in_ddc_sda_o),
//    .hdmi_in_ddc_sda_t(hdmi_in_ddc_sda_t),
//    .hdmi_in_hpd(hdmi_in_hpd),
//    .hdmi_out_clk_n(hdmi_out_clk_n),
//    .hdmi_out_clk_p(hdmi_out_clk_p),
//    .hdmi_out_data_n(hdmi_out_data_n),
//    .hdmi_out_data_p(hdmi_out_data_p),
//    .hdmi_out_ddc_scl_i(hdmi_out_ddc_scl_i),
//    .hdmi_out_ddc_scl_o(hdmi_out_ddc_scl_o),
//    .hdmi_out_ddc_scl_t(hdmi_out_ddc_scl_t),
//    .hdmi_out_ddc_sda_i(hdmi_out_ddc_sda_i),
//    .hdmi_out_ddc_sda_o(hdmi_out_ddc_sda_o),
//    .hdmi_out_ddc_sda_t(hdmi_out_ddc_sda_t),
//    .hdmi_out_hpd(hdmi_out_hpd),
    .leds_4bits_tri_o(leds_4bits_tri_o),
    .pdm_audio_shutdown(pdm_audio_shutdown),
    .pdm_m_clk(pdm_m_clk),
    .pdm_m_data_i(pdm_m_data_i),
//    .pmodJA_data_in(pmodJA_data_in),
//    .pmodJA_data_out(pmodJA_data_out),
//    .pmodJA_tri_out(pmodJA_tri_out),
//    .pmodJB_data_in(pmodJB_data_in),
//    .pmodJB_data_out(pmodJB_data_out),
//    .pmodJB_tri_out(pmodJB_tri_out),
    .pwm_audio_o(pwm_audio_o),
    .rgbleds_6bits_tri_o(rgbleds_6bits_tri_o),
//    .shield2sw_scl_i_in(shield2sw_scl_i_in),
//    .shield2sw_sda_i_in(shield2sw_sda_i_in),
//    .spi_sw_shield_io0_i(spi_sw_shield_io0_i),
//    .spi_sw_shield_io0_o(spi_sw_shield_io0_o),
//    .spi_sw_shield_io0_t(spi_sw_shield_io0_t),
//    .spi_sw_shield_io1_i(spi_sw_shield_io1_i),
//    .spi_sw_shield_io1_o(spi_sw_shield_io1_o),
//    .spi_sw_shield_io1_t(spi_sw_shield_io1_t),
//    .spi_sw_shield_sck_i(spi_sw_shield_sck_i),
//    .spi_sw_shield_sck_o(spi_sw_shield_sck_o),
//    .spi_sw_shield_sck_t(spi_sw_shield_sck_t),
//    .spi_sw_shield_ss_i(spi_sw_shield_ss_i),
//    .spi_sw_shield_ss_o(spi_sw_shield_ss_o),
//    .spi_sw_shield_ss_t(spi_sw_shield_ss_t),
//    .sw2shield_scl_o_out(sw2shield_scl_o_out),
//    .sw2shield_scl_t_out(sw2shield_scl_t_out),
//    .sw2shield_sda_o_out(sw2shield_sda_o_out),
//    .sw2shield_sda_t_out(sw2shield_sda_t_out),
//    .shield2sw_data_i(shield2sw_data_i),
//    .sw2shield_data_o(sw2shield_data_o),
//    .sw2shield_tri_o(sw2shield_tri_o),
    .sws_2bits_tri_i(sws_2bits_tri_i));

    assign tg68_writeb = {tg68_write, tg68_write, tg68_write, tg68_write};

    assign dma_from_axi_linux = User_Design_Control[31];

    assign dma_hi_nlo_axi_linux = User_Design_Control[30];

    always @ (posedge User_Design_Clk)
        Sync_Reset <= User_Design_Control[0];

    always @ (posedge User_Design_Clk)
        Sync_Reset_68k <= User_Design_Control[1];

    always @ (posedge User_Design_Clk)
        Sync_Enable_68k <= User_Design_Control[2];

    always @ (posedge User_Design_Clk)
        Sync_Lock_Step_68k <= User_Design_Control[3];

    always @ (posedge User_Design_Clk)
        Sync_Step_68k <= User_Design_Control[4];

    always @ (posedge User_Design_Clk)
        break_point_enabled <= User_Design_Control[5];

    always @ (posedge User_Design_Clk)
        layer_processor_enabled <= User_Design_Control[16];

    assign joystick_to_top = joystick_inputs | {28'h000000, 2'b00, sws_2bits_tri_i, btns_4bits_tri_i};

vhdl_top vhdl_top_1
    (
     // Base signals
     .clk_i(User_Design_Clk),
     //.rst_i(btns_4bits_tri_i[0]),
     .rst_i(Sync_Reset),
     // Joystick
     .joystick_inputs_i(joystick_to_top),
     // VGA Initial Reset
     .rst_vga_init_i(Reset_VGA),
     // Control signals
     .rst_68k_i(Sync_Reset_68k),
     .enable_68k_i(Sync_Enable_68k),
     .lock_step_68k_i(Sync_Lock_Step_68k),
     .step_68k_i(Sync_Step_68k),
     .break_point_addr_i(break_point_addr),
     .break_point_enabled_i(break_point_enabled),
     .base_addr_prog_rom_i(addr_to_user_w),
     .base_addr_sprite_rom_i(sprite_rom_addr_to_user),
     .base_addr_layer_rom_i('b0),
     .layer_processor_enabled_i(layer_processor_enabled),
     // Debug
     .tg68_addr_o(tg68_addr),
     .tg68_pc_o(tg68_data),
     .tg68_pcw_o(tg68_write),
     .clk_68k_o(tg68_clk),
     // Cache memory interface
     .rom_addr_68k_cache_o(rom_addr_68k_cache),
     .rom_read_68k_cache_o(rom_read_68k_cache),
     .rom_valid_68k_cache_i(rom_valid_68k_cache),
     .rom_data_68k_cache_i(rom_data_68k_cache),
     // Graphics
     .rom_addr_gfx_o(gfx_addr),
     .tiny_burst_gfx_o(gfx_tiny_burst),
     .rom_burst_read_gfx_o(gfx_burst_read),
     .rom_data_gfx_i(gfx_data),
     .rom_data_valid_gfx_i(gfx_data_valid),
     .rom_burst_done_gfx_i(gfx_burst_done),
     // Frame FIFO in
     .frame_fifo_data_i(frame_fifo_data),
     .frame_fifo_empty_i(frame_fifo_empty),
     .frame_fifo_read_o(frame_fifo_read),
     .frame_fifo_clk_o(frame_fifo_clk),
     .draw_hi_n_lo_frame_o(draw_hi_frame_n_lo_frame),
     // Frame buffer
     .frame_buffer_dma_start_o(fb_dma_transfer),
     .frame_buffer_dma_busy_i(fb_dma_busy),
     .frame_buffer_hi_nlo_o(fb_dma_hi_nlo),
     .frame_buffer_data_o(fb_data),
     .frame_buffer_addr_i(fb_addr),
     // HDMI out
     .hdmi_tx_clk_o(From_User_HDMI_TX_Clk),
     .hdmi_tx_r_o(From_User_HDMI_TX_R),
     .hdmi_tx_g_o(From_User_HDMI_TX_G),
     .hdmi_tx_b_o(From_User_HDMI_TX_B)//,
     //.hdmi_out_ddc_scl_i(hdmi_out_ddc_scl_i),
     //.hdmi_out_ddc_scl_o(hdmi_out_ddc_scl_o),
     //.hdmi_out_ddc_scl_t(hdmi_out_ddc_scl_t),
     //.hdmi_out_ddc_sda_i(hdmi_out_ddc_sda_i),
     //.hdmi_out_ddc_sda_o(hdmi_out_ddc_sda_o),
     //.hdmi_out_ddc_sda_t(hdmi_out_ddc_sda_t),
     //.hdmi_out_hpd_i(hdmi_out_hpd),
     );

    // Address conversion to 32-bit (two unused bits).
    assign tg68_addr_32b = {1'b0, 16'h0000, tg68_addr, 2'b00};

endmodule

`default_nettype wire
