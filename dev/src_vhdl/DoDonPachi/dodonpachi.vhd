-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : dodonpachi.vhd
-- Description  : Dodonpachi - contains :
--                The Main Processor a motorola 68000
--                The RAMs
--                The Graphic Hardware (sprites and layers)
--
-- Author       : Rick Wertenbroek
-- Version      : 0.2
--
-- Dependencies : All of its contents
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.dodonpachi_pkg.all;

entity dodonpachi is
    generic (
        INCLUDE_GRAPHIC_PROCESSOR_G : boolean := true;
        INCLUDE_EEPROM_G            : boolean := false;
        EXPORT_WATCH_SIGNALS_G      : boolean := true);
    port (
        -- Standard signals
        clk_fast_i            : in  std_logic;
        rst_i                 : in  std_logic;
        -- 68k signals
        enable_68k_i          : in  std_logic                     := '1';
        clk_68k_i             : in  std_logic;
        rst_68k_i             : in  std_logic;
        -- Joystick
        joystick_inputs_i     : in  std_logic_vector(31 downto 0);
        -- 68k ROM
        rom_addr_68k_o        : out std_logic_vector(DDP_ROM_LOG_SIZE_C-1 downto 0);
        rom_read_68k_o        : out std_logic;
        rom_valid_68k_i       : in  std_logic;
        rom_data_68k_i        : in  std_logic_vector(DDP_WORD_WIDTH-1 downto 0);
        -- GFX signals (clocked with clk_fast)
        rom_addr_gfx_o        : out std_logic_vector(31 downto 0);
        tiny_burst_gfx_o      : out std_logic;
        rom_burst_read_gfx_o  : out std_logic;
        rom_data_valid_gfx_i  : in  std_logic;
        rom_data_gfx_i        : in  std_logic_vector(31 downto 0);
        rom_burst_done_gfx_i  : in  std_logic;
        -- Frame Buffer
        frame_buffer_addr_o   : out frame_buffer_addr_t;
        frame_buffer_color_o  : out color_t;
        frame_buffer_write_o  : out std_logic;
        -- Frame Buffer DMA
        start_fb_dma_o        : out std_logic;
        -- Video signal for IRQ generation
        frame_i               : in  std_logic;
        -- Debug signals
        command_bus_i         : in  command_bus_t;
        lock_step_i           : in  std_logic                     := '0';
        step_i                : in  std_logic                     := '0';
        break_point_addr_i    : in  std_logic_vector(31 downto 0) := (others => '0');
        break_point_enabled_i : in  std_logic                     := '0';
        mem_bus_68k_o         : out std_logic_vector(31 downto 0);
        TG68_PC_o             : out std_logic_vector(31 downto 0);
        TG68_PCW_o            : out std_logic;
        watch_bus_o           : out watch_bus_t
        );
end entity dodonpachi;

architecture struct of dodonpachi is

    signal lock_step_s                     : std_logic := '0';
    signal break_at_point_s                : std_logic;

    -- Processor related signals
    signal enable_68k_s                    : std_logic;
    signal rst_68k_s                       : std_logic;
    signal n_rst_68k_s                     : std_logic;
    signal n_data_ack_68k_s                : std_logic;
    signal addr_68k_s                      : std_logic_vector(31 downto 0);  -- only 24 MSB bits used
    signal data_out_68k_s                  : std_logic_vector(15 downto 0);
    signal n_addr_strobe_68k_s             : std_logic;
    signal addr_strobe_68k_s               : std_logic;
    signal addr_strobe_68k_old_reg_s       : std_logic;
    signal n_upper_data_select_68k_s       : std_logic;
    signal upper_data_select_68k_s         : std_logic;
    signal upper_data_select_68k_old_reg_s : std_logic;
    signal n_lower_data_select_68k_s       : std_logic;
    signal lower_data_select_68k_s         : std_logic;
    signal lower_data_select_68k_old_reg_s : std_logic;
    signal read_n_write_68k_s              : std_logic;
    signal drive_data_68k_s                : std_logic;
    signal n_ipl_to_68k_s                  : std_logic_vector(2 downto 0);
    signal ipl_s                           : std_logic_vector(2 downto 0);
    signal other_ack_s                     : std_logic;
    signal other_data_o_s                  : std_logic_vector(15 downto 0);

    -- Memory bus related signals
    signal memory_bus_ack_s         : std_logic;
    signal memory_bus_data_s        : std_logic_vector(15 downto 0);
    signal read_strobe_s            : std_logic;
    signal write_strobe_s           : std_logic;
    signal high_write_strobe_s      : std_logic;
    signal low_write_strobe_s       : std_logic;
    -- ROM
    constant ROM_LOG_SIZE_C         : natural := DDP_ROM_LOG_SIZE_C;  -- 1MB
    signal rom_ack_s                : std_logic;
    signal rom_data_o_s             : std_logic_vector(15 downto 0);
    signal rom_enable_s             : std_logic;
    -- RAM
    constant RAM_LOG_SIZE_C         : natural := 16;  -- 64kB
    signal ram_enable_s             : std_logic;
    signal ram_ack_s                : std_logic;
    signal ram_data_o_s             : std_logic_vector(15 downto 0);
    -- YMZ RAM
    constant YMZ_RAM_LOG_SIZE_C     : natural := 2;   -- 4B
    signal ymz_ram_enable_s         : std_logic;
    signal ymz_ram_ack_s            : std_logic;
    signal ymz_ram_data_o_s         : std_logic_vector(15 downto 0);
    -- Sprite RAM
    constant SPRITE_RAM_LOG_SIZE_C  : natural := 16;  -- 64kB
    signal sprite_ram_enable_s      : std_logic;
    signal sprite_ram_ack_s         : std_logic;
    signal sprite_ram_data_o_s      : std_logic_vector(15 downto 0);
    -- Layer 0 RAM
    constant LAYER_0_RAM_LOG_SIZE_C : natural := 15;  -- 32kB
    signal layer_0_ram_enable_s     : std_logic;
    signal layer_0_ram_ack_s        : std_logic;
    signal layer_0_ram_data_o_s     : std_logic_vector(15 downto 0);
    -- Layer 1 RAM
    constant LAYER_1_RAM_LOG_SIZE_C : natural := 15;  -- 32kB
    signal layer_1_ram_enable_s     : std_logic;
    signal layer_1_ram_ack_s        : std_logic;
    signal layer_1_ram_data_o_s     : std_logic_vector(15 downto 0);
    -- Layer 2 RAM
    constant LAYER_2_RAM_LOG_SIZE_C : natural := 16;  -- 64kB
    signal layer_2_ram_enable_s     : std_logic;
    signal layer_2_ram_ack_s        : std_logic;
    signal layer_2_ram_data_o_s     : std_logic_vector(15 downto 0);
    -- Video Registers (should not necessarily be ram) (write only)
    constant VIDEO_REGS_LOG_SIZE_C  : natural := 7;   -- 128B
    signal video_regs_enable_s      : std_logic;
    signal video_regs_ack_s         : std_logic;
    -- IRQ Cause (read only)
    signal irq_cause_enable_s       : std_logic;
    signal irq_cause_ack_s          : std_logic;
    signal irq_cause_data_o_s       : std_logic_vector(15 downto 0);
    -- Video Control Registers 0
    constant V_CTRL_0_LOG_SIZE_C    : natural := 3;   -- 8B (actually 6B)
    signal v_ctrl_0_enable_s        : std_logic;
    signal v_ctrl_0_ack_s           : std_logic;
    signal v_ctrl_0_data_o_s        : std_logic_vector(15 downto 0);
    -- Video Control Registers 1
    constant V_CTRL_1_LOG_SIZE_C    : natural := 3;   -- 8B (actually 6B)
    signal v_ctrl_1_enable_s        : std_logic;
    signal v_ctrl_1_ack_s           : std_logic;
    signal v_ctrl_1_data_o_s        : std_logic_vector(15 downto 0);
    -- Video Control Registers 2
    constant V_CTRL_2_LOG_SIZE_C    : natural := 3;   -- 8B (actually 6B)
    signal v_ctrl_2_enable_s        : std_logic;
    signal v_ctrl_2_ack_s           : std_logic;
    signal v_ctrl_2_data_o_s        : std_logic_vector(15 downto 0);
    -- Palette RAM
    constant PALETTE_RAM_LOG_SIZE_C : natural := 16;  -- 64kB
    signal palette_ram_enable_s     : std_logic;
    signal palette_ram_ack_s        : std_logic;
    signal palette_ram_data_o_s     : std_logic_vector(15 downto 0);
    -- Inputs 0
    constant IN_0_LOG_SIZE_C        : natural := 1;   -- 2B
    signal in_0_enable_s            : std_logic;
    signal in_0_ack_s               : std_logic;
    signal in_0_data_o_s            : std_logic_vector(15 downto 0);
    -- Inputs 1
    constant IN_1_LOG_SIZE_C        : natural := 1;   -- 2B
    signal in_1_enable_s            : std_logic;
    signal in_1_ack_s               : std_logic;
    signal in_1_data_o_s            : std_logic_vector(15 downto 0);
    -- EEPROM
    constant EEPROM_LOG_SIZE_C      : natural := 1;   -- 2B
    signal eeprom_enable_s          : std_logic;
    signal eeprom_ack_s             : std_logic;
    signal eeprom_data_o_s          : std_logic_vector(15 downto 0);
    signal eeprom_ci_s              : std_logic;
    signal eeprom_cs_s              : std_logic;
    signal eeprom_di_s              : std_logic;
    signal eeprom_do_s              : std_logic;
    -- Edge Cases
    signal edge_case_enable_s       : std_logic;
    signal edge_case_ack_s          : std_logic;
    -- No data for edge cases (will be 0 since the bus is OR'ed)

    signal addr_68k_32b_s           : std_logic_vector(31 downto 0);

begin

    -----------------------
    -- IO with top level --
    -----------------------
    rom_addr_68k_o <= addr_68k_s(ROM_LOG_SIZE_C-1 downto 0);
    rom_read_68k_o <= rom_enable_s and read_strobe_s;
    rom_ack_s      <= rom_valid_68k_i;
    -- The data bus is an OR'ed bus, so we need to ensure that the data from
    -- the ROM section does not interfere when the ROM is not enabled.
    rom_data_o_s <= rom_data_68k_i when rom_enable_s else
                    (others => '0');

    -- Step by step
    step_block : block
        signal step_old_s : std_logic;
    begin

        process(clk_68k_i) is
        begin
            if rising_edge(clk_68k_i) then
                if rst_68k_s = '1' then
                    step_old_s <= '0';
                    lock_step_s <= '0';
                else
                    step_old_s <= step_i;

                    -- Enable the 68k on a rising edge of enable
                    if step_old_s = '0' and step_i = '1' then
                        -- Unlock a step by rising on step_i
                        lock_step_s <= '0';
                    elsif memory_bus_ack_s = '1' then
                        -- The CPU will lock after each memory access
                        lock_step_s <= '1';
                    end if;
                end if;
            end if;
        end process;

    end block step_block;

    -- Break Point Mechanism
    break_point_block : block
        signal break_point_addr_s    : std_logic_vector(addr_68k_s'length-1 downto 0);
        signal break_point_enabled_s : std_logic;
    begin

        process(clk_68k_i) is
        begin
            if rising_edge(clk_68k_i) then
                break_point_addr_s    <= break_point_addr_i;
                break_point_enabled_s <= break_point_enabled_i;
                if rst_68k_s = '1' then
                    break_at_point_s <= '0';
                else
                    if (break_point_enabled_s = '1') and (addr_68k_s = break_point_addr_s) then
                        break_at_point_s <= '1';
                    elsif (break_point_enabled_s = '0') then
                        break_at_point_s <= '0';
                    end if;
                end if;
            end if;
        end process;

    end block break_point_block;

    -- Sync
    process(clk_68k_i) is
    begin
        if rising_edge(clk_68k_i) then
            rst_68k_s    <= rst_68k_i;
            enable_68k_s <= enable_68k_i;
        end if;
    end process;

    -- Debug
    process(clk_68k_i) is
    begin
        if rising_edge(clk_68k_i) then
            if addr_strobe_68k_s = '1' then
                mem_bus_68k_o <= addr_68k_s;
            end if;
        end if;
    end process;

    -------------------
    -- Interruptions --
    -------------------

    -- This only simulates the V-Blank IRQ for now (TODO : Other IRQs and ACKs)
    interrupt_block : block
        signal v_blank_counter_s : unsigned(18 downto 0);
        signal sync_reg_s        : std_logic_vector(3 downto 0);
        signal frame_changed_s   : std_logic;
    begin

        -- Synchronisation shift register
        sync_process : process(clk_68k_i) is
        begin
            if rising_edge(clk_68k_i) then
                sync_reg_s(0) <= frame_i;
                for i in 1 to sync_reg_s'high loop
                    sync_reg_s(i) <= sync_reg_s(i-1);
                end loop;
            end if;
        end process sync_process;

        -- The frame changed when there is an edge on frame_i
        frame_changed_s <= sync_reg_s(sync_reg_s'high) xor sync_reg_s(sync_reg_s'high-1);

        interrupt_process : process(clk_68k_i) is
        begin
            if rising_edge(clk_68k_i) then
                if rst_68k_i = '1' then
                    ipl_s         <= (others => '0');
                else
                    if frame_changed_s = '1' then
                        ipl_s     <= "001";
                    else
                        if (read_strobe_s = '1') and (addr_68k_s(23 downto 0) = x"800004") then
                            -- The 68k acknowledged the IRQ (I am not sure
                            -- about this...)
                            ipl_s <= (others => '0');
                        end if; -- IRQ Ack
                    end if; -- IRQ fired
                end if; -- Reset
            end if; -- Rising Edge Clock
        end process interrupt_process;

        -- Test for interrupts
        process(clk_68k_i) is
        begin
            if rising_edge(clk_68k_i) then
                other_ack_s    <= '0';
                other_data_o_s <= (others => '0');
                if (read_strobe_s = '1') and (addr_68k_s(addr_68k_s'high downto addr_68k_s'high-3) = x"F") then
                    other_ack_s    <= '1';
                    --other_data_o_s <= x"0019"; -- The 68k uses auto-vectorized interrupts
                end if;
            end if;
        end process;

    end block interrupt_block;

    --------------------
    -- Main Processor --
    --------------------

    -- The main 68000 processor uses negative logic, since we use positive
    -- logic in our design we need to invert some signals.

    -- Switch from positive logic
    n_rst_68k_s      <= not rst_68k_s;
    n_data_ack_68k_s <= not memory_bus_ack_s;
    n_ipl_to_68k_s   <= not ipl_s;

    -- The main 68000 processor
    main_68k : entity work.TG68
        port map (
            clk        => clk_68k_i,
            reset      => n_rst_68k_s,
            clkena_in  => enable_68k_s,
            data_in    => memory_bus_data_s,
            IPL        => n_ipl_to_68k_s,
            dtack      => n_data_ack_68k_s,
            addr       => addr_68k_32b_s,
            data_out   => data_out_68k_s,
            as         => n_addr_strobe_68k_s,
            uds        => n_upper_data_select_68k_s,
            lds        => n_lower_data_select_68k_s,
            rw         => read_n_write_68k_s,
            drive_data => drive_data_68k_s, -- Doesn't seem to be used
            TG68_PC_o  => TG68_PC_o,
            TG68_PCW_o => TG68_PCW_o);

    -- Mask the high byte since Dodonpachi only uses a 24 bit address bus,
    -- sometimes the CPU will have the upper 8 bits set due to an operation
    -- and this will lock the CPU because we are outside of the address space
    -- therefore no ack is generated, this mask makes sure the CPU only
    -- addresses 24 bits.
    --addr_68k_s <= x"00" & addr_68k_32b_s(23 downto 0); -- For some reason
    -- this deadlocks the CPU at instruction 0x57594, it could be that the TG68
    -- is written so that if the MSB byte is not used some signals get pruned
    -- and some address computation fail. (No idea for now).
    addr_68k_s <= addr_68k_32b_s;

    -- Switch to positive logic
    addr_strobe_68k_s       <= (not n_addr_strobe_68k_s) and (not (lock_step_s and lock_step_i)) and (not (break_at_point_s));
    upper_data_select_68k_s <= not n_upper_data_select_68k_s;
    lower_data_select_68k_s <= not n_lower_data_select_68k_s;

    -- TODO : Add support for byte access to RAM, check if unaligned accesses
    -- to a byte in RAM do use the uds/lds signals, and add the support for
    -- this in the RAM module, since there are accesses to bytes in RAM e.g.
    -- addq.b 1, <ram_address> in the program code. Check if these are done
    -- right.

    ---------------------
    -- Main Memory Bus --
    ---------------------

    -- TODO : Write an assertion to check that only one is active at any given
    -- time !

    -- This is an OR'ed bus
    memory_bus_ack_s <= rom_ack_s         or
                        ram_ack_s         or
                        ymz_ram_ack_s     or
                        sprite_ram_ack_s  or
                        layer_0_ram_ack_s or
                        layer_1_ram_ack_s or
                        layer_2_ram_ack_s or
                        video_regs_ack_s  or
                        irq_cause_ack_s   or
                        v_ctrl_0_ack_s    or
                        v_ctrl_1_ack_s    or
                        v_ctrl_2_ack_s    or
                        palette_ram_ack_s or
                        in_0_ack_s        or
                        in_1_ack_s        or
                        eeprom_ack_s      or
                        edge_case_ack_s   or
                        other_ack_s;

    -- TODO : Write an assertion to check that only one is active at any given
    -- time !

    -- "OR" everything together to create the "OR'ed" bus
    memory_bus_data_s <= rom_data_o_s         or
                         ram_data_o_s         or
                         ymz_ram_data_o_s     or
                         sprite_ram_data_o_s  or
                         layer_0_ram_data_o_s or
                         layer_1_ram_data_o_s or
                         layer_2_ram_data_o_s or
                         irq_cause_data_o_s   or
                         v_ctrl_0_data_o_s    or
                         v_ctrl_1_data_o_s    or
                         v_ctrl_2_data_o_s    or
                         palette_ram_data_o_s or
                         in_0_data_o_s        or
                         in_1_data_o_s        or
                         eeprom_data_o_s      or
                         other_data_o_s;

    -- This assertion is to check that therer is no byte access to any other
    -- memory than the RAM or ROM - TODO Change this, since byte read is OK on
    -- other devices
    --assert ((ram_enable_s = '0' and rom_enable_s = '0') and (not ((upper_data_select_68k_s = '1') xor (lower_data_select_68k_s = '1')))) or (ram_enable_s = '1' or rom_enable_s = '1') report "Byte access to a memory that is not RAM !" severity error;

    -- We register the address strobe in order to detect when it is asserted in
    -- order to make a single clock read/write strobe below
    addr_strobe_reg_process : process(clk_68k_i) is
    begin
        if rising_edge(clk_68k_i) then
            if rst_68k_s = '1' then
                addr_strobe_68k_old_reg_s       <= '0';
                upper_data_select_68k_old_reg_s <= '0';
                lower_data_select_68k_old_reg_s <= '0';
            else
                addr_strobe_68k_old_reg_s       <= addr_strobe_68k_s;
                upper_data_select_68k_old_reg_s <= upper_data_select_68k_s;
                lower_data_select_68k_old_reg_s <= lower_data_select_68k_s;
            end if;
        end if;
    end process addr_strobe_reg_process;

    -- These strobes indicate a read or write operation from the processor
    read_strobe_s       <= addr_strobe_68k_s and (not addr_strobe_68k_old_reg_s) and read_n_write_68k_s;
    write_strobe_s      <= addr_strobe_68k_s and (not addr_strobe_68k_old_reg_s) and (not read_n_write_68k_s);
    high_write_strobe_s <= upper_data_select_68k_s and (not upper_data_select_68k_old_reg_s) and (not read_n_write_68k_s);
    low_write_strobe_s  <= lower_data_select_68k_s and (not lower_data_select_68k_old_reg_s) and (not read_n_write_68k_s);

    -- Memory bus decode logic - Dodonpachi Address Map -- TODO Change to constants
    ---------------------------------------------------

    -- ROM                  0x000000 - 0x0fffff
    rom_enable_s         <= '1' when addr_68k_s(31 downto ROM_LOG_SIZE_C) = x"000" else
                            '0';
    -- RAM                  0x100000 - 0x10ffff
    ram_enable_s         <= '1' when addr_68k_s(31 downto RAM_LOG_SIZE_C) = x"0010" else
                            '0';
    -- YMZ RAM              0x300000 - 0x300003
    ymz_ram_enable_s     <= '1' when addr_68k_s(31 downto YMZ_RAM_LOG_SIZE_C) = x"0030000" & "00" else
                            '0';
    -- Sprite RAM           0x400000 - 0x40ffff
    sprite_ram_enable_s  <= '1' when addr_68k_s(31 downto SPRITE_RAM_LOG_SIZE_C) = x"0040" else
                            '0';
    -- Layer 0 RAM          0x500000 - 0x507fff
    layer_0_ram_enable_s <= '1' when addr_68k_s(31 downto LAYER_0_RAM_LOG_SIZE_C) = x"0050" & "0" else
                            '0';
    -- Layer 1 RAM          0x600000 - 0x607fff
    layer_1_ram_enable_s <= '1' when addr_68k_s(31 downto LAYER_1_RAM_LOG_SIZE_C) = x"0060" & "0" else
                            '0';
    -- Layer 2 RAM          0x700000 - 0x70ffff
    layer_2_ram_enable_s <= '1' when addr_68k_s(31 downto LAYER_2_RAM_LOG_SIZE_C) = x"0070" else
                            '0';
    -- Video Registers (should not necessarily be ram) (maybe remove read/write
    -- check redundancy).   0x800000 - 0x80007f
    video_regs_enable_s  <= '1' when (addr_68k_s(31 downto VIDEO_REGS_LOG_SIZE_C) = x"008000" & "0") and (read_n_write_68k_s = '0') else
                            '0';
    -- IRQ Cause (same about redundancy)
    --                      0x800000 - 0x800007
    irq_cause_enable_s   <= '1' when (addr_68k_s(31 downto 3) = x"0080000" & "0") and (read_n_write_68k_s = '1') else
                            '0';
    -- Video Control Registers 0
    --                      0x900000 - 0x900005
    v_ctrl_0_enable_s    <= '1' when addr_68k_s(31 downto V_CTRL_0_LOG_SIZE_C) = x"0090000" & "0" else
                            '0';
    -- Video Control Registers 1
    --                      0xa00000 - 0xa00005
    v_ctrl_1_enable_s    <= '1' when addr_68k_s(31 downto V_CTRL_1_LOG_SIZE_C) = x"00a0000" & "0" else
                            '0';
    -- Video Control Registers 2
    --                      0xb00000 - 0xb00005
    v_ctrl_2_enable_s    <= '1' when addr_68k_s(31 downto V_CTRL_2_LOG_SIZE_C) = x"00b0000" & "0" else
                            '0';
    -- Palette RAM          0xc00000 - 0xc0ffff
    palette_ram_enable_s <= '1' when addr_68k_s(31 downto PALETTE_RAM_LOG_SIZE_C) = x"00c0" else
                            '0';
    -- Inputs 0             0xd00000 - 0xd00001
    in_0_enable_s        <= '1' when addr_68k_s(31 downto IN_0_LOG_SIZE_C) = x"00d0000" & "000" else
                            '0';
    -- Inputs 1             0xd00000 - 0xd00003
    in_1_enable_s        <= '1' when addr_68k_s(31 downto IN_1_LOG_SIZE_C) = x"00d0000" & "001" else
                            '0';
    -- EEPROM               0xe00000 - 0xe00001
    eeprom_enable_s      <= '1' when addr_68k_s(31 downto EEPROM_LOG_SIZE_C) = x"00e0000" & "000" else
                            '0';
    -- Edge Cases
    edge_case_enable_s   <= '1' when addr_68k_s(23 downto 16) = x"5f" else
                            '0';
    -- Access to 0x5fxxxx appears in dodonpachi on attract loop when showing
    -- the air stage on frame 9355 i.e., after roughly 2 min 30 sec
    -- The game is accessing data relative to a Layer 1 address and underflows,
    -- these accesses do nothing but should be acknowledged in order not to
    -- block de CPU.
    -- The reason these accesses appear is probably because it made the layer
    -- update routine simpler to write (no need to handle edge cases) and
    -- these accesses are simply ignored by the hardware.

    --------------
    -- Main ROM --
    --------------
    -- Main ROM is external

    --------------
    -- Main RAM --
    --------------
    main_ram_1 : entity work.main_ram
        port map (
            clk_i               => clk_68k_i,
            enable_i            => ram_enable_s,
            write_low_i         => high_write_strobe_s,
            write_high_i        => low_write_strobe_s,
            read_low_i          => read_strobe_s,
            read_high_i         => read_strobe_s,
            addr_i              => addr_68k_s(RAM_LOG_SIZE_C-1 downto 0),  -- There are
                                                                           -- unaligned
                                                                           -- byte accesses
            data_i              => data_out_68k_s,
            data_o              => ram_data_o_s,
            ack_o               => ram_ack_s);

    -------------
    -- YMZ280b --
    -------------
    -- This is not supposed to infer a BRAM, TODO : check resource usage after
    -- synthesis.
    ymz280b_block : block
        signal this_data_will_be_read_from_the_sound_processor : std_logic_vector(15 downto 0);
    begin
        ymz280b_regs : entity work.dual_port_ram
            port map (
                clk_68k_i   => clk_68k_i,
                enable_i    => ymz_ram_enable_s,
                write_i     => write_strobe_s,
                read_i      => read_strobe_s,
                addr_i      => addr_68k_s(YMZ_RAM_LOG_SIZE_C-1 downto 1),
                data_i      => data_out_68k_s,
                data_o      => ymz_ram_data_o_s,
                ack_o       => ymz_ram_ack_s,
                clk_fast_i  => clk_fast_i,
                addr_fast_i => std_logic_vector(to_unsigned(0, YMZ_RAM_LOG_SIZE_C-1)),
                data_fast_o => this_data_will_be_read_from_the_sound_processor);
    end block ymz280b_block;

    graphic_processor_block : block
        signal generate_frame_s         : std_logic;
        signal buffer_select_s          : std_logic;
        signal gfx_processor_busy_s     : std_logic;
        -- Sprite signals
        signal gfx_sprite_ram_addr_s    : sprite_ram_info_access_t;
        signal gfx_sprite_ram_info_s    : sprite_ram_line_t;
        -- Layer signals
        signal gfx_layer_0_ram_addr_s   : layer_ram_info_access_t;
        signal gfx_layer_1_ram_addr_s   : layer_ram_info_access_t;
        signal gfx_layer_2_ram_addr_s   : layer_ram_info_access_t;
        signal gfx_layer_0_ram_info_s   : layer_ram_line_t;
        signal gfx_layer_1_ram_info_s   : layer_ram_line_t;
        signal gfx_layer_2_ram_info_s   : layer_ram_line_t;
        signal gfx_vctrl_0_reg_s        : layer_info_line_t;
        signal gfx_vctrl_1_reg_s        : layer_info_line_t;
        signal gfx_vctrl_2_reg_s        : layer_info_line_t;
        -- Palette signals
        signal gfx_palette_ram_addr_s   : palette_ram_addr_t;
        signal gfx_palette_ram_data_s   : std_logic_vector(DDP_WORD_WIDTH-1 downto 0);
    begin

        graphic_processor_generate : if INCLUDE_GRAPHIC_PROCESSOR_G generate

            -----------------------
            -- Graphic Processor --
            -----------------------
            graphic_processor_inst : entity work.graphic_processor
                generic map (
                    INCLUDE_LAYER_PROCESOR_G => true)
                port map (
                    clk_i                 => clk_fast_i,
                    rst_i                 => rst_i,
                    --
                    generate_frame_i      => generate_frame_s,
                    buffer_select_i       => buffer_select_s,
                    busy_o                => gfx_processor_busy_s,
                    layer_processor_en_i  => command_bus_i.layers_enabled,
                    --
                    sprite_ram_addr_o     => gfx_sprite_ram_addr_s,
                    sprite_ram_info_i     => gfx_sprite_ram_info_s,
                    --
                    layer_0_ram_addr_o    => gfx_layer_0_ram_addr_s,
                    layer_0_ram_info_i    => gfx_layer_0_ram_info_s,
                    --
                    layer_1_ram_addr_o    => gfx_layer_1_ram_addr_s,
                    layer_1_ram_info_i    => gfx_layer_1_ram_info_s,
                    --
                    layer_2_ram_addr_o    => gfx_layer_2_ram_addr_s,
                    layer_2_ram_info_i    => gfx_layer_2_ram_info_s,
                    --
                    vctrl_reg_0_i         => gfx_vctrl_0_reg_s,
                    vctrl_reg_1_i         => gfx_vctrl_1_reg_s,
                    vctrl_reg_2_i         => gfx_vctrl_2_reg_s,
                    --
                    rom_addr_o            => rom_addr_gfx_o,
                    tiny_burst_gfx_o      => tiny_burst_gfx_o,
                    rom_burst_read_o      => rom_burst_read_gfx_o,
                    rom_data_i            => rom_data_gfx_i,
                    rom_data_valid_i      => rom_data_valid_gfx_i,
                    rom_data_burst_done_i => rom_burst_done_gfx_i,
                    --
                    palette_ram_addr_o    => gfx_palette_ram_addr_s,
                    palette_ram_data_i    => gfx_palette_ram_data_s,
                    --
                    frame_buffer_addr_o   => frame_buffer_addr_o,
                    frame_buffer_color_o  => frame_buffer_color_o,
                    frame_buffer_write_o  => frame_buffer_write_o,
                    --
                    start_fb_dma_o        => start_fb_dma_o);

        else generate

            gfx_sprite_ram_addr_s  <= (others => '0');
            rom_addr_gfx_o         <= (others => '0');
            rom_burst_read_gfx_o   <= '0';
            gfx_palette_ram_addr_s <= (others => '0');
            frame_buffer_addr_o    <= (others => '0');
            frame_buffer_color_o   <= (others => (others => '0'));
            frame_buffer_write_o   <= '0';

        end generate graphic_processor_generate;

        ----------------
        -- Sprite RAM --
        ----------------
        --sprite_ram : entity work.dual_port_ram
        --    port map (
        --        clk_68k_i   => clk_68k_i,
        --        enable_i    => sprite_ram_enable_s,
        --        write_i     => write_strobe_s,
        --        read_i      => read_strobe_s,
        --        addr_i      => addr_68k_s(SPRITE_RAM_LOG_SIZE_C-1 downto 1),
        --        data_i      => data_out_68k_s,
        --        data_o      => sprite_ram_data_o_s,
        --        ack_o       => sprite_ram_ack_s,
        --        clk_fast_i  => clk_fast_i,
        --        addr_fast_i => gfx_sprite_ram_addr_s,
        --        data_fast_o => gfx_sprite_ram_info_s);
        sprite_ram : entity work.dual_access_ram
            port map (
                clk_68k_i    => clk_68k_i,
                enable_i     => sprite_ram_enable_s,
                write_low_i  => low_write_strobe_s,
                write_high_i => high_write_strobe_s,
                read_low_i   => read_strobe_s,
                read_high_i  => read_strobe_s,
                addr_i       => addr_68k_s(SPRITE_RAM_LOG_SIZE_C-1 downto 0),
                data_i       => data_out_68k_s,
                data_o       => sprite_ram_data_o_s,
                ack_o        => sprite_ram_ack_s,
                clk_fast_i   => clk_fast_i,
                addr_fast_i  => gfx_sprite_ram_addr_s,
                data_fast_o  => gfx_sprite_ram_info_s);

        -----------------
        -- Layer 0 RAM --
        -----------------
        layer_0_ram : entity work.dual_port_ram
            port map (
                clk_68k_i   => clk_68k_i,
                enable_i    => layer_0_ram_enable_s,
                write_i     => write_strobe_s,
                read_i      => read_strobe_s,
                addr_i      => addr_68k_s(LAYER_0_RAM_LOG_SIZE_C-1 downto 1),
                data_i      => data_out_68k_s,
                data_o      => layer_0_ram_data_o_s,
                ack_o       => layer_0_ram_ack_s,
                clk_fast_i  => clk_fast_i,
                -- Do not use the MSB bit because this RAM is 32kB and
                -- address is for 64kB
                addr_fast_i => std_logic_vector(gfx_layer_0_ram_addr_s(gfx_layer_0_ram_addr_s'high-1 downto 0)),
                data_fast_o => gfx_layer_0_ram_info_s);

        -----------------
        -- Layer 1 RAM --
        -----------------
        layer_1_ram : entity work.dual_port_ram
            port map (
                clk_68k_i   => clk_68k_i,
                enable_i    => layer_1_ram_enable_s,
                write_i     => write_strobe_s,
                read_i      => read_strobe_s,
                addr_i      => addr_68k_s(LAYER_1_RAM_LOG_SIZE_C-1 downto 1),
                data_i      => data_out_68k_s,
                data_o      => layer_1_ram_data_o_s,
                ack_o       => layer_1_ram_ack_s,
                clk_fast_i  => clk_fast_i,
                -- Do not use the MSB bit because this RAM is 32kB and
                -- address is for 64kB
                addr_fast_i => std_logic_vector(gfx_layer_1_ram_addr_s(gfx_layer_1_ram_addr_s'high-1 downto 0)),
                data_fast_o => gfx_layer_1_ram_info_s);

        -----------------
        -- Layer 2 RAM --
        -----------------
        layer_2_ram_block : block
            signal addr_to_layer_2_ram_s : std_logic_vector(LAYER_2_RAM_LOG_SIZE_C-2 downto 0);
        begin

            -- This is actually mirrored RAM
            -- https://github.com/mamedev/mame/blob/master/src/mame/drivers/cave.cpp#L442
            addr_to_layer_2_ram_s(12 downto 0)                          <= addr_68k_s(13 downto 1);
            addr_to_layer_2_ram_s(addr_to_layer_2_ram_s'high downto 13) <= (others => '0');

        layer_2_ram : entity work.dual_port_ram
        --layer_2_ram : entity work.layer2_ram
            port map (
                clk_68k_i   => clk_68k_i,
                enable_i    => layer_2_ram_enable_s,
                write_i     => write_strobe_s,
                read_i      => read_strobe_s,
                --addr_i      => addr_68k_s(LAYER_2_RAM_LOG_SIZE_C-1 downto 1),
                addr_i      => addr_to_layer_2_ram_s,
                data_i      => data_out_68k_s,
                data_o      => layer_2_ram_data_o_s,
                ack_o       => layer_2_ram_ack_s,
                clk_fast_i  => clk_fast_i,
                addr_fast_i => std_logic_vector(gfx_layer_2_ram_addr_s),
                data_fast_o => gfx_layer_2_ram_info_s);

        end block layer_2_ram_block;

        ---------------------
        -- Video Registers --
        ---------------------
        -- Could be changed to a more specific component since most of them are not
        -- needed
        video_regs_block : block
            signal unused_data_out1_s : std_logic_vector(15 downto 0);
            signal video_reg_4_s      : std_logic_vector(15 downto 0);
        begin
            video_regs : entity work.dual_port_ram
                port map (
                    clk_68k_i   => clk_68k_i,
                    enable_i    => video_regs_enable_s,
                    write_i     => write_strobe_s,
                    read_i      => '0', -- Are write only from CPU
                    addr_i      => addr_68k_s(VIDEO_REGS_LOG_SIZE_C-1 downto 1),
                    data_i      => data_out_68k_s,
                    data_o      => unused_data_out1_s, -- Write only
                    ack_o       => video_regs_ack_s,
                    clk_fast_i  => clk_fast_i,
                    addr_fast_i => std_logic_vector(to_unsigned(4, VIDEO_REGS_LOG_SIZE_C-1)),
                    data_fast_o => video_reg_4_s);

            -- TODO: Check this
            buffer_select_s <= video_reg_4_s(0);

            sync_generate_frame_block : block
                signal sync_reg_s            : std_logic_vector(2 downto 0);
                signal start_frame_gen_reg_s : std_logic;
            begin
                -- Register at 68k side which indicates we need to start
                -- drawing a frame
                process (clk_68k_i) is
                begin
                    if rising_edge(clk_68k_i) then
                        if rst_68k_s = '1' then
                            start_frame_gen_reg_s <= '0';
                        else
                            -- If video reg at 0x800004 is written with 0x1F0
                            -- the graphic co-processor should start drawing a
                            -- frame
                            if (write_strobe_s = '1') and (addr_68k_s(23 downto 0) = x"800004") and (data_out_68k_s = x"01f0") then
                                start_frame_gen_reg_s <= '1';
                            else
                                start_frame_gen_reg_s <= '0';
                            end if;
                        end if; -- Reset
                    end if; -- Rising Edge Clock
                end process;

                -- Shift register for sync (clock domain crossing) and edge
                -- detection (to start the frame generation)
                process (clk_fast_i) is
                begin
                    if rising_edge(clk_fast_i) then
                        if rst_i = '1' then
                            sync_reg_s <= (others => '0');
                        else
                            -- Input
                            sync_reg_s(0) <= start_frame_gen_reg_s;
                            -- Shift
                            for i in 1 to sync_reg_s'high loop
                                sync_reg_s(i) <= sync_reg_s(i-1);
                            end loop;
                        end if; -- Reset
                    end if; -- Rising Edge Clock
                end process;

                -- Edge detection
                generate_frame_s <= (not sync_reg_s(sync_reg_s'high)) and sync_reg_s(sync_reg_s'high-1);
            end block sync_generate_frame_block;

        end block video_regs_block;

        ---------------
        -- IRQ Cause --
        ---------------
        -- TODO (Temporary)
        irq_cause_process : process(clk_68k_i) is
        begin
            if rising_edge(clk_68k_i) then
                irq_cause_ack_s       <= '0';
                irq_cause_data_o_s    <= (others => '0');
                if irq_cause_enable_s = '1' then
                    if read_strobe_s = '1' then
                        irq_cause_data_o_s <= x"0003";  -- Active low
                        irq_cause_ack_s    <= '1';
                    end if;
                end if;
            end if;  -- Rising Edge Clock
        end process irq_cause_process;

        -- TODO : Check the VCTRL REGS (signals etc.)
        -- TODO : Replace the VCTRL blocks altogether (so not to use BRAM)

        -------------
        -- Vctrl 0 --
        -------------
        vctrl_regs_0 : entity work.vctrl_regs
            port map (
                clk_68k_i    => clk_68k_i,
                enable_i     => v_ctrl_0_enable_s,
                write_low_i  => low_write_strobe_s,
                write_high_i => high_write_strobe_s,
                read_low_i   => read_strobe_s,
                read_high_i  => read_strobe_s,
                addr_i       => addr_68k_s(V_CTRL_0_LOG_SIZE_C-1 downto 1),
                data_i       => data_out_68k_s,
                data_o       => v_ctrl_0_data_o_s,
                ack_o        => v_ctrl_0_ack_s,
                clk_fast_i   => clk_fast_i,
                vctrl_o      => gfx_vctrl_0_reg_s);

        -- This is not supposed to infer a BRAM, TODO : check resource usage after
        -- synthesis. It does generate a BRAM due to dual clocks
        --v_ctrl_0_block : block
        --    signal this_data_will_be_read_from_graphical_processor_s : std_logic_vector(16-1 downto 0);
        --begin
        --    v_ctrl_0 : entity work.dual_port_ram
        --        port map (
        --            clk_68k_i   => clk_68k_i,
        --            enable_i    => v_ctrl_0_enable_s,
        --            write_i     => write_strobe_s,
        --            read_i      => read_strobe_s,
        --            addr_i      => addr_68k_s(V_CTRL_0_LOG_SIZE_C-1 downto 1),
        --            data_i      => data_out_68k_s,
        --            data_o      => v_ctrl_0_data_o_s,
        --            ack_o       => v_ctrl_0_ack_s,
        --            clk_fast_i  => clk_fast_i,
        --            addr_fast_i => std_logic_vector(to_unsigned(0, V_CTRL_0_LOG_SIZE_C-1)),
        --            data_fast_o => this_data_will_be_read_from_graphical_processor_s);
        --
        --    --gfx_vctrl_0_reg_s <= this_data_will_be_read_from_graphical_processor_s(gfx_vctrl_0_reg_s'length-1 downto 0);
        --    gfx_vctrl_0_reg_s <= (others => '0');
        --
        --end block v_ctrl_0_block;

        -------------
        -- Vctrl 1 --
        -------------
        vctrl_regs_1 : entity work.vctrl_regs
            port map (
                clk_68k_i    => clk_68k_i,
                enable_i     => v_ctrl_1_enable_s,
                write_low_i  => low_write_strobe_s,
                write_high_i => high_write_strobe_s,
                read_low_i   => read_strobe_s,
                read_high_i  => read_strobe_s,
                addr_i       => addr_68k_s(V_CTRL_1_LOG_SIZE_C-1 downto 1),
                data_i       => data_out_68k_s,
                data_o       => v_ctrl_1_data_o_s,
                ack_o        => v_ctrl_1_ack_s,
                clk_fast_i   => clk_fast_i,
                vctrl_o      => gfx_vctrl_1_reg_s);

        -- This is not supposed to infer a BRAM, TODO : check resource usage after
        -- synthesis.
        --v_ctrl_1_block : block
        --    signal this_data_will_be_read_from_graphical_processor_s : std_logic_vector(16-1 downto 0);
        --begin
        --    v_ctrl_1 : entity work.dual_port_ram
        --        port map (
        --            clk_68k_i   => clk_68k_i,
        --            enable_i    => v_ctrl_1_enable_s,
        --            write_i     => write_strobe_s,
        --            read_i      => read_strobe_s,
        --            addr_i      => addr_68k_s(V_CTRL_1_LOG_SIZE_C-1 downto 1),
        --            data_i      => data_out_68k_s,
        --            data_o      => v_ctrl_1_data_o_s,
        --            ack_o       => v_ctrl_1_ack_s,
        --            clk_fast_i  => clk_fast_i,
        --            addr_fast_i => std_logic_vector(to_unsigned(0, V_CTRL_1_LOG_SIZE_C-1)),
        --            data_fast_o => this_data_will_be_read_from_graphical_processor_s);
        --
        --    --gfx_vctrl_1_reg_s <= this_data_will_be_read_from_graphical_processor_s(gfx_vctrl_1_reg_s'length-1 downto 0);
        --    gfx_vctrl_1_reg_s <= (others => '0');
        --
        --end block v_ctrl_1_block;

        -------------
        -- Vctrl 2 --
        -------------
        vctrl_regs_2 : entity work.vctrl_regs
            port map (
                clk_68k_i    => clk_68k_i,
                enable_i     => v_ctrl_2_enable_s,
                write_low_i  => low_write_strobe_s,
                write_high_i => high_write_strobe_s,
                read_low_i   => read_strobe_s,
                read_high_i  => read_strobe_s,
                addr_i       => addr_68k_s(V_CTRL_2_LOG_SIZE_C-1 downto 1),
                data_i       => data_out_68k_s,
                data_o       => v_ctrl_2_data_o_s,
                ack_o        => v_ctrl_2_ack_s,
                clk_fast_i   => clk_fast_i,
                vctrl_o      => gfx_vctrl_2_reg_s);

        -- This is not supposed to infer a BRAM, TODO : check resource usage after
        -- synthesis.
        --v_ctrl_2_block : block
        --    signal this_data_will_be_read_from_graphical_processor_s : std_logic_vector(16-1 downto 0);
        --begin
        --    v_ctrl_2 : entity work.dual_port_ram
        --        port map (
        --            clk_68k_i   => clk_68k_i,
        --            enable_i    => v_ctrl_2_enable_s,
        --            write_i     => write_strobe_s,
        --            read_i      => read_strobe_s,
        --            addr_i      => addr_68k_s(V_CTRL_2_LOG_SIZE_C-1 downto 1),
        --            data_i      => data_out_68k_s,
        --            data_o      => v_ctrl_2_data_o_s,
        --            ack_o       => v_ctrl_2_ack_s,
        --            clk_fast_i  => clk_fast_i,
        --            addr_fast_i => std_logic_vector(to_unsigned(0, V_CTRL_2_LOG_SIZE_C-1)),
        --            data_fast_o => this_data_will_be_read_from_graphical_processor_s);
        --
        --    --gfx_vctrl_2_reg_s <= this_data_will_be_read_from_graphical_processor_s(gfx_vctrl_2_reg_s'length-1 downto 0);
        --    gfx_vctrl_2_reg_s <= (others => '0');
        --
        --end block v_ctrl_2_block;

        -----------------
        -- Palette RAM --
        -----------------
        palette_ram : entity work.dual_port_ram
        port map (
            clk_68k_i   => clk_68k_i,
            enable_i    => palette_ram_enable_s,
            write_i     => write_strobe_s,
            read_i      => read_strobe_s,
            addr_i      => addr_68k_s(PALETTE_RAM_LOG_SIZE_C-1 downto 1),
            data_i      => data_out_68k_s,
            data_o      => palette_ram_data_o_s,
            ack_o       => palette_ram_ack_s,
            clk_fast_i  => clk_fast_i,
            addr_fast_i => gfx_palette_ram_addr_s,
            data_fast_o => gfx_palette_ram_data_s);

    end block graphic_processor_block;

    -----------------
    -- Input Ports --
    -----------------
    input_ports_block : block
        signal in_0_reg_s : std_logic_vector(15 downto 0);
        signal in_1_reg_s : std_logic_vector(15 downto 0);
        signal in_1_s     : std_logic_vector(15 downto 0);
    begin
        -- Enable for the OR'ed bus
        in_0_data_o_s <= in_0_enable_s and in_0_reg_s;
        in_1_data_o_s <= in_1_enable_s and in_1_reg_s;

        in_1_s <= (not joystick_inputs_i(31 downto 28)) & eeprom_do_s & (not joystick_inputs_i(26 downto 16));

        in_process : process(clk_68k_i) is
        begin
            if rising_edge(clk_68k_i) then
                in_0_ack_s <= '0';
                in_1_ack_s <= '0';
                -- The inputs are active low
                --in_0_reg_s <= x"FFFF";
                in_0_reg_s <= not joystick_inputs_i(15 downto 0);
                in_1_reg_s <= in_1_s; -- Warning this also has the EEPROM DO signal
                if read_strobe_s = '1' then
                    if in_0_enable_s = '1' then
                        in_0_ack_s <= '1';
                    end if;
                    if in_1_enable_s = '1' then
                        in_1_ack_s <= '1';
                    end if;
                end if;
            end if;
        end process in_process;

    end block input_ports_block;

    ------------
    -- EEPROM --
    ------------
    eeprom_generate : if INCLUDE_EEPROM_G generate

        eeprom : entity work.eeprom_93c46
            port map (
                clk_i  => eeprom_ci_s,
                cs_i   => eeprom_cs_s,
                data_i => eeprom_di_s,
                data_o => eeprom_do_s);

    else generate

        eeprom_do_s <= '0';

    end generate eeprom_generate;

    eeprom_process : process(clk_68k_i) is
    begin
        if rising_edge(clk_68k_i) then
            eeprom_ack_s <= '0';
            if eeprom_enable_s = '1' then
                -- I believe it is possible that this is never ever read
                if read_strobe_s = '1' then
                    eeprom_data_o_s <= x"0000";
                    eeprom_ack_s    <= '1';
                elsif write_strobe_s = '1' then
                    eeprom_cs_s  <= data_out_68k_s(1);
                    eeprom_ci_s  <= data_out_68k_s(2);
                    eeprom_di_s  <= data_out_68k_s(3);
                    eeprom_ack_s <= '1';
                end if;
            else
                eeprom_data_o_s <= (others => '0');
            end if;  -- Enable
        end if;  -- Rising Edge Clock
    end process eeprom_process;

    ----------------
    -- Edge Cases --
    ----------------
    edge_case_process : process(clk_68k_i) is
    begin
        if rising_edge(clk_68k_i) then
            edge_case_ack_s <= '0';
            if edge_case_enable_s = '1' then
                if (read_strobe_s = '1') or (write_strobe_s = '1') then
                    edge_case_ack_s <= '1';
                end if;
            end if; -- Enable
        end if; -- Rising Edge Clock
    end process edge_case_process;

    --------------------------------
    -- Watch Bus (Only for debug) --
    --------------------------------
    watch_bus_generate : if EXPORT_WATCH_SIGNALS_G generate

        ram_1017a4_watch_process : process(clk_68k_i) is
        begin
            if rising_edge(clk_68k_i) then
                if (write_strobe_s = '1') then
                    if addr_68k_s(23 downto 1) = (x"1017a" & "010") then
                        -- Big Endian (MSB is in lower byte, LSB is in upper byte)
                        if (high_write_strobe_s = '1') then
                            watch_bus_o.ram_flags_1017a4(7 downto 0) <= data_out_68k_s(7 downto 0);
                        end if;
                        if (low_write_strobe_s = '1') then
                            watch_bus_o.ram_flags_1017a4(15 downto 8) <= data_out_68k_s(15 downto 8);
                        end if;
                    end if;
                end if;
            end if;
        end process ram_1017a4_watch_process;

    else generate

        -- Nothing to see here
        watch_bus_o <= (others => (others => '0'));

    end generate watch_bus_generate;

end struct;
