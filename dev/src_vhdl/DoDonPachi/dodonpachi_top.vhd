-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : dodonpachi_top.vhd
-- Description  : This is a top for Dodonpachi, it is a layer to connect
--                Dodonpachi to the outside world, e.g. inputs and outputs as
--                well as the memories that are too big to be BRAM e.g., all
--                the ROMs, which amount to around 20MB are stored outside of
--                the FPGA and this layer connects them. This layer also does
--                some clock domain crossing since Dodonpachi's main processor
--                should run at around 16MHz and the rest of FPGA runs at
--                100MHz (e.g. HP interface to DDR3).
--                I say the main processor runs around 16MHz (which is the
--                value in real life) but since the VHDL core may not be cycle
--                accurate, this clock my need to be adjusted a bit to have a
--                similar speed as the real game.
--
-- Author       : Rick Wertenbroek
-- Version      : 0.1
--
-- Dependencies : All of its contents
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.dodonpachi_pkg.all;

entity dodonpachi_top is
    generic (
        SIMULATION_G                : boolean := false;
        INCLUDE_GRAPHIC_PROCESSOR_G : boolean := true
        );
    port (
        -- Standard signals
        clk_i                 : in  std_logic;
        rst_i                 : in  std_logic;
        -- Reset signal for the 68k
        rst_68k_i             : in  std_logic;
        enable_68k_i          : in  std_logic := '1';
        -- Joystick inputs
        joystick_inputs_i     : in  std_logic_vector(31 downto 0) := (others => '0');
        -- Memory interface for the ROM
        rom_addr_68k_cache_o  : out std_logic_vector(DDP_ROM_LOG_SIZE_C-1 downto 0);
        rom_read_68k_cache_o  : out std_logic;
        rom_valid_68k_cache_i : in  std_logic;
        rom_data_68k_cache_i  : in  std_logic_vector(DDP_ROM_CACHE_LINE_WIDTH-1 downto 0);
        -- GFX
        rom_addr_gfx_o        : out std_logic_vector(31 downto 0);
        tiny_burst_gfx_o      : out std_logic;
        rom_burst_read_gfx_o  : out std_logic;
        rom_data_valid_gfx_i  : in  std_logic;
        rom_data_gfx_i        : in  std_logic_vector(31 downto 0);
        rom_burst_done_gfx_i  : in  std_logic;
        -- Frame Buffer
        frame_buffer_addr_o   : out frame_buffer_addr_t;
        frame_buffer_data_o   : out std_logic_vector(DDP_WORD_WIDTH-2 downto 0);
        frame_buffer_write_o  : out std_logic;
        -- Frame Buffer DMA
        start_fb_dma_o        : out std_logic;
        -- For video interrupt
        frame_i               : in  std_logic;
        -- Debug
        command_bus_i         : in  command_bus_t;
        lock_step_i           : in  std_logic := '0';
        step_i                : in  std_logic := '0';
        break_point_addr_i    : in  std_logic_vector(31 downto 0) := (others => '0');
        break_point_enabled_i : in  std_logic := '0';
        mem_bus_68k_o         : out std_logic_vector(31 downto 0);
        TG68_PC_o             : out std_logic_vector(31 downto 0);
        TG68_PCW_o            : out std_logic;
        clk_68k_o             : out std_logic;
        miss_counter_o        : out std_logic_vector(31 downto 0);
        watch_bus_o           : out watch_bus_t
        );
end entity dodonpachi_top;

architecture struct of dodonpachi_top is

    ----------------
    -- Components --
    ----------------
    component clk_dodonpachi_68k
        port (
            clk_out1 : out std_logic;
            locked   : out std_logic;
            clk_in1  : in  std_logic);
    end component clk_dodonpachi_68k;

    -------------
    -- Signals --
    -------------

    -- PLL related signals
    signal clk_68k_s              : std_logic;
    signal locked_s               : std_logic;
    signal rst_68k_s              : std_logic;

    -- 68k related signals
    signal rom_addr_from_68k_s    : std_logic_vector(DDP_ROM_LOG_SIZE_C-1 downto 0);
    signal rom_read_from_68k_s    : std_logic;
    signal rom_data_to_68k_s      : std_logic_vector(DDP_WORD_WIDTH-1 downto 0);
    signal rom_valid_to_68k_s     : std_logic;

    -- Cache memory related signals
    signal rom_addr_from_cache_s  : std_logic_vector(DDP_ROM_LOG_SIZE_C-1 downto 0);
    signal rom_read_from_cache_s  : std_logic;
    signal rom_valid_to_cache_s   : std_logic;
    signal rom_data_to_cache_s    : std_logic_vector(DDP_ROM_CACHE_LINE_WIDTH-1 downto 0);

    -- Frame buffer conversion
    signal frame_buffer_color_s   : color_t;

begin

    -- Dodonpachi Main Processor Clock

    ------------------------------------------------------------------------------
    --  Output     Output      Phase    Duty Cycle   Pk-to-Pk     Phase
    --   Clock     Freq (MHz)  (degrees)    (%)     Jitter (ps)  Error (ps)
    ------------------------------------------------------------------------------
    -- clk_out1____16.000______0.000______50.0______191.387_____98.575
    --
    ------------------------------------------------------------------------------
    -- Input Clock   Freq (MHz)    Input Jitter (UI)
    ------------------------------------------------------------------------------
    -- __primary_________100.000____________0.010
    clk_dodonpachi_68k_inst : clk_dodonpachi_68k -- IP
        port map (
            clk_out1 => clk_68k_s,  -- 16MHz
            locked   => locked_s,   -- Locked
            clk_in1  => clk_i       -- 100MHz
            );

    clk_68k_o <= clk_68k_s;

    -- This reset should maybe go to the level above to reset the cache and or
    -- memory conversion mechanisms etc. I any case if the PLL unlocks we will
    -- have a bad time, what if it unlocks during a memory burst or
    -- something... I see no easy way to handle this. Let's hope it will not
    -- unlock during operation (and it shouldn't).
    rst_68k_s <= (not locked_s) or rst_i or rst_68k_i;

    --------------
    -- Main PCB --
    --------------
    dodonpachi : entity work.dodonpachi
        generic map (
            INCLUDE_GRAPHIC_PROCESSOR_G => INCLUDE_GRAPHIC_PROCESSOR_G)
        port map (
            clk_fast_i            => clk_i,
            rst_i                 => rst_i,
            enable_68k_i          => enable_68k_i,
            clk_68k_i             => clk_68k_s,
            rst_68k_i             => rst_68k_s,
            joystick_inputs_i     => joystick_inputs_i,
            rom_addr_68k_o        => rom_addr_from_68k_s,
            rom_read_68k_o        => rom_read_from_68k_s,
            rom_valid_68k_i       => rom_valid_to_68k_s,
            rom_data_68k_i        => rom_data_to_68k_s,
            rom_addr_gfx_o        => rom_addr_gfx_o,
            tiny_burst_gfx_o      => tiny_burst_gfx_o,
            rom_burst_read_gfx_o  => rom_burst_read_gfx_o,
            rom_data_valid_gfx_i  => rom_data_valid_gfx_i,
            rom_data_gfx_i        => rom_data_gfx_i,
            rom_burst_done_gfx_i  => rom_burst_done_gfx_i,
            frame_buffer_addr_o   => frame_buffer_addr_o,
            frame_buffer_color_o  => frame_buffer_color_s,
            frame_buffer_write_o  => frame_buffer_write_o,
            start_fb_dma_o        => start_fb_dma_o,
            frame_i               => frame_i,
            command_bus_i         => command_bus_i,
            lock_step_i           => lock_step_i,
            step_i                => step_i,
            break_point_addr_i    => break_point_addr_i,
            break_point_enabled_i => break_point_enabled_i,
            mem_bus_68k_o         => mem_bus_68k_o,
            TG68_PC_o             => TG68_PC_o,
            TG68_PCW_o            => TG68_PCW_o,
            watch_bus_o           => watch_bus_o);

    frame_buffer_data_o <= frame_buffer_color_s.r & frame_buffer_color_s.g & frame_buffer_color_s.b;
    -- TODO : FIX EVERYWHERE - CAVE 1st gen stores as GRB555 not RGB555
    --frame_buffer_data_o <= frame_buffer_color_s.g & frame_buffer_color_s.r & frame_buffer_color_s.b;

    ------------------
    -- Cache Memory --
    ------------------

    -- Cache memory to reduce the number of accesses to the external memory.
    cache_memory : entity work.cache_memory
        generic map (
            ADDRESS_BITS_G     => DDP_ROM_LOG_SIZE_C,
            --LOG_CACHE_LINES_G  => LOG_CACHE_LINES_G,
            --LOG_CACHE_LINES_G  => 10,
            WORD_BYTE_SIZE_G   => DDP_WORD_WIDTH/8,
            CACHE_LINE_WORDS_G => DDP_ROM_CACHE_LINE_WORDS,
            PERF_COUNT_EN_G    => true)
            --PERF_COUNT_EN_G    => SIMULATION_G) -- This is for performance monitoring
        port map (
            clk_i                   => clk_68k_s,
            rst_i                   => rst_68k_s,
            agent_to_cache_addr_i   => rom_addr_from_68k_s,
            agent_to_cache_read_i   => rom_read_from_68k_s,
            cache_to_agent_data_o   => rom_data_to_68k_s,
            cache_to_agent_valid_o  => rom_valid_to_68k_s,
            cache_to_memory_addr_o  => rom_addr_from_cache_s,
            cache_to_memory_read_o  => rom_read_from_cache_s,
            memory_to_cache_data_i  => rom_data_to_cache_s,
            memory_to_cache_valid_i => rom_valid_to_cache_s,
            req_counter_o           => open, -- The two last outputs are for
                                             -- performance monitoring
            miss_counter_o          => miss_counter_o);

    ---------------------------
    -- Clock Domain Crossing --
    ---------------------------
    kel_thuzad : entity work.data_freezer
        port map (
            slow_clk_i => clk_68k_s,
            slow_rst_i => rst_68k_s,
            fast_clk_i => clk_i,
            fast_rst_i => rst_i,
            data_sc_i  => rom_addr_from_cache_s,
            write_sc_i => rom_read_from_cache_s,
            data_sc_o  => rom_data_to_cache_s,
            valid_sc_o => rom_valid_to_cache_s,
            data_fc_i  => rom_data_68k_cache_i,
            write_fc_i => rom_valid_68k_cache_i,
            data_fc_o  => rom_addr_68k_cache_o,
            valid_fc_o => rom_read_68k_cache_o);

end struct;
