-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : dual_port_ram.vhd
-- Description  : This is a generic dual port RAM for dodonpachi, one side is
--                the processor to access via the main bus and the other side
--                is for other components to access. E.g., the Color Palette
--                RAM which is written at one side by the 68k and is used on
--                the other side by the graphical processor. (This can be at
--                different clock speeds).
--
-- Author       : Rick Wertenbroek
-- Version      : 0.0
--
-- VHDL std     : 2008
-- Dependencies : true_dual_port_bram.vhd
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dual_port_ram is
    port (
        -- 68k port
        clk_68k_i   : in  std_logic;
        enable_i    : in  std_logic;
        write_i     : in  std_logic;
        read_i      : in  std_logic;
        addr_i      : in  std_logic_vector;
        data_i      : in  std_logic_vector;
        data_o      : out std_logic_vector;
        ack_o       : out std_logic;
        -- fast port (Read-Only Port)
        clk_fast_i  : in  std_logic;
        addr_fast_i : in  std_logic_vector;
        data_fast_o : out std_logic_vector
        );
end entity dual_port_ram;

architecture struct of dual_port_ram is

    signal data_s : std_logic_vector(data_o'length-1 downto 0);

begin

    assert data_i'length = data_o'length report "Data in and data out have different widths !" severity error;

    true_dual_port_bram_1 : entity work.true_dual_port_bram
        generic map (
            WIDTH_PORT_A => data_i'length,
            DEPTH_PORT_A => 2**(addr_i'length),
            ADDR_WIDTH_A => addr_i'length,
            WIDTH_PORT_B => data_fast_o'length,
            DEPTH_PORT_B => 2**(addr_fast_i'length),
            ADDR_WIDTH_B => addr_fast_i'length)
        port map (
            clk_a_i      => clk_68k_i,
            write_en_a_i => enable_i and write_i,
            addr_a_i     => addr_i,
            data_a_i     => data_i,
            data_a_o     => data_s,
            clk_b_i      => clk_fast_i,
            write_en_b_i => '0', -- Read Only
            addr_b_i     => addr_fast_i,
            data_b_i     => (others => '0'),
            data_b_o     => data_fast_o);

    data_o <= enable_i and data_s; -- Since it will be on an OR'ed bus

    process(clk_68k_i)
    begin
        if rising_edge(clk_68k_i) then
            ack_o <= enable_i and (read_i or write_i);
        end if;
    end process;

end struct;
