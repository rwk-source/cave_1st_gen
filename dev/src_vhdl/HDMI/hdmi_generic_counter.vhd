-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : hdmi_generic_counter.vhd
-- Description  :
--
-- Author       : Rick Wertenbroek
-- Version      : 0.0
--
-- VHDL std     : 2008
-- Dependencies :
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.hdmi_pkg.all;

entity hdmi_generic_counter is
    generic (
        COUNTER_ATTR_G  : counter_attr_t;
        PIXEL_DELAY_G   : natural := 1 -- Delay until the first pixel arrives
        );
    port (
        -- Stanrdard signals
        clk_hdmi_i     : in  std_logic;
        rst_i          : in  std_logic;
        next_pix_pos_o : out max_position_t;
        current_pos_o  : out max_position_t;
        video_on_o     : out std_logic;
        sync_o         : out sync_t
        );
end entity hdmi_generic_counter;

architecture rtl of hdmi_generic_counter is

    ---------------
    -- Constants --
    ---------------
    constant HDMI_H_CNT_SIZE_C : natural := max_position_t.x'length;
    constant HDMI_V_CNT_SIZE_C : natural := max_position_t.y'length;
    constant HDMI_H_LEN_C      : natural := COUNTER_ATTR_G.h_active_pixels +
                                            COUNTER_ATTR_G.h_front_porch_len +
                                            COUNTER_ATTR_G.h_sync_len +
                                            COUNTER_ATTR_G.h_back_porch_len;
    constant HDMI_V_LEN_C      : natural := COUNTER_ATTR_G.v_active_pixels +
                                            COUNTER_ATTR_G.v_front_porch_len +
                                            COUNTER_ATTR_G.v_sync_len +
                                            COUNTER_ATTR_G.v_back_porch_len;

    -------------
    -- Signals --
    -------------
    signal sync_s     : sync_t;
    signal video_on_s : std_logic;

    --------------
    -- Counters --
    --------------
    signal counter_x_s  : unsigned(HDMI_H_CNT_SIZE_C-1 downto 0);
    signal counter_y_s  : unsigned(HDMI_V_CNT_SIZE_C-1 downto 0);
    signal position_x_s : unsigned(HDMI_H_CNT_SIZE_C-1 downto 0);
    signal position_y_s : unsigned(HDMI_V_CNT_SIZE_C-1 downto 0);

begin

    -- Counter
    process (clk_hdmi_i) is
    begin
        if rising_edge(clk_hdmi_i) then
            if rst_i = '1' then
                counter_x_s <= (others => '0');
                counter_y_s <= (others => '0');
            else
                if counter_x_s = HDMI_H_LEN_C-1 then
                    counter_x_s <= (others => '0');
                    if counter_y_s = HDMI_V_LEN_C-1 then
                        counter_y_s <= (others => '0');
                    else
                        counter_y_s <= counter_y_s + 1;
                    end if; -- Y
                else
                    counter_x_s <= counter_x_s + 1;
                end if; -- X
            end if; -- Reset

            -- Second counter for position (offset is because of delay of bram).
            if rst_i = '1' then
                position_x_s <= to_unsigned(PIXEL_DELAY_G, position_x_s'length);
                position_y_s <= (others => '0');
            else
                if position_x_s = HDMI_H_LEN_C-1 then
                    position_x_s <= (others => '0');
                    if position_y_s = HDMI_V_LEN_C-1 then
                        position_y_s <= (others => '0');
                    else
                        position_y_s <= position_y_s + 1;
                    end if; -- Y
                else
                    position_x_s <= position_x_s + 1;
                end if; -- X
            end if; -- Reset
        end if; -- Rising Edge Clock
    end process;

    -- Note : The signals below could be done with = comparators and registers
    -- to save logic instead of being combinatorial with < comparators.

    -- Horizontal Sync
    sync_s.h_sync <= '1' when (counter_x_s > (COUNTER_ATTR_G.h_active_pixels + COUNTER_ATTR_G.h_front_porch_len - 1)) and (counter_x_s < (COUNTER_ATTR_G.h_active_pixels + COUNTER_ATTR_G.h_front_porch_len + COUNTER_ATTR_G.h_sync_len)) else
                     '0';

    -- Vertical Sync
    sync_s.v_sync <= '1' when (counter_y_s > (COUNTER_ATTR_G.v_active_pixels + COUNTER_ATTR_G.v_front_porch_len - 1)) and (counter_y_s < (COUNTER_ATTR_G.v_active_pixels + COUNTER_ATTR_G.v_front_porch_len + COUNTER_ATTR_G.v_sync_len)) else
                     '0';

    -- Video On (not blank)
    video_on_s <= '1' when (counter_x_s < COUNTER_ATTR_G.h_active_pixels) and (counter_y_s < COUNTER_ATTR_G.v_active_pixels) else
                  '0';

    -------------
    -- Outputs --
    -------------
    sync_o           <= sync_s;
    video_on_o       <= video_on_s;
    next_pix_pos_o.x <= position_x_s;
    next_pix_pos_o.y <= position_y_s;
    current_pos_o.x  <= counter_x_s;
    current_pos_o.y  <= counter_y_s;

end architecture rtl;
