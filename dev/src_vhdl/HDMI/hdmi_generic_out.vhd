-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : hdmi_generic_out.vhd
-- Description  :
--
-- Author       : Rick Wertenbroek
-- Version      : 1.0
--
-- VHDL std     : 2008
-- Dependencies :
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.hdmi_pkg.all;

entity hdmi_generic_out is
    generic (
        COUNTER_ATTR_G : counter_attr_t
        );
    port (
        -- Standard signals
        clk_hdmi_parallel_i : in  std_logic;
        clk_hdmi_serial_i   : in  std_logic;
        rst_i               : in  std_logic;
        -- Pixels in
        rgb_i               : in  rgb_t;
        next_pix_pos_o      : out max_position_t;
        current_pos_o       : out max_position_t;
        video_on_o          : out std_logic;
        -- HDMI TX
        hdmi_tx_o           : out hdmi_tx_out_t
        );
end entity hdmi_generic_out;

architecture struct of hdmi_generic_out is

    ----------------
    -- Components --
    ----------------
    component hdmi_tx_interface is
        port (
            clk_parallel_i : in  std_logic;
            clk_serial_i   : in  std_logic;
            rst_i          : in  std_logic;
            sync_i         : in  sync_t;
            rgb_i          : in  rgb_t;
            video_on_i     : in  std_logic;
            hdmi_tx_o      : out hdmi_tx_out_t);
    end component hdmi_tx_interface;

    -------------
    -- Signals --
    -------------
    signal sync_s     : sync_t;
    signal video_on_s : std_logic;

begin

    -- HDMI Counter (generates the sync signals and position of next pixel to draw)
    ------------------------------------------------------------------------------------
    hdmi_counter_inst : entity work.hdmi_generic_counter
        generic map (
            COUNTER_ATTR_G  => COUNTER_ATTR_G
            )
        port map (
            clk_hdmi_i      => clk_hdmi_parallel_i,
            rst_i           => rst_i,
            next_pix_pos_o  => next_pix_pos_o,
            current_pos_o   => current_pos_o,
            video_on_o      => video_on_s,
            sync_o          => sync_s
            );

    -- HDMI TX Interface (Single Ended)
    -----------------------------------
    hdmi_tx_interface_inst : hdmi_tx_interface
        port map (
            clk_parallel_i => clk_hdmi_parallel_i,
            clk_serial_i   => clk_hdmi_serial_i,
            rst_i          => rst_i,
            sync_i         => sync_s,
            rgb_i          => rgb_i,
            video_on_i     => video_on_s,
            hdmi_tx_o      => hdmi_tx_o
            );

    video_on_o <= video_on_s;

end architecture struct;
