-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : hdmi_pkg.vhd
-- Description  :
--
-- Author       : Rick Wertenbroek
-- Version      : 1.0
--
-- Dependencies : -
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package hdmi_pkg is

    ---------------
    -- Constants --
    ---------------

    -- Transitional Minimized Differential Signaling
    constant TMDS_DATA_WIDTH_PKG_C   : natural := 10;
    constant COLOR_BYTE_WIDTH_PKG_C  : natural := 8;

    -- VGA 640x480
    constant VGA_H_CNT_SIZE_PKG_C    : natural := 10;
    constant VGA_H_ACTIVE_PKG_C      : natural := 640;
    constant VGA_H_FRONT_PORCH_PKG_C : natural := 16;
    constant VGA_H_SYNC_LEN_PKG_C    : natural := 96;
    constant VGA_H_BACK_PORCH_PKG_C  : natural := 48;

    constant VGA_V_CNT_SIZE_PKG_C    : natural := 9;
    constant VGA_V_ACTIVE_PKG_C      : natural := 480;
    constant VGA_V_FRONT_PORCH_PKG_C : natural := 10;
    constant VGA_V_SYNC_LEN_PKG_C    : natural := 2;
    constant VGA_V_BACK_PORCH_PKG_C  : natural := 33;

    constant VGA_H_LEN_PKG_C         : natural := VGA_H_ACTIVE_PKG_C + VGA_H_FRONT_PORCH_PKG_C + VGA_H_SYNC_LEN_PKG_C + VGA_H_BACK_PORCH_PKG_C;
    constant VGA_V_LEN_PKG_C         : natural := VGA_V_ACTIVE_PKG_C + VGA_V_FRONT_PORCH_PKG_C + VGA_V_SYNC_LEN_PKG_C + VGA_V_BACK_PORCH_PKG_C;

    -- HDMI 1080p 1920x1080
    constant HDMI_1080_H_CNT_SIZE_PKG_C    : natural := 12;
    constant HDMI_1080_H_ACTIVE_PKG_C      : natural := 1920;
    constant HDMI_1080_H_FRONT_PORCH_PKG_C : natural := 88;
    constant HDMI_1080_H_SYNC_LEN_PKG_C    : natural := 44;
    constant HDMI_1080_H_BACK_PORCH_PKG_C  : natural := 148;

    constant HDMI_1080_V_CNT_SIZE_PKG_C    : natural := 11;
    constant HDMI_1080_V_ACTIVE_PKG_C      : natural := 1080;
    constant HDMI_1080_V_FRONT_PORCH_PKG_C : natural := 4;
    constant HDMI_1080_V_SYNC_LEN_PKG_C    : natural := 5;
    constant HDMI_1080_V_BACK_PORCH_PKG_C  : natural := 36;

    constant HDMI_1080_H_LEN_PKG_C         : natural := HDMI_1080_H_ACTIVE_PKG_C + HDMI_1080_H_FRONT_PORCH_PKG_C + HDMI_1080_H_SYNC_LEN_PKG_C + HDMI_1080_H_BACK_PORCH_PKG_C;
    constant HDMI_1080_V_LEN_PKG_C         : natural := HDMI_1080_V_ACTIVE_PKG_C + HDMI_1080_V_FRONT_PORCH_PKG_C + HDMI_1080_V_SYNC_LEN_PKG_C + HDMI_1080_V_BACK_PORCH_PKG_C;

    -----------
    -- Types --
    -----------

    subtype tmds_word_t is std_logic_vector(TMDS_DATA_WIDTH_PKG_C-1 downto 0);

    type counter_attr_t is record
        h_active_pixels   : natural;
        h_front_porch_len : natural;
        h_sync_len        : natural;
        h_back_porch_len  : natural;
        v_active_pixels   : natural;
        v_front_porch_len : natural;
        v_sync_len        : natural;
        v_back_porch_len  : natural;
    end record counter_attr_t;

    type position_vga_t is record
        x : unsigned(VGA_H_CNT_SIZE_PKG_C-1 downto 0);
        y : unsigned(VGA_V_CNT_SIZE_PKG_C-1 downto 0);
    end record position_vga_t;

    type position_1080_t is record
        x : unsigned(HDMI_1080_H_CNT_SIZE_PKG_C-1 downto 0);
        y : unsigned(HDMI_1080_V_CNT_SIZE_PKG_C-1 downto 0);
    end record position_1080_t;

    -- This should be enough up to 4k
    type max_position_t is record
        x : unsigned(12 downto 0);
        y : unsigned(12 downto 0);
    end record max_position_t;

    --subtype max_position_t is position_1080_t;

    type rgb_t is record
        r      : std_logic_vector(7 downto 0);
        g      : std_logic_vector(7 downto 0);
        b      : std_logic_vector(7 downto 0);
    end record rgb_t;

    type sync_t is record
        h_sync : std_logic;
        v_sync : std_logic;
    end record sync_t;

    type hdmi_tx_out_t is record
        scl : std_logic;                -- I2C Clock (Leave high if unused)
        clk : std_logic;
        r   : std_logic;
        g   : std_logic;
        b   : std_logic;
    end record hdmi_tx_out_t;

    -- Is not used
    type hdmi_tx_in_t is record
        hdmi_tx_hpd : std_logic; -- Hot Plug Detect
    end record hdmi_tx_in_t;

    -- Is not used
    type hdmi_tx_inout_t is record
        hdmi_tx_sda : std_logic; -- I2C for DDC (Display Data Channel)
        hdmi_tx_cec : std_logic; -- Consumer Electronics Control (Will not be used in this project)
    end record hdmi_tx_inout_t;

    type array_of_32b_vectors is array (natural range <>) of std_logic_vector(31 downto 0);

    -----------
    -- Modes --
    -----------

    --  1920x1080 (0x2f5) 148.500MHz +HSync +VSync *current +preferred
    --    h: width  1920 start 2008 end 2052 total 2200 skew    0 clock  67.50KHz
    --    v: height 1080 start 1084 end 1089 total 1125           clock  60.00Hz
    constant HDMI_COUNTER_ATTR_1080P_PKG_C : counter_attr_t := (h_active_pixels   => 1920,
                                                                h_front_porch_len => 88,
                                                                h_sync_len        => 44,
                                                                h_back_porch_len  => 148,
                                                                v_active_pixels   => 1080,
                                                                v_front_porch_len => 4,
                                                                v_sync_len        => 5,
                                                                v_back_porch_len  => 36);

    -- 1024x600
    -- Clock 51.2 MHz
    constant HDMI_COUNTER_ATTR_1024x600_PKG_C : counter_attr_t := (h_active_pixels   => 1024,
                                                                   h_front_porch_len => 160,
                                                                   h_sync_len        => 20,
                                                                   h_back_porch_len  => 140,
                                                                   v_active_pixels   => 600,
                                                                   v_front_porch_len => 12,
                                                                   v_sync_len        => 3,
                                                                   v_back_porch_len  => 20);

    -- The Mode below is not possible because it would require more than 1 GHz
    -- serial clock...
    --  1440x2560 (0x2ec) 204.790MHz -HSync -VSync *current +preferred
    --    h: width  1440 start 1510 end 1545 total 1590 skew    0 clock 128.80KHz
    --    v: height 2560 start 2572 end 2574 total 2576           clock  50.00Hz
    constant HDMI_COUNTER_ATTR_1440x2560_PKG_C : counter_attr_t := (h_active_pixels   => 1440,
                                                                    h_front_porch_len => 70,
                                                                    h_sync_len        => 35,
                                                                    h_back_porch_len  => 45,
                                                                    v_active_pixels   => 2560,
                                                                    v_front_porch_len => 12,
                                                                    v_sync_len        => 2,
                                                                    v_back_porch_len  => 2);




end hdmi_pkg;
