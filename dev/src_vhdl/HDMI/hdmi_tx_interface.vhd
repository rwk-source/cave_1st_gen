-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : hdmi_tx_interface.vhd
-- Description  :
--
-- Author       : Rick Wertenbroek
-- Version      : 1.0
--
-- Dependencies : hdmi_pkg.vhd
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.hdmi_pkg.all;

entity hdmi_tx_interface is
    port (
        clk_parallel_i : in  std_logic;
        clk_serial_i   : in  std_logic;
        rst_i          : in  std_logic;
        -- Sync signals plus RGB
        sync_i         : in  sync_t;
        rgb_i          : in  rgb_t;
        -- Asserted if we are in the "on-screen" frame
        video_on_i     : in  std_logic;
        -- HDMI interface
        hdmi_tx_o      : out hdmi_tx_out_t
        );
end entity hdmi_tx_interface;

architecture struct of hdmi_tx_interface is

    ---------------
    -- Constants --
    ---------------
    --                                              _____|````
    constant TMDS_ENCODED_CLOCK_C : tmds_word_t := "0000011111"; -- One pixel
                                                                 -- is 8 bits
                                                                 -- (10-bit encoded)
    -- This will give us the following serialized clock :
    -- |____|````|____|````|____|```` etc.
    -- 000001111100000111110000011111 ...

    -------------
    -- Signals --
    -------------
    signal tmds_r_s : tmds_word_t;
    signal tmds_g_s : tmds_word_t;
    signal tmds_b_s : tmds_word_t;

    signal tmds_serial_r_s   : std_logic;
    signal tmds_serial_g_s   : std_logic;
    signal tmds_serial_b_s   : std_logic;
    signal tmds_serial_clk_s : std_logic;

begin

    -- Transition Minimized Differential Signaling encoding of RGB signals
    ----------------------------------------------------------------------

    tmds_encoder_red_channel : entity work.tmds_encoder
        port map (
            clk_i      => clk_parallel_i,
            data_i     => rgb_i.r,
            control_i  => (others => '0'),
            video_on_i => video_on_i,
            data_o     => tmds_r_s);

    tmds_encoder_green_channel : entity work.tmds_encoder
        port map (
            clk_i      => clk_parallel_i,
            data_i     => rgb_i.g,
            control_i  => (others => '0'),
            video_on_i => video_on_i,
            data_o     => tmds_g_s);

    -- The blue channel does the H/V Sync signaling
    tmds_encoder_blue_channel : entity work.tmds_encoder
        port map (
            clk_i        => clk_parallel_i,
            data_i       => rgb_i.b,
            control_i(0) => sync_i.h_sync,
            control_i(1) => sync_i.v_sync,
            video_on_i   => video_on_i,
            data_o       => tmds_b_s);


    -- Serialization (10-bit TMDS to high-speed serial line)
    --------------------------------------------------------

    serializer_red_channel : entity work.serializer_10_to_1
        port map (
            clk_parallel_i     => clk_parallel_i,
            clk_serial_i       => clk_serial_i,
            rst_i              => rst_i,
            tmds_data_i        => tmds_r_s,
            tmds_serial_data_o => hdmi_tx_o.r);

    serializer_green_channel : entity work.serializer_10_to_1
        port map (
            clk_parallel_i     => clk_parallel_i,
            clk_serial_i       => clk_serial_i,
            rst_i              => rst_i,
            tmds_data_i        => tmds_g_s,
            tmds_serial_data_o => hdmi_tx_o.g);

    serializer_blue_channel : entity work.serializer_10_to_1
        port map (
            clk_parallel_i     => clk_parallel_i,
            clk_serial_i       => clk_serial_i,
            rst_i              => rst_i,
            tmds_data_i        => tmds_b_s,
            tmds_serial_data_o => hdmi_tx_o.b);

    -- The clock is generated from a constant (that will be encoded as transitions)
    serializer_clock : entity work.serializer_10_to_1
        port map (
            clk_parallel_i     => clk_parallel_i,
            clk_serial_i       => clk_serial_i,
            rst_i              => rst_i,
            tmds_data_i        => TMDS_ENCODED_CLOCK_C,
            tmds_serial_data_o => hdmi_tx_o.clk);

    hdmi_tx_o.scl <= '1'; -- Unused I2C Clock

end architecture struct;
