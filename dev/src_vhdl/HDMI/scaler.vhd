-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : scaler.vhd
-- Description  :
--
-- Author       : Rick Wertenbroek
-- Version      : 0.0
--
-- Dependencies :
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.hdmi_pkg.all;

entity scaler is
    generic (
        SCALE_3x3_EN        : boolean := true);
    port (
        reset_i             : in  std_logic;
        clk_pixel_i         : in  std_logic;
        video_on_i          : in  std_logic;
        current_pos_i       : in  max_position_t;
        debug_pixel_i       : in  rgb_t;
        frame_fifo_data_i   : in  std_logic_vector(15 downto 0);
        frame_fifo_empty_i  : in  std_logic;
        frame_fifo_read_o   : out std_logic;
        pixel_o             : out rgb_t);
end entity scaler;

architecture rtl of scaler is
    ---------------
    -- Constants --
    ---------------
    constant red_c   : rgb_t := (r => (others => '1'), others => (others => '0'));
    constant black_c : rgb_t := (others => (others => '0'));

    -------------
    -- Signals --
    -------------
    signal video_on_s    : std_logic;
    signal debug_pixel_s : rgb_t;
    signal current_pos_s : max_position_t;
    signal rgb_s         : rgb_t;

    ---------------
    -- Functions --
    ---------------
    type color_correction_matrix_t is array (0 to 31) of std_logic_vector(7 downto 0);

    -- Generate the 32 value color correction matrix to go from 5-bit to 8-bit
    function color_correction_matrix return color_correction_matrix_t is
        variable result_v : color_correction_matrix_t;
    begin
        for i in 0 to 31 loop
            result_v(i) := std_logic_vector(to_unsigned((i * 255) / 31, 8));
        end loop;

        return result_v;
    end function color_correction_matrix;

    constant color_correction_matrix_c : color_correction_matrix_t := color_correction_matrix;

    function frame_buffer_pixel_to_rgb(pix_i : std_logic_vector(14 downto 0)) return rgb_t is
        variable rgb_v : rgb_t;
    begin

        -- Frame buffer contains pixels as RGB555 they need to be converted to
        -- RGB888 before being displayed.
        rgb_v.r := color_correction_matrix_c(to_integer(unsigned(pix_i(14 downto 10))));
        rgb_v.g := color_correction_matrix_c(to_integer(unsigned(pix_i(9 downto 5))));
        rgb_v.b := color_correction_matrix_c(to_integer(unsigned(pix_i(4 downto 0))));

        return rgb_v;
    end function frame_buffer_pixel_to_rgb;

    ----------------
    -- Components --
    ----------------
    component blk_mem_scaling
        port (
            clka  : in  std_logic;
            wea   : in  std_logic_vector(0 downto 0);
            addra : in  std_logic_vector(7 downto 0);
            dina  : in  std_logic_vector(14 downto 0);
            clkb  : in  std_logic;
            addrb : in  std_logic_vector(7 downto 0);
            doutb : out std_logic_vector(14 downto 0));
    end component;

begin

    video_on_s    <= video_on_i;
    debug_pixel_s <= debug_pixel_i;
    current_pos_s <= current_pos_i;

    scaling_generate : if SCALE_3x3_EN generate

        scaling_block : block
            signal allow_read_fifo_s  : std_logic;
            signal delay_counter_s    : unsigned(2 downto 0);
            signal write_scale_bram_s : std_logic;
            signal scale_counter_s    : unsigned(1 downto 0);
            signal scaled_pixel_s     : std_logic_vector(14 downto 0);
            signal scaled_addr_s      : unsigned(7 downto 0);
            signal scale_counter_x_s  : unsigned(1 downto 0);
        begin

            -- Some delay before starting to read the FIFO, this should be
            -- redone and use a signal from the VDMA.
            process(clk_pixel_i) is
            begin
                if rising_edge(clk_pixel_i) then
                    if reset_i = '1' then
                        delay_counter_s   <= (others => '0');
                        allow_read_fifo_s <= '0';
                    else
                        if (current_pos_s.x = 1919) and (current_pos_s.y = 1079) then
                            delay_counter_s <= delay_counter_s + 1;
                        end if;

                        if delay_counter_s = 7 then
                            allow_read_fifo_s <= '1';
                        end if;
                    end if;
                end if;
            end process;

            -- Scale Counter
            scale_counter_process : process(clk_pixel_i) is
            begin
                if rising_edge(clk_pixel_i) then
                    if reset_i = '1' then
                        scale_counter_s <= (others => '0');
                    else
                        -- TODO : replace Magic
                        if (current_pos_s.x = 1900) and (current_pos_s.y < 3*320) and (video_on_s = '1') then
                            if scale_counter_s = 2 then -- TODO : replace Magic
                                scale_counter_s <= (others => '0');
                            else
                                scale_counter_s <= scale_counter_s + 1;
                            end if;
                        end if;
                    end if;
                end if;
            end process scale_counter_process;

            write_scale_bram_s <= '1' when (scale_counter_s = 0) and (current_pos_s.x < 240) and (current_pos_s.y < 3*320) and (video_on_s = '1') else
                                  '0';

            scale_addr_process : process(clk_pixel_i) is
            begin
                if rising_edge(clk_pixel_i) then
                    if reset_i = '1' then
                        scale_counter_x_s <= (others => '0');
                        scaled_addr_s     <= (others => '0');
                    else
                        if (current_pos_s.x >= 600) and (current_pos_s.x < (600+3*240)) and (video_on_s = '1') then
                            if scale_counter_x_s = 2 then
                                scale_counter_x_s <= (others => '0');
                            else
                                scale_counter_x_s <= scale_counter_x_s + 1;
                            end if;
                        end if;

                        if (scale_counter_x_s = 1) and (video_on_s = '1') then
                            if scaled_addr_s = 239 then
                                scaled_addr_s <= (others => '0');
                            else
                                scaled_addr_s <= scaled_addr_s + 1;
                            end if;
                        end if;
                    end if;
                end if;
            end process scale_addr_process;

            scaling_bram : blk_mem_scaling
                port map (
                    clka  => clk_pixel_i,
                    wea(0)=> write_scale_bram_s,
                    addra => std_logic_vector(current_pos_s.x(7 downto 0)),
                    dina  => frame_fifo_data_i(14 downto 0),
                    clkb  => clk_pixel_i,
                    addrb => std_logic_vector(scaled_addr_s),
                    doutb => scaled_pixel_s);


            rgb_s <= --debug_pixel_s when (current_pos_s.x < 64) and ((current_pos_s.y < 10) or
                     --                                               ((current_pos_s.y > 15) and (current_pos_s.y < 26)) or
                     --                                               ((current_pos_s.y > 31) and (current_pos_s.y < 42)) or
                     --                                               ((current_pos_s.y > 47) and (current_pos_s.y < 58))) else
                     frame_buffer_pixel_to_rgb(scaled_pixel_s(14 downto 0)) when (current_pos_s.x >= 600) and (current_pos_s.x < 1320) and (current_pos_s.y < 960) else
                     black_c;

            frame_fifo_read_o <= '1' when (write_scale_bram_s = '1') and (frame_fifo_empty_i = '0') and (allow_read_fifo_s = '1') else
                                 '0';

        end block scaling_block;

    else generate

        -- If debug zone show debug else frame buffer pixel value to RGB
        -- 65 instead of 64 because there is a 1 clock cycle delay
        rgb_s <= debug_pixel_s when (current_pos_s.x < 64) and ((current_pos_s.y < 10) or
                                                                ((current_pos_s.y > 15) and (current_pos_s.y < 26)) or
                                                                ((current_pos_s.y > 31) and (current_pos_s.y < 42)) or
                                                                ((current_pos_s.y > 47) and (current_pos_s.y < 58))) else
                 --frame_buffer_pixel_to_rgb(frame_fifo_data_i(14 downto 0)) when (current_pos_s.x >= 320) and (current_pos_s.y < 240) else
                 frame_buffer_pixel_to_rgb(frame_fifo_data_i(14 downto 0)) when (current_pos_s.x >= 200) and (current_pos_s.x < 440) and (current_pos_s.y < 320) else
                 black_c;

        -- Read when you draw a pixel and the FIFO is not empty and the video is on.
        --frame_fifo_read_o <= '1' when (current_pos_s.x >= 320) and (current_pos_s.y < 240) and (frame_fifo_empty_i = '0') and (video_on_s = '1') else
        frame_fifo_read_o <= '1' when (current_pos_s.x >= 200) and (current_pos_s.x < 440) and (current_pos_s.y < 320) and (frame_fifo_empty_i = '0') and (video_on_s = '1') else
                             '0';

    end generate scaling_generate;

    -------------
    -- Outputs --
    -------------
    pixel_o <= rgb_s;

end architecture rtl;
