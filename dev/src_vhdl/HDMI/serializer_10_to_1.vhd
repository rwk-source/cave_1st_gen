-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : serializer_10_to_1.vhd
-- Description  : A high speed 10 to 1 serilizer, it uses built-in OSERDESE2
--                components (unisim lib). Two of them since they are 8-bit and
--                we need to serialize 10 bits.
--
-- Author       : Rick Wertenbroek
-- Version      : 1.0
--
-- Dependencies : unisim lib, OSERDESE2 (Xilinx specific)
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity serializer_10_to_1 is
    port (
        clk_parallel_i     : in  std_logic; -- Should be 5x the serial Clock
        clk_serial_i       : in  std_logic; -- since it is DDR
        rst_i              : in  std_logic;
        tmds_data_i        : in  std_logic_vector(9 downto 0);
        tmds_serial_data_o : out std_logic);

end entity serializer_10_to_1;

architecture struct of serializer_10_to_1 is

    ---------------
    -- Constants --
    ---------------
    constant TMDS_PARALLEL_DATA_WIDTH_C : natural := tmds_data_i'length;

    -------------
    -- Signals --
    -------------
    signal tmds_serial_data_s : std_logic;

    -- To link the two serializers (Master / Slave)
    signal shift_1_s          : std_logic;
    signal shift_2_s          : std_logic;

begin


    Master_OSERDESE2 : OSERDESE2
        generic map (
            DATA_RATE_OQ   => "DDR",                      -- DDR, SDR
            DATA_RATE_TQ   => "DDR",                      -- DDR, BUF, SDR
            DATA_WIDTH     => TMDS_PARALLEL_DATA_WIDTH_C, -- Parallel data width (2-8,10,14)
            INIT_OQ        => '1',                        -- Initial value of OQ output (1'b0,1'b1)
            INIT_TQ        => '1',                        -- Initial value of TQ output (1'b0,1'b1)
            SERDES_MODE    => "MASTER",                   -- MASTER, SLAVE
            SRVAL_OQ       => '0',                        -- OQ output value when SR is used (1'b0,1'b1)
            SRVAL_TQ       => '0',                        -- TQ output value when SR is used (1'b0,1'b1)
            TBYTE_CTL      => "FALSE",                    -- Enable tristate byte operation (FALSE, TRUE)
            TBYTE_SRC      => "FALSE",                    -- Tristate byte source (FALSE, TRUE)
            TRISTATE_WIDTH => 1)                          -- 3-state converter width (1,4)
        port map (
            OFB       => open,                            -- 1-bit output: Feedback path for data
            OQ        => tmds_serial_data_s,              -- 1-bit output: Data path output
            -- SHIFTOUT1 / SHIFTOUT2: 1-bit (each) output: Data output expansion (1-bit each)
            SHIFTOUT1 => open,
            SHIFTOUT2 => open,
            TBYTEOUT  => open,                            -- 1-bit output: Byte group tristate
            TFB       => open,                            -- 1-bit output: 3-state control
            TQ        => open,                            -- 1-bit output: 3-state control
            CLK       => clk_serial_i,                    -- 1-bit input: High speed clock
            CLKDIV    => clk_parallel_i,                  -- 1-bit input: Divided clock
            -- D1 - D8: 1-bit (each) input: Parallel data inputs (1-bit each)
            D1        => tmds_data_i(0),
            D2        => tmds_data_i(1),
            D3        => tmds_data_i(2),
            D4        => tmds_data_i(3),
            D5        => tmds_data_i(4),
            D6        => tmds_data_i(5),
            D7        => tmds_data_i(6),
            D8        => tmds_data_i(7),
            OCE       => '1',                             -- 1-bit input: Output data clock enable
            RST       => rst_i,                           -- 1-bit input: Reset
            -- SHIFTIN1 / SHIFTIN2: 1-bit (each) input: Data input expansion (1-bit each)
            SHIFTIN1  => shift_1_s,
            SHIFTIN2  => shift_2_s,
            -- T1 - T4: 1-bit (each) input: Parallel 3-state inputs
            T1        => '0',
            T2        => '0',
            T3        => '0',
            T4        => '0',
            TBYTEIN   => '0',                             -- 1-bit input: Byte group tristate
            TCE       => '0');                            -- 1-bit input: 3-state clock enable

    Slave_OSERDESE2 : OSERDESE2
        generic map (
            DATA_RATE_OQ   => "DDR",                      -- DDR, SDR
            DATA_RATE_TQ   => "DDR",                      -- DDR, BUF, SDR
            DATA_WIDTH     => TMDS_PARALLEL_DATA_WIDTH_C, -- Parallel data width (2-8,10,14)
            INIT_OQ        => '1',                        -- Initial value of OQ output (1'b0,1'b1)
            INIT_TQ        => '1',                        -- Initial value of TQ output (1'b0,1'b1)
            SERDES_MODE    => "SLAVE",                    -- MASTER, SLAVE
            SRVAL_OQ       => '0',                        -- OQ output value when SR is used (1'b0,1'b1)
            SRVAL_TQ       => '0',                        -- TQ output value when SR is used (1'b0,1'b1)
            TBYTE_CTL      => "FALSE",                    -- Enable tristate byte operation (FALSE, TRUE)
            TBYTE_SRC      => "FALSE",                    -- Tristate byte source (FALSE, TRUE)
            TRISTATE_WIDTH => 1)                          -- 3-state converter width (1,4)
        port map (
            OFB       => open,                            -- 1-bit output: Feedback path for data
            OQ        => open,                            -- 1-bit output: Data path output
            -- SHIFTOUT1 / SHIFTOUT2: 1-bit (each) output: Data output expansion (1-bit each)
            SHIFTOUT1 => shift_1_s,
            SHIFTOUT2 => shift_2_s,
            TBYTEOUT  => open,                            -- 1-bit output: Byte group tristate
            TFB       => open,                            -- 1-bit output: 3-state control
            TQ        => open,                            -- 1-bit output: 3-state control
            CLK       => clk_serial_i,                    -- 1-bit input: High speed clock
            CLKDIV    => clk_parallel_i,                  -- 1-bit input: Divided clock
            -- D1 - D8: 1-bit (each) input: Parallel data inputs (1-bit each)
            D1        => '0',
            D2        => '0',
            D3        => tmds_data_i(8),
            D4        => tmds_data_i(9),
            D5        => '0',
            D6        => '0',
            D7        => '0',
            D8        => '0',
            OCE       => '1',                             -- 1-bit input: Output data clock enable
            RST       => rst_i,                           -- 1-bit input: Reset
            -- SHIFTIN1 / SHIFTIN2: 1-bit (each) input: Data input expansion (1-bit each)
            SHIFTIN1  => '0',
            SHIFTIN2  => '0',
            -- T1 - T4: 1-bit (each) input: Parallel 3-state inputs
            T1        => '0',
            T2        => '0',
            T3        => '0',
            T4        => '0',
            TBYTEIN   => '0',                             -- 1-bit input: Byte group tristate
            TCE       => '0');                            -- 1-bit input: 3-state clock enable

    ------------
    -- Output --
    ------------
    tmds_serial_data_o <= tmds_serial_data_s;

end architecture struct;
