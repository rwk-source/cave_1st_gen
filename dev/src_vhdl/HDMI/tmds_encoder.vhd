-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : tmds_encoder.vhd
-- Description  : This is a TMDS encoder, it takes 8-bit data as input and
--                generates 10-bit TMDS output data. When the Video On signal
--                is low a control word is generated according to the Control
--                input. Created by reading the information on :
--                https://eewiki.net/pages/viewpage.action?pageId=36569119
--
-- Author       : Rick Wertenbroek
-- Version      : 1.0
--
-- Dependencies : -
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tmds_encoder is
    port (
        clk_i      : in  std_logic;
        data_i     : in  std_logic_vector(7 downto 0); -- Data to be encoded
        control_i  : in  std_logic_vector(1 downto 0); -- MSB is control bit C1
                                                       -- LSB is control bit C0
        video_on_i : in  std_logic;                    -- Data when 1 else Control
        data_o     : out std_logic_vector(9 downto 0)  -- Encoded data
        );
end entity tmds_encoder;

architecture rtl of tmds_encoder is

    -------------
    -- signals --
    -------------
    signal q_m_s              : std_logic_vector(8 downto 0);  -- Internal Data
    signal pop_count_data_s   : unsigned(3 downto 0);
    signal pop_count_q_m_s    : unsigned(3 downto 0);
    signal pop_count_2x_q_m_s : unsigned(4 downto 0);
    signal diff_count_q_m_s   : signed(4 downto 0);

    ---------------
    -- registers --
    ---------------
    signal disparity_reg_s : signed(4 downto 0);

    ---------------
    -- Functions --
    ---------------

    -- Returns the number of 1's in an 8-bit word
    function pop_count(data_i : std_logic_vector(7 downto 0)) return unsigned is
        variable result_v : unsigned(3 downto 0) := (others => '0');
    begin
        for i in data_i'range loop
            result_v := result_v + to_integer(unsigned(data_i(i downto i)));
        end loop;

        return result_v;
    end function pop_count;

begin

    -- Population Counts
    pop_count_data_s <= pop_count(data_i);
    pop_count_q_m_s  <= pop_count(q_m_s(7 downto 0));

    -- Difference of 1's and 0's in q_m
    pop_count_2x_q_m_s <= pop_count_q_m_s & '0';
    diff_count_q_m_s <= signed(pop_count_2x_q_m_s) - 8;

    -- Minimize Transitions
    minimize_transitions_process : process(all) is
    begin
        q_m_s(0) <= data_i(0);
        if (pop_count_data_s > 4) or ((pop_count_data_s = 4) and (data_i(0) = '0')) then
            for i in 1 to 7 loop
                q_m_s(i) <= q_m_s(i-1) xnor data_i(i);
            end loop;
            q_m_s(8) <= '0';
        else
            for i in 1 to 7 loop
                q_m_s(i) <= q_m_s(i-1) xor data_i(i);
            end loop;
            q_m_s(8) <= '1';
        end if;
    end process minimize_transitions_process;

    -- Registers (disparity update) and output register
    process(clk_i) is
    begin
        if rising_edge(clk_i) then
            if video_on_i = '1' then
                if (disparity_reg_s = 0) or (pop_count_q_m_s = 4) then
                    if q_m_s(8) = '1' then
                        data_o <= not q_m_s(8) & q_m_s(8) & not q_m_s(7 downto 0);
                        disparity_reg_s <= disparity_reg_s - diff_count_q_m_s;
                    else
                        data_o <= not q_m_s(8) & q_m_s(8 downto 0);
                        disparity_reg_s <= disparity_reg_s + diff_count_q_m_s;
                    end if;
                else
                    if ((disparity_reg_s > 0) and (pop_count_q_m_s > 4)) or ((disparity_reg_s < 0) and (pop_count_q_m_s < 4)) then
                        data_o <= '1' & q_m_s(8) & not q_m_s(7 downto 0);
                        if q_m_s(8) = '0' then
                            disparity_reg_s <= disparity_reg_s - diff_count_q_m_s;
                        else
                            disparity_reg_s <= disparity_reg_s - diff_count_q_m_s + 2;
                        end if;
                    else
                        data_o <= '0' & q_m_s(8 downto 0);
                        if q_m_s(8) = '0' then
                            disparity_reg_s <= disparity_reg_s + diff_count_q_m_s - 2;
                        else
                            disparity_reg_s <= disparity_reg_s + diff_count_q_m_s;
                        end if;
                    end if;
                end if;
            else
                -- Here video is off
                case control_i is
                    when "00" =>
                        data_o <= "1101010100";
                    when "01" =>
                        data_o <= "0010101011";
                    when "10" =>
                        data_o <= "0101010100";
                    when "11" =>
                        data_o <= "1010101011";
                    when others =>
                        null;
                end case;
                -- The words above are balanced so don't worry about sending
                -- only one in a loop when no video for channels that don't need
                -- control words specifically.

                disparity_reg_s <= (others => '0'); -- Reset disparity reg
            end if; -- Video On
        end if; -- Rising Edge Clock
    end process;

    -- Note : disparity_reg_s has no reset, since we don't know the initial
    -- charge on the line this value doesn't matter, also it will be reset when
    -- a control word is sent. The disparity will be minimized from then on.

end architecture rtl;
