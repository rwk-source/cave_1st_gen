library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.log_pkg.all;

entity axi_memory_burster_v1_0_M_AXI is
    generic (
        -- Users to add parameters here

        -- User parameters ends
        -- Do not modify the parameters beyond this line

        -- Burst Length. Supports 1, 2, 4, 8, 16, 32, 64, 128, 256 burst lengths
        C_M_AXI_BURST_LEN	: integer	:= 32;
        -- Thread ID Width
        C_M_AXI_ID_WIDTH	: integer	:= 1;
        -- Width of Address Bus
        C_M_AXI_ADDR_WIDTH	: integer	:= 32;
        -- Width of Data Bus
        C_M_AXI_DATA_WIDTH	: integer	:= 32;
        -- Width of User Write Address Bus
        C_M_AXI_AWUSER_WIDTH	: integer	:= 0;
        -- Width of User Read Address Bus
        C_M_AXI_ARUSER_WIDTH	: integer	:= 0;
        -- Width of User Write Data Bus
        C_M_AXI_WUSER_WIDTH	: integer	:= 0;
        -- Width of User Read Data Bus
        C_M_AXI_RUSER_WIDTH	: integer	:= 0;
        -- Width of User Response Bus
        C_M_AXI_BUSER_WIDTH	: integer	:= 0
	);
    port (
        -- Users to add ports here
        address_i     : in  std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        read_i        : in  std_logic;
        tiny_burst_i  : in  std_logic;
        data_valid_o  : out std_logic;
        burst_done_o  : out std_logic;
        data_o        : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- User ports ends
        -- Do not modify the ports beyond this line

        -- Global Clock Signal.
        M_AXI_ACLK    : in  std_logic;
        -- Global Reset Singal. This Signal is Active Low
        M_AXI_ARESETN : in  std_logic;
        -- Master Interface Write Address ID
        M_AXI_AWID    : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Interface Write Address
        M_AXI_AWADDR  : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        -- Burst length. The burst length gives the exact number of transfers in a burst
        M_AXI_AWLEN   : out std_logic_vector(7 downto 0);
        -- Burst size. This signal indicates the size of each transfer in the burst
        M_AXI_AWSIZE  : out std_logic_vector(2 downto 0);
        -- Burst type. The burst type and the size information,
        -- determine how the address for each transfer within the burst is calculated.
        M_AXI_AWBURST : out std_logic_vector(1 downto 0);
        -- Lock type. Provides additional information about the
        -- atomic characteristics of the transfer.
        M_AXI_AWLOCK  : out std_logic;
        -- Memory type. This signal indicates how transactions
        -- are required to progress through a system.
        M_AXI_AWCACHE : out std_logic_vector(3 downto 0);
        -- Protection type. This signal indicates the privilege
        -- and security level of the transaction, and whether
        -- the transaction is a data access or an instruction access.
        M_AXI_AWPROT  : out std_logic_vector(2 downto 0);
        -- Quality of Service, QoS identifier sent for each write transaction.
        M_AXI_AWQOS   : out std_logic_vector(3 downto 0);
        -- Optional User-defined signal in the write address channel.
        M_AXI_AWUSER  : out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
        -- Write address valid. This signal indicates that
        -- the channel is signaling valid write address and control information.
        M_AXI_AWVALID : out std_logic;
        -- Write address ready. This signal indicates that
        -- the slave is ready to accept an address and associated control signals
        M_AXI_AWREADY : in  std_logic;
        -- Master Interface Write Data.
        M_AXI_WDATA   : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- Write strobes. This signal indicates which byte
        -- lanes hold valid data. There is one write strobe
        -- bit for each eight bits of the write data bus.
        M_AXI_WSTRB   : out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
        -- Write last. This signal indicates the last transfer in a write burst.
        M_AXI_WLAST   : out std_logic;
        -- Optional User-defined signal in the write data channel.
        M_AXI_WUSER   : out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
        -- Write valid. This signal indicates that valid write
        -- data and strobes are available
        M_AXI_WVALID  : out std_logic;
        -- Write ready. This signal indicates that the slave
        -- can accept the write data.
        M_AXI_WREADY  : in  std_logic;
        -- Master Interface Write Response.
        M_AXI_BID     : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Write response. This signal indicates the status of the write transaction.
        M_AXI_BRESP   : in  std_logic_vector(1 downto 0);
        -- Optional User-defined signal in the write response channel
        M_AXI_BUSER   : in  std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
        -- Write response valid. This signal indicates that the
        -- channel is signaling a valid write response.
        M_AXI_BVALID  : in  std_logic;
        -- Response ready. This signal indicates that the master
        -- can accept a write response.
        M_AXI_BREADY  : out std_logic;
        -- Master Interface Read Address.
        M_AXI_ARID    : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Read address. This signal indicates the initial
        -- address of a read burst transaction.
        M_AXI_ARADDR  : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        -- Burst length. The burst length gives the exact number of transfers in a burst
        M_AXI_ARLEN   : out std_logic_vector(7 downto 0);
        -- Burst size. This signal indicates the size of each transfer in the burst
        M_AXI_ARSIZE  : out std_logic_vector(2 downto 0);
        -- Burst type. The burst type and the size information,
        -- determine how the address for each transfer within the burst is calculated.
        M_AXI_ARBURST : out std_logic_vector(1 downto 0);
        -- Lock type. Provides additional information about the
        -- atomic characteristics of the transfer.
        M_AXI_ARLOCK  : out std_logic;
        -- Memory type. This signal indicates how transactions
        -- are required to progress through a system.
        M_AXI_ARCACHE : out std_logic_vector(3 downto 0);
        -- Protection type. This signal indicates the privilege
        -- and security level of the transaction, and whether
        -- the transaction is a data access or an instruction access.
        M_AXI_ARPROT  : out std_logic_vector(2 downto 0);
        -- Quality of Service, QoS identifier sent for each read transaction
        M_AXI_ARQOS   : out std_logic_vector(3 downto 0);
        -- Optional User-defined signal in the read address channel.
        M_AXI_ARUSER  : out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
        -- Write address valid. This signal indicates that
        -- the channel is signaling valid read address and control information
        M_AXI_ARVALID : out std_logic;
        -- Read address ready. This signal indicates that
        -- the slave is ready to accept an address and associated control signals
        M_AXI_ARREADY : in  std_logic;
        -- Read ID tag. This signal is the identification tag
        -- for the read data group of signals generated by the slave.
        M_AXI_RID     : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Read Data
        M_AXI_RDATA   : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- Read response. This signal indicates the status of the read transfer
        M_AXI_RRESP   : in  std_logic_vector(1 downto 0);
        -- Read last. This signal indicates the last transfer in a read burst
        M_AXI_RLAST   : in  std_logic;
        -- Optional User-defined signal in the read address channel.
        M_AXI_RUSER   : in  std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
        -- Read valid. This signal indicates that the channel
        -- is signaling the required read data.
        M_AXI_RVALID  : in  std_logic;
        -- Read ready. This signal indicates that the master can
        -- accept the read data and response information.
        M_AXI_RREADY  : out std_logic
    );
end axi_memory_burster_v1_0_M_AXI;

architecture implementation of axi_memory_burster_v1_0_M_AXI is

    -- C_TRANSACTIONS_NUM is the width of the index counter for
    -- number of beats in a burst write or burst read transaction.
    constant C_TRANSACTIONS_NUM : integer := ilogup(C_M_AXI_BURST_LEN-1);

    subtype data_word_t is std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    type data_word_array_t is array (natural range <>) of data_word_t;

    -- AXI4FULL signals
    -- AXI4 internal temp signals
    signal axi_arvalid_s       : std_logic;
    signal axi_rready_s        : std_logic;
    -- User signals
    signal burst_read_active_s : std_logic;
    signal tiny_burst_s        : std_logic;
    signal address_s           : std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);

begin
    -- I/O Connections assignments

    -- I/O Connections. Write Address (AW)
    M_AXI_AWID       <= (others => '0');
    -- The AXI address is a concatenation of the target base address + active offset range
    M_AXI_AWADDR     <= (others => '0'); -- No writes
    -- Burst Length is number of transaction beats, minus 1
    M_AXI_AWLEN      <= (others => '0');
    -- Size should be C_M_AXI_DATA_WIDTH, in 2^SIZE bytes, otherwise narrow bursts are used
    M_AXI_AWSIZE     <= (others => '0');
    -- INCR burst type is usually used, except for keyhole bursts
    M_AXI_AWBURST    <= "01";
    M_AXI_AWLOCK     <= '0';
    -- Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache.
    M_AXI_AWCACHE    <= "0010";
    M_AXI_AWPROT     <= "000";
    M_AXI_AWQOS      <= x"0";
    M_AXI_AWUSER     <= (others => '1');
    M_AXI_AWVALID    <= '0';
    -- Write Data(W)
    M_AXI_WDATA      <= (others => '0');
    -- All bursts are complete and aligned in this example
    M_AXI_WSTRB      <= (others => '1');
    M_AXI_WLAST      <= '0';
    M_AXI_WUSER      <= (others => '0');
    M_AXI_WVALID     <= '0';
    -- Write Response (B)
    M_AXI_BREADY     <= '1';
    -- Read Address (AR)
    M_AXI_ARID       <= (others => '0');
    M_AXI_ARADDR     <= address_s(address_s'high downto 2) & "00";
    --M_AXI_ARADDR     <= address_i(address_i'high downto C_TRANSACTIONS_NUM + 2) & std_logic_vector(to_unsigned(0, C_TRANSACTIONS_NUM + 2)); -- We always transfer the word of the requested address if it is word aligned, this also ensures us we don't cross 4k boundaries
    -- Burst Length is number of transaction beats, minus 1
    M_AXI_ARLEN      <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN - 1, 8)) when tiny_burst_s = '0' else
                        std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN/2 - 1, 8));
    -- Size should be C_M_AXI_DATA_WIDTH, in 2^n bytes, otherwise narrow bursts are used
    M_AXI_ARSIZE     <= std_logic_vector(to_unsigned(ilogup((C_M_AXI_DATA_WIDTH/8)-1), 3));
    -- INCR burst type is usually used, except for keyhole bursts
    M_AXI_ARBURST    <= "01";
    M_AXI_ARLOCK     <= '0';
    -- Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache.
    M_AXI_ARCACHE    <= "0010";
    M_AXI_ARPROT     <= "000";
    M_AXI_ARQOS      <= x"0";
    M_AXI_ARUSER     <= (others => '1');
    M_AXI_ARVALID    <= axi_arvalid_s;
    -- Read and Read Response (R)
    M_AXI_RREADY     <= axi_rready_s;

    --------------------------------------------------------------------------------

    axi_rready_s     <= '1'; -- We are always ready

    -- Register the request info
    process(M_AXI_ACLK) is
    begin
        if rising_edge(M_AXI_ACLK) then
            if read_i = '1' then
                tiny_burst_s <= tiny_burst_i;
                address_s    <= address_i;
            end if;
        end if;
    end process;

    ------------------------------
    --Read Address Channel
    ------------------------------
    process(M_AXI_ACLK) is
    begin
        if rising_edge(M_AXI_ACLK) then
            if M_AXI_ARESETN = '0' then
                axi_arvalid_s <= '0';
            else
                if (axi_arvalid_s = '0' and read_i = '1' and burst_read_active_s = '0') then
                    axi_arvalid_s <= '1';
                elsif (M_AXI_ARREADY = '1' and axi_arvalid_s = '1') then
                    axi_arvalid_s <= '0';
                end if;
            end if;
        end if;
    end process;

    ----------------------------------
    --Read Data (and Response) Channel
    ----------------------------------

    -- burst_read_active signal is asserted when there is a burst write transaction
    -- is initiated by the assertion of start_single_burst_write. start_single_burst_read
    -- signal remains asserted until the burst read is accepted by the master
    process(M_AXI_ACLK)
    begin
        if rising_edge(M_AXI_ACLK) then
            if M_AXI_ARESETN = '0' then
                burst_read_active_s <= '0';
            else
                if (axi_arvalid_s = '0' and read_i = '1' and burst_read_active_s = '0') then
                    burst_read_active_s <= '1';
                elsif (M_AXI_RVALID = '1' and M_AXI_RLAST = '1') then
                    burst_read_active_s <= '0';
                end if;
            end if;
        end if;
    end process;

    -------------
    -- Outputs --
    -------------
    data_valid_o <= M_AXI_RVALID and axi_rready_s;
    data_o       <= M_AXI_RDATA;
    burst_done_o <= M_AXI_RVALID and M_AXI_RLAST;

end implementation;
