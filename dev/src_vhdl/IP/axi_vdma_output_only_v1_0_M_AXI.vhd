-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : axi_vdma_output_only_v1_0_M_AXI.vhd
-- Description  : This is an AXI Video DMA that reads a region via AXI of
--                320x240 pixels coded on 16-bits and outputs it to a an
--                asymmetric FIFO (32-bit to 16-bit). The region is pointed to
--                by address_i input and the hi_n_lo_addr_i bit will add a 2^18
--                offset to address_i. This therefore reads a dual frame buffer
--                of 2^19 bytes (512 kilo bytes).
--
--                The fifo_ready_i signal means that the FIFO can accomodate
--                256 32-bit words.
--
-- Author       : Rick Wertenbroek
-- Version      : 0.0
--
-- VHDL std     : 93 since Vivado IP integrator does not support VHDL-2008
-- Dependencies :
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.log_pkg.all;

entity axi_vdma_output_only_v1_0_M_AXI is
    generic (
        -- Users to add parameters here

        -- User parameters ends
        -- Do not modify the parameters beyond this line

        -- Thread ID Width
        C_M_AXI_ID_WIDTH           : integer          := 1;
        -- Width of Address Bus
        C_M_AXI_ADDR_WIDTH         : integer          := 32;
        -- Width of Data Bus
        C_M_AXI_DATA_WIDTH         : integer          := 32;
        -- Width of User Write Address Bus
        C_M_AXI_AWUSER_WIDTH       : integer          := 0;
        -- Width of User Read Address Bus
        C_M_AXI_ARUSER_WIDTH       : integer          := 0;
        -- Width of User Write Data Bus
        C_M_AXI_WUSER_WIDTH        : integer          := 0;
        -- Width of User Read Data Bus
        C_M_AXI_RUSER_WIDTH        : integer          := 0;
        -- Width of User Response Bus
        C_M_AXI_BUSER_WIDTH        : integer          := 0
        );
    port (
        -- Users to add ports here
        address_i      : in  std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        hi_n_lo_addr_i : in  std_logic;
        fifo_ready_i   : in  std_logic;
        data_o         : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        write_fifo_o   : out std_logic;
        -- User ports ends
        -- Do not modify the ports beyond this line

        -- Global Clock Signal.
        M_AXI_ACLK    : in  std_logic;
        -- Global Reset Singal. This Signal is Active Low
        M_AXI_ARESETN : in  std_logic;
        -- Master Interface Write Address ID
        M_AXI_AWID    : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Interface Write Address
        M_AXI_AWADDR  : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        -- Burst length. The burst length gives the exact number of transfers in a burst
        M_AXI_AWLEN   : out std_logic_vector(7 downto 0);
        -- Burst size. This signal indicates the size of each transfer in the burst
        M_AXI_AWSIZE  : out std_logic_vector(2 downto 0);
        -- Burst type. The burst type and the size information,
        -- determine how the address for each transfer within the burst is calculated.
        M_AXI_AWBURST : out std_logic_vector(1 downto 0);
        -- Lock type. Provides additional information about the
        -- atomic characteristics of the transfer.
        M_AXI_AWLOCK  : out std_logic;
        -- Memory type. This signal indicates how transactions
        -- are required to progress through a system.
        M_AXI_AWCACHE : out std_logic_vector(3 downto 0);
        -- Protection type. This signal indicates the privilege
        -- and security level of the transaction, and whether
        -- the transaction is a data access or an instruction access.
        M_AXI_AWPROT  : out std_logic_vector(2 downto 0);
        -- Quality of Service, QoS identifier sent for each write transaction.
        M_AXI_AWQOS   : out std_logic_vector(3 downto 0);
        -- Optional User-defined signal in the write address channel.
        M_AXI_AWUSER  : out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
        -- Write address valid. This signal indicates that
        -- the channel is signaling valid write address and control information.
        M_AXI_AWVALID : out std_logic;
        -- Write address ready. This signal indicates that
        -- the slave is ready to accept an address and associated control signals
        M_AXI_AWREADY : in  std_logic;
        -- Master Interface Write Data.
        M_AXI_WDATA   : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- Write strobes. This signal indicates which byte
        -- lanes hold valid data. There is one write strobe
        -- bit for each eight bits of the write data bus.
        M_AXI_WSTRB   : out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
        -- Write last. This signal indicates the last transfer in a write burst.
        M_AXI_WLAST   : out std_logic;
        -- Optional User-defined signal in the write data channel.
        M_AXI_WUSER   : out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
        -- Write valid. This signal indicates that valid write
        -- data and strobes are available
        M_AXI_WVALID  : out std_logic;
        -- Write ready. This signal indicates that the slave
        -- can accept the write data.
        M_AXI_WREADY  : in  std_logic;
        -- Master Interface Write Response.
        M_AXI_BID     : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Write response. This signal indicates the status of the write transaction.
        M_AXI_BRESP   : in  std_logic_vector(1 downto 0);
        -- Optional User-defined signal in the write response channel
        M_AXI_BUSER   : in  std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
        -- Write response valid. This signal indicates that the
        -- channel is signaling a valid write response.
        M_AXI_BVALID  : in  std_logic;
        -- Response ready. This signal indicates that the master
        -- can accept a write response.
        M_AXI_BREADY  : out std_logic;
        -- Master Interface Read Address.
        M_AXI_ARID    : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Read address. This signal indicates the initial
        -- address of a read burst transaction.
        M_AXI_ARADDR  : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        -- Burst length. The burst length gives the exact number of transfers in a burst
        M_AXI_ARLEN   : out std_logic_vector(7 downto 0);
        -- Burst size. This signal indicates the size of each transfer in the burst
        M_AXI_ARSIZE  : out std_logic_vector(2 downto 0);
        -- Burst type. The burst type and the size information,
        -- determine how the address for each transfer within the burst is calculated.
        M_AXI_ARBURST : out std_logic_vector(1 downto 0);
        -- Lock type. Provides additional information about the
        -- atomic characteristics of the transfer.
        M_AXI_ARLOCK  : out std_logic;
        -- Memory type. This signal indicates how transactions
        -- are required to progress through a system.
        M_AXI_ARCACHE : out std_logic_vector(3 downto 0);
        -- Protection type. This signal indicates the privilege
        -- and security level of the transaction, and whether
        -- the transaction is a data access or an instruction access.
        M_AXI_ARPROT  : out std_logic_vector(2 downto 0);
        -- Quality of Service, QoS identifier sent for each read transaction
        M_AXI_ARQOS   : out std_logic_vector(3 downto 0);
        -- Optional User-defined signal in the read address channel.
        M_AXI_ARUSER  : out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
        -- Write address valid. This signal indicates that
        -- the channel is signaling valid read address and control information
        M_AXI_ARVALID : out std_logic;
        -- Read address ready. This signal indicates that
        -- the slave is ready to accept an address and associated control signals
        M_AXI_ARREADY : in  std_logic;
        -- Read ID tag. This signal is the identification tag
        -- for the read data group of signals generated by the slave.
        M_AXI_RID     : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        -- Master Read Data
        M_AXI_RDATA   : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- Read response. This signal indicates the status of the read transfer
        M_AXI_RRESP   : in  std_logic_vector(1 downto 0);
        -- Read last. This signal indicates the last transfer in a read burst
        M_AXI_RLAST   : in  std_logic;
        -- Optional User-defined signal in the read address channel.
        M_AXI_RUSER   : in  std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
        -- Read valid. This signal indicates that the channel
        -- is signaling the required read data.
        M_AXI_RVALID  : in  std_logic;
        -- Read ready. This signal indicates that the master can
        -- accept the read data and response information.
        M_AXI_RREADY  : out std_logic
        );
end axi_vdma_output_only_v1_0_M_AXI;

architecture implementation of axi_vdma_output_only_v1_0_M_AXI is

    -- Burst Length. Supports 1, 2, 4, 8, 16, 32, 64, 128, 256 burst lengths
    constant C_M_AXI_BURST_LEN  : integer := 256;

    -- C_TRANSACTIONS_NUM is the width of the index counter for
    -- number of beats in a burst write or burst read transaction.
    constant C_TRANSACTIONS_NUM : integer := ilogup(C_M_AXI_BURST_LEN-1);

    -- 150 since 320*240*16 = 150*256*32
    constant COUNT_MAX_C        : integer := 320*240*16/(C_M_AXI_BURST_LEN*C_M_AXI_DATA_WIDTH);
    -- Number of bits required to address a 320*240*2 (two bytes per pixel) frame buffer
    constant BITS_PER_FB_C      : integer := ilogup(320*240*2);

    subtype data_word_t is std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    type data_word_array_t is array (natural range <>) of data_word_t;

    -- AXI4FULL signals
    -- AXI4 internal temp signals
    signal axi_arvalid_s       : std_logic;
    signal axi_rready_s        : std_logic;
    -- User signals
    signal burst_read_active_s : std_logic;
    signal read_s              : std_logic;

    signal address_s           : std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal offset_s            : unsigned(C_M_AXI_ADDR_WIDTH-1 downto 0);

    signal count_s             : unsigned(ilogup(COUNT_MAX_C)-1 downto 0);
    signal hi_n_lo_addr_old_s  : std_logic;

begin
    -- I/O Connections assignments

    -- I/O Connections. Write Address (AW)
    M_AXI_AWID    <= (others => '0');
    -- The AXI address is a concatenation of the target base address + active offset range
    M_AXI_AWADDR  <= (others => '0');   -- No writes
    -- Burst Length is number of transaction beats, minus 1
    M_AXI_AWLEN   <= (others => '0');
    -- Size should be C_M_AXI_DATA_WIDTH, in 2^SIZE bytes, otherwise narrow bursts are used
    M_AXI_AWSIZE  <= (others => '0');
    -- INCR burst type is usually used, except for keyhole bursts
    M_AXI_AWBURST <= "01";
    M_AXI_AWLOCK  <= '0';
    -- Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache.
    M_AXI_AWCACHE <= "0010";
    M_AXI_AWPROT  <= "000";
    M_AXI_AWQOS   <= x"0";
    M_AXI_AWUSER  <= (others => '1');
    M_AXI_AWVALID <= '0';
    -- Write Data(W)
    M_AXI_WDATA   <= (others => '0');
    -- All bursts are complete and aligned in this example
    M_AXI_WSTRB   <= (others => '1');
    M_AXI_WLAST   <= '0';
    M_AXI_WUSER   <= (others => '0');
    M_AXI_WVALID  <= '0';
    -- Write Response (B)
    M_AXI_BREADY  <= '1';
    -- Read Address (AR)
    M_AXI_ARID    <= (others => '0');
    M_AXI_ARADDR  <= address_s(address_s'high downto C_TRANSACTIONS_NUM + 2) & std_logic_vector(to_unsigned(0, C_TRANSACTIONS_NUM + 2));  -- We always transfer the word of the requested address if it is word aligned, this also ensures us we don't cross 4k boundaries
    -- Burst Length is number of transaction beats, minus 1
    M_AXI_ARLEN   <= std_logic_vector(to_unsigned(C_M_AXI_BURST_LEN - 1, 8));
    -- Size should be C_M_AXI_DATA_WIDTH, in 2^n bytes, otherwise narrow bursts are used
    M_AXI_ARSIZE  <= std_logic_vector(to_unsigned(ilogup((C_M_AXI_DATA_WIDTH/8)-1), 3));
    -- INCR burst type is usually used, except for keyhole bursts
    M_AXI_ARBURST <= "01";
    M_AXI_ARLOCK  <= '0';
    -- Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache.
    M_AXI_ARCACHE <= "0010";
    M_AXI_ARPROT  <= "000";
    M_AXI_ARQOS   <= x"0";
    M_AXI_ARUSER  <= (others => '1');
    M_AXI_ARVALID <= axi_arvalid_s;
    -- Read and Read Response (R)
    M_AXI_RREADY  <= axi_rready_s;

    --------------------------------------------------------------------------------

    -- The offset for the next bursts depends on the hi_n_lo_addr (hi frame
    -- buffer, low frame buffer) and the current burst counter value.
    offset_s     <= resize(hi_n_lo_addr_i & count_s & to_unsigned(0, BITS_PER_FB_C-count_s'length), offset_s'length);
    -- The real address for the next burst read is the offset added to the base
    -- address of the memory region allocated by the processing system.
    address_s    <= std_logic_vector(unsigned(address_i) + offset_s);

    axi_rready_s <= '1'; -- We are always ready to receive data

    -- We only do a burst read if the count is not yet achieved and the fifo is
    -- ready to accept the same amount of data as the burst length
    --read_s       <= '1' when (count_s < COUNT_MAX_C) and (fifo_ready_i = '1') else
    --                '0';
    read_s       <= '1' when (fifo_ready_i = '1') else
                    '0';
    
    -------------
    -- Counter --
    -------------
    process(M_AXI_ACLK) is
    begin
        if rising_edge(M_AXI_ACLK) then
            hi_n_lo_addr_old_s <= hi_n_lo_addr_i;

            if M_AXI_ARESETN = '0' then
                count_s <= (others => '0');
            else
                ---- Reset when frame buffer toggles
                --if hi_n_lo_addr_old_s /= hi_n_lo_addr_i then
                --    count_s <= (others => '0');
                ---- Increment when burst read finished
                --elsif (M_AXI_RVALID = '1' and M_AXI_RLAST = '1') then
                --    count_s <= count_s + 1;
                --end if; -- Counter Behavior
                if (M_AXI_RVALID = '1') and (M_AXI_RLAST = '1') then
                    if count_s = (COUNT_MAX_C-1) then
                        count_s <= (others => '0');
                    else
                        count_s <= count_s + 1;
                    end if;
                end if;
            end if; -- Reset
        end if; -- Rising Edge Clock
    end process;

    ------------------------------
    --Read Address Channel
    ------------------------------
    process(M_AXI_ACLK) is
    begin
        if rising_edge(M_AXI_ACLK) then
            if M_AXI_ARESETN = '0' then
                axi_arvalid_s <= '0';
            else
                if (axi_arvalid_s = '0' and read_s = '1' and burst_read_active_s = '0') then
                    axi_arvalid_s <= '1';
                elsif (M_AXI_ARREADY = '1' and axi_arvalid_s = '1') then
                    axi_arvalid_s <= '0';
                end if;
            end if;
        end if;
    end process;

    ----------------------------------
    --Read Data (and Response) Channel
    ----------------------------------

    -- burst_read_active signal is asserted when there is a burst write transaction
    -- is initiated by the assertion of start_single_burst_write. start_single_burst_read
    -- signal remains asserted until the burst read is accepted by the master
    process(M_AXI_ACLK)
    begin
        if rising_edge(M_AXI_ACLK) then
            if M_AXI_ARESETN = '0' then
                burst_read_active_s <= '0';
            else
                if (axi_arvalid_s = '0' and read_s = '1' and burst_read_active_s = '0') then
                    burst_read_active_s <= '1';
                elsif (M_AXI_RVALID = '1' and M_AXI_RLAST = '1') then
                    burst_read_active_s <= '0';
                end if;
            end if;
        end if;
    end process;

    -------------
    -- Outputs --
    -------------
    write_fifo_o <= M_AXI_RVALID and axi_rready_s;
    data_o       <= M_AXI_RDATA;

end implementation;
