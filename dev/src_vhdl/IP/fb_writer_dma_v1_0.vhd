-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : fb_writer_dma_v1_0.vhd
-- Description  :
--
-- Author       : Rick Wertenbroek
-- Version      : 0.0
--
-- VHDL std     : 93
-- Dependencies :
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fb_writer_dma_v1_0 is
    generic (
        -- Users to add parameters here

        -- User parameters ends
        -- Do not modify the parameters beyond this line


        -- Parameters of Axi Master Bus Interface M_AXI
        C_M_AXI_ID_WIDTH     : integer := 1;
        C_M_AXI_ADDR_WIDTH   : integer := 32;
        C_M_AXI_DATA_WIDTH   : integer := 32;
        C_M_AXI_AWUSER_WIDTH : integer := 0;
        C_M_AXI_ARUSER_WIDTH : integer := 0;
        C_M_AXI_WUSER_WIDTH  : integer := 0;
        C_M_AXI_RUSER_WIDTH  : integer := 0;
        C_M_AXI_BUSER_WIDTH  : integer := 0
        );
    port (
        -- Users to add ports here
        address_i  : in  std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        transfer_i : in  std_logic;
        hi_nlo_i   : in  std_logic;
        busy_o     : out std_logic;
        fb_addr_o  : out std_logic_vector(15 downto 0);
        fb_data_i  : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        -- User ports ends
        -- Do not modify the ports beyond this line


        -- Ports of Axi Master Bus Interface M_AXI
        m_axi_aclk    : in  std_logic;
        m_axi_aresetn : in  std_logic;
        m_axi_awid    : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        m_axi_awaddr  : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        m_axi_awlen   : out std_logic_vector(7 downto 0);
        m_axi_awsize  : out std_logic_vector(2 downto 0);
        m_axi_awburst : out std_logic_vector(1 downto 0);
        m_axi_awlock  : out std_logic;
        m_axi_awcache : out std_logic_vector(3 downto 0);
        m_axi_awprot  : out std_logic_vector(2 downto 0);
        m_axi_awqos   : out std_logic_vector(3 downto 0);
        m_axi_awuser  : out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
        m_axi_awvalid : out std_logic;
        m_axi_awready : in  std_logic;
        m_axi_wdata   : out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        m_axi_wstrb   : out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
        m_axi_wlast   : out std_logic;
        m_axi_wuser   : out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
        m_axi_wvalid  : out std_logic;
        m_axi_wready  : in  std_logic;
        m_axi_bid     : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        m_axi_bresp   : in  std_logic_vector(1 downto 0);
        m_axi_buser   : in  std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
        m_axi_bvalid  : in  std_logic;
        m_axi_bready  : out std_logic;
        m_axi_arid    : out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        m_axi_araddr  : out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
        m_axi_arlen   : out std_logic_vector(7 downto 0);
        m_axi_arsize  : out std_logic_vector(2 downto 0);
        m_axi_arburst : out std_logic_vector(1 downto 0);
        m_axi_arlock  : out std_logic;
        m_axi_arcache : out std_logic_vector(3 downto 0);
        m_axi_arprot  : out std_logic_vector(2 downto 0);
        m_axi_arqos   : out std_logic_vector(3 downto 0);
        m_axi_aruser  : out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
        m_axi_arvalid : out std_logic;
        m_axi_arready : in  std_logic;
        m_axi_rid     : in  std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
        m_axi_rdata   : in  std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
        m_axi_rresp   : in  std_logic_vector(1 downto 0);
        m_axi_rlast   : in  std_logic;
        m_axi_ruser   : in  std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
        m_axi_rvalid  : in  std_logic;
        m_axi_rready  : out std_logic
        );
end fb_writer_dma_v1_0;

architecture arch_imp of fb_writer_dma_v1_0 is
    constant NUMBER_OF_BURSTS : natural := 150;
    constant BURST_LENGTH     : natural := 256;

    -- AXI4FULL signals
    --AXI4 internal temp signals
    signal axi_awaddr  : std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal axi_awvalid : std_logic;
    signal axi_wdata   : std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
    signal axi_wlast   : std_logic;
    signal axi_wvalid  : std_logic;
    signal axi_bready  : std_logic;
    signal axi_araddr  : std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
    signal axi_arvalid : std_logic;
    signal axi_rready  : std_logic;

    signal update_fb_counter_s : std_logic;

    type state_t is (IDLE, INIT_BURST_WRITE, BURST_WRITE);
    signal state_reg_s, state_next_s : state_t;

    signal burst_counter_s   : unsigned(7 downto 0); -- Need 150 bursts
    signal data_counter_s    : unsigned(7 downto 0); -- bust length is 256
    signal last_burst_beat_s : std_logic;
    signal last_burst_s      : std_logic;

    signal fb_counter, fb_counter_next : unsigned(15 downto 0);
begin

    -- I/O Connections assignments

    --I/O Connections. Write Address (AW)
    m_axi_awid    <= (others => '0');
    --The AXI address is a concatenation of the target base address + active offset range
    m_axi_awaddr  <= axi_awaddr;
    --Burst length is number of transaction beats, minus 1
    m_axi_awlen   <= std_logic_vector(to_unsigned(BURST_LENGTH-1, 8));
    --Size should be C_M_AXI_DATA_WIDTH, in 2^SIZE bytes, otherwise narrow bursts are used
    m_axi_awsize  <= "010"; -- 4 bytes
    --INCR burst type is usually used, except for keyhole bursts
    m_axi_awburst <= "01";
    m_axi_awlock  <= '0';
    --Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache.
    m_axi_awcache <= "0010";
    m_axi_awprot  <= "000";
    m_axi_awqos   <= x"0";
    m_axi_awuser  <= (others => '1');
    m_axi_awvalid <= axi_awvalid;
    --Write Data(W)
    m_axi_wdata   <= axi_wdata;
    --All bursts are complete and aligned in this example
    m_axi_wstrb   <= (others => '1');
    m_axi_wlast   <= axi_wlast;
    m_axi_wuser   <= (others => '0');
    m_axi_wvalid  <= axi_wvalid;
    --Write Response (B)
    m_axi_bready  <= axi_bready;
    --Read Address (AR)
    m_axi_arid    <= (others => '0');
    m_axi_araddr  <= (others => '0');
    --Burst length is number of transaction beats, minus 1
    m_axi_arlen   <= (others => '0');
    --Size should be C_M_AXI_DATA_WIDTH, in 2^n bytes, otherwise narrow bursts are used
    m_axi_arsize  <= "010";
    --INCR burst type is usually used, except for keyhole bursts
    m_axi_arburst <= "01";
    m_axi_arlock  <= '0';
    --Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache.
    m_axi_arcache <= "0010";
    m_axi_arprot  <= "000";
    m_axi_arqos   <= x"0";
    m_axi_aruser  <= (others => '1');
    m_axi_arvalid <= '0';
    --Read and Read Response (R)
    m_axi_rready  <= '1';

    -- FSM
    process(m_axi_aclk) is
    begin
        if rising_edge(m_axi_aclk) then
            if (m_axi_aresetn = '0') then
                state_reg_s <= IDLE;
            else
                state_reg_s <= state_next_s;
            end if;
        end if;
    end process;

    process(state_reg_s,
            transfer_i,
            m_axi_awready,
            last_burst_beat_s,
            last_burst_s) is
    begin
        state_next_s <= state_reg_s;

        case (state_reg_s) is
            when IDLE =>
                if (transfer_i = '1') then
                    state_next_s <= INIT_BURST_WRITE;
                end if;

            when INIT_BURST_WRITE =>
                if (m_axi_awready = '1') then
                    state_next_s <= BURST_WRITE;
                end if;

            when BURST_WRITE =>
                if (last_burst_beat_s = '1') then
                    if (last_burst_s = '1') then
                        state_next_s <= IDLE;
                    else
                        state_next_s <= INIT_BURST_WRITE;
                    end if;
                end if;
        end case;
    end process;

    -- The write request is valid when whe issue the burst write request
    axi_awvalid <= '1' when state_reg_s = INIT_BURST_WRITE else
                   '0';

    -- The data is always valid when we are transferring
    axi_wvalid  <= '1' when state_reg_s = BURST_WRITE else
                   '0';

    -- The last burst is when the counter is 149, this will be our 150'th burst
    last_burst_s <= '1' when burst_counter_s = (NUMBER_OF_BURSTS-1) else
                    '0';

    -- The last data of a burst transfer is when the counter is at BURST_LENGTH-1
    axi_wlast <= '1' when data_counter_s = (BURST_LENGTH-1) else
                 '0';

    axi_wdata <= fb_data_i;

    -- The last beat is when the slave also accepts it
    last_burst_beat_s <= '1' when (axi_wlast = '1') and (axi_wvalid = '1') and (m_axi_wready = '1') else
                         '0';

    -- Write Response Channel
    process(m_axi_aclk) is
    begin
        if rising_edge(m_axi_aclk) then
            if (m_axi_aresetn = '0') then
                axi_bready <= '0';
            else
                if (m_axi_bvalid = '1' and axi_bready = '0') then
                    -- accept/acknowledge bresp with axi_bready by the master
                    -- when M_AXI_BVALID is asserted by slave
                    axi_bready <= '1';
                elsif (axi_bready = '1') then
                    -- deassert after one clock cycle
                    axi_bready <= '0';
                end if;
            end if;
        end if;
    end process;

    address_generation_process : process(m_axi_aclk) is
    begin
        if rising_edge(m_axi_aclk) then
            if (state_reg_s = IDLE) then
                if (hi_nlo_i = '1') then
                    -- The dual frame buffer in RAM has two 512*256*2 bytes
                    -- regions (high and low)
                    axi_awaddr <= std_logic_vector(unsigned(address_i) + 512*256*2);
                else
                    axi_awaddr <= address_i;
                end if;
            else
                if ((state_reg_s = BURST_WRITE) and (state_next_s = INIT_BURST_WRITE)) then
                    -- Here we just transferred 256 x 4 bytes so we increment
                    -- the address by that value
                    axi_awaddr <= std_logic_vector(unsigned(axi_awaddr) + 256*4);
                end if;
            end if;
        end if;
    end process address_generation_process;

    burst_counter_process : process(m_axi_aclk) is
    begin
        if rising_edge(m_axi_aclk) then
            if (state_reg_s = IDLE) then
                burst_counter_s <= (others => '0');
            else
                if ((state_reg_s = BURST_WRITE) and (state_next_s = INIT_BURST_WRITE)) then
                    burst_counter_s <= burst_counter_s + 1;
                end if;
            end if;
        end if;
    end process burst_counter_process;

    data_counter_process : process(m_axi_aclk) is
    begin
        if rising_edge(m_axi_aclk) then
            if (state_next_s = INIT_BURST_WRITE) then
                data_counter_s <= (others => '0');
            else
                -- Increment the counter when data is transferred
                if ((axi_wvalid = '1') and (m_axi_wready = '1')) then
                    data_counter_s <= data_counter_s + 1;
                end if;
            end if;
        end if;
    end process data_counter_process;

    -- This process computes the next value of the frame buffer access counter
    fb_address_counter_next_process : process(fb_counter,
                                              state_reg_s,
                                              axi_wvalid,
                                              m_axi_wready) is
    begin
        -- Default value
        fb_counter_next <= fb_counter;

        if (state_reg_s = IDLE) then
            fb_counter_next <= (others => '0');
        else
            if (axi_wvalid = '1') and (m_axi_wready = '1') then
                fb_counter_next <= fb_counter + 1;
            end if;
        end if;
    end process fb_address_counter_next_process;

    -- The corresponding register
    fb_address_counter_process : process(m_axi_aclk) is
    begin
        if rising_edge(m_axi_aclk) then
            fb_counter <= fb_counter_next;
        end if;
    end process fb_address_counter_process;

    -------------
    -- Outputs --
    -------------

    -- The next values are used to counter the bram 1 clock cycle latency
    fb_addr_o <= std_logic_vector(fb_counter_next);

    -- If we are not IDLE we are busy
    busy_o <= '0' when state_reg_s = IDLE else
              '1';

end arch_imp;
