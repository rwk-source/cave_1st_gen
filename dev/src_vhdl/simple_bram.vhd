-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : simple_bram.vhd
-- Description  : This is a simple BRAM (if large enough else it will be
--                inferred as registers).
--
-- Author       : Rick Wertenbroek
-- Version      : 0.0
--
-- VHDL std     : 2008
-- Dependencies : log_pkg.vhd
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.log_pkg.all;

entity simple_bram is
    generic (
        WIDTH : natural := 8;
        DEPTH : natural := 1024
        );
    port (
        clk_i      : in  std_logic;
        write_en_i : in  std_logic;
        addr_i     : in  std_logic_vector(ilogup(DEPTH)-1 downto 0);
        data_i     : in  std_logic_vector(WIDTH-1 downto 0);
        data_o     : out std_logic_vector(WIDTH-1 downto 0)
        );
end entity simple_bram;

architecture rtl of simple_bram is

    -----------
    -- Types --
    -----------
    type bram_t is array (0 to DEPTH-1) of std_logic_vector(WIDTH-1 downto 0);

    -------------
    -- Signals --
    -------------
    signal bram_s : bram_t;

begin

    process(clk_i) is
    begin
        if rising_edge(clk_i) then
            if write_en_i = '1' then
                bram_s(to_integer(unsigned(addr_i))) <= data_i;
            end if;
            data_o <= bram_s(to_integer(unsigned(addr_i)));
        end if;
    end process;

end rtl;
