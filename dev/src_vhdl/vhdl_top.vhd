-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 Rick Wertenbroek <rick.wertenbroek@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- 2. Redistributions in binary form must reproduce the above copyright notice,
-- this list of conditions and the following disclaimer in the documentation
-- and/or other materials provided with the distribution.
--
-- 3. Neither the name of the copyright holder nor the names of its
-- contributors may be used to endorse or promote products derived from this
-- software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-------------------------------------------------------------------------------
-- File         : vhdl_top.vhd
-- Description  :
--
-- Author       : Rick Wertenbroek
-- Version      : 0.1
--
-- Dependencies : hdmi_pkg.vhd, bram, clk_mmcm, debug_display.vhd,
--                hdmi_out.vhd, dodonpachi_pkg.vhd
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.hdmi_pkg.all;
use work.dodonpachi_pkg.all;

entity vhdl_top is
    generic (
        USE_FAKE_FRAME_BUFFER : boolean := false
    );
    port (
        -- Base signals
        clk_i                     : in  std_logic;
        rst_i                     : in  std_logic;
        -- Joystick in
        joystick_inputs_i         : in  std_logic_vector(31 downto 0);
        -- VGA Initial Reset
        rst_vga_init_i            : in  std_logic;
        -- Control signals
        rst_68k_i                 : in  std_logic;
        enable_68k_i              : in  std_logic := '1';
        lock_step_68k_i           : in  std_logic := '0';
        step_68k_i                : in  std_logic := '0';
        break_point_addr_i        : in  std_logic_vector(31 downto 0) := (others => '0');
        break_point_enabled_i     : in  std_logic := '0';
        base_addr_prog_rom_i      : in  std_logic_vector(31 downto 0);
        base_addr_sprite_rom_i    : in  std_logic_vector(31 downto 0);
        base_addr_layer_rom_i     : in  std_logic_vector(31 downto 0);
        layer_processor_enabled_i : in  std_logic := '1';
        -- Debug
        tg68_addr_o               : out std_logic_vector(9 downto 0);
        tg68_pc_o                 : out std_logic_vector(31 downto 0);
        tg68_pcw_o                : out std_logic;
        clk_68k_o                 : out std_logic;
        -- Memory out interface
        rom_addr_68k_cache_o      : out std_logic_vector(31 downto 0);
        rom_read_68k_cache_o      : out std_logic;
        rom_valid_68k_cache_i     : in  std_logic;
        rom_data_68k_cache_i      : in  std_logic_vector(DDP_ROM_CACHE_LINE_WIDTH-1 downto 0);
        -- Graphics
        rom_addr_gfx_o            : out std_logic_vector(31 downto 0);
        tiny_burst_gfx_o          : out std_logic;
        rom_burst_read_gfx_o      : out std_logic;
        rom_data_gfx_i            : in  std_logic_vector(31 downto 0);
        rom_data_valid_gfx_i      : in  std_logic;
        rom_burst_done_gfx_i      : in  std_logic;
        -- Frame FIFO in
        frame_fifo_data_i         : in  std_logic_vector(DDP_WORD_WIDTH-1 downto 0);
        frame_fifo_empty_i        : in  std_logic;
        frame_fifo_read_o         : out std_logic;
        frame_fifo_clk_o          : out std_logic;
        draw_hi_n_lo_frame_o      : out std_logic;
        -- Frame buffer
        frame_buffer_dma_start_o  : out std_logic;
        frame_buffer_dma_busy_i   : in  std_logic;
        frame_buffer_hi_nlo_o     : out std_logic;
        frame_buffer_data_o       : out std_logic_vector(31 downto 0); -- For AXI
        frame_buffer_addr_i       : in  std_logic_vector(DDP_FRAME_BUFFER_ADDR_BITS-2 downto 0);
        -- HDMI out
        hdmi_tx_clk_o             : out std_logic;
        hdmi_tx_r_o               : out std_logic;
        hdmi_tx_g_o               : out std_logic;
        hdmi_tx_b_o               : out std_logic
        );
end entity vhdl_top;

architecture struct of vhdl_top is

    ---------------
    -- Constants --
    ---------------
    constant red_c   : rgb_t := (r => (others => '1'), others => (others => '0'));
    constant green_c : rgb_t := (g => (others => '1'), others => (others => '0'));
    constant black_c : rgb_t := (others => (others => '0'));
    constant HDMI_COUNTER_ATTR_C : counter_attr_t := HDMI_COUNTER_ATTR_1080P_PKG_C;

    ----------------
    -- Components --
    ----------------
    component clk_mmcm_vga is
        port (
            clk_out1 : out std_logic;
            clk_out2 : out std_logic;
            clk_in1  : in  std_logic);
    end component clk_mmcm_vga;

    component clk_mmcm_hdmi_1080 is
        port (
            clk_out1 : out std_logic;
            clk_out2 : out std_logic;
            clk_in1  : in  std_logic);
    end component clk_mmcm_hdmi_1080;

    component blk_mem_gen_0 is
        port (
            clka  : in  std_logic;
            wea   : in  std_logic_vector (0 to 0);
            addra : in  std_logic_vector (16 downto 0);
            dina  : in  std_logic_vector (14 downto 0);
            douta : out std_logic_vector (14 downto 0);
            clkb  : in  std_logic;
            web   : in  std_logic_vector (0 to 0);
            addrb : in  std_logic_vector (16 downto 0);
            dinb  : in  std_logic_vector (14 downto 0);
            doutb : out std_logic_vector (14 downto 0));
    end component blk_mem_gen_0;

    component frame_buffer_bram is
        port (
            clka  : in  std_logic;
            wea   : in  std_logic_vector(0 downto 0);
            addra : in  std_logic_vector(16 downto 0);
            dina  : in  std_logic_vector(14 downto 0);
            clkb  : in  std_logic;
            addrb : in  std_logic_vector(15 downto 0);
            doutb : out std_logic_vector(29 downto 0));
    end component frame_buffer_bram;

    -------------
    -- Signals --
    -------------
    signal frame_buffer_addr_s      : frame_buffer_addr_t;
    signal frame_buffer_data_s      : std_logic_vector(DDP_WORD_WIDTH-2 downto 0);
    signal frame_buffer_write_s     : std_logic;

    signal clk_hdmi_pixel_s         : std_logic;
    signal clk_hdmi_s               : std_logic;
    signal rst_vga_s                : std_logic;
    --signal rst_hdmi_s : std_logic;

    signal next_pix_pos_s           : max_position_t;
    signal current_pos_s            : max_position_t;
    -- Frame buffer encodes pixels as RGB555
    signal frame_buffer_video_out_s : std_logic_vector(14 downto 0);

    signal debug_pixel_s            : rgb_t;
    signal debug_pixel_2_s          : rgb_t;

    signal sync_s                   : sync_t;
    signal rgb_s                    : rgb_t;
    signal video_on_s               : std_logic;
    signal hdmi_tx_s                : hdmi_tx_out_t;

    signal frame_s                  : std_logic;

    -- Timing regs
    signal base_addr_s              : std_logic_vector(base_addr_prog_rom_i'length-1 downto 0);

    -- Debug
    signal mem_bus_68k_s            : std_logic_vector(31 downto 0);
    signal clk_68k_s                : std_logic;
    signal miss_counter_s           : std_logic_vector(31 downto 0);
    signal watch_bus_s              : watch_bus_t;
    signal command_bus_s            : command_bus_t;

    ---------------
    -- Functions --
    ---------------
    type color_correction_matrix_t is array (0 to 31) of std_logic_vector(7 downto 0);

    -- Generate the 32 value color correction matrix to go from 5-bit to 8-bit
    function color_correction_matrix return color_correction_matrix_t is
        variable result_v : color_correction_matrix_t;
    begin
        for i in 0 to 31 loop
            result_v(i) := std_logic_vector(to_unsigned((i * 255) / 31, 8));
        end loop;

        return result_v;
    end function color_correction_matrix;

    constant color_correction_matrix_c : color_correction_matrix_t := color_correction_matrix;

    function frame_buffer_pixel_to_rgb(pix_i : std_logic_vector(14 downto 0)) return rgb_t is
        variable rgb_v : rgb_t;
    begin

        -- Frame buffer contains pixels as RGB555 they need to be converted to
        -- RGB888 before being displayed.
        rgb_v.r := color_correction_matrix_c(to_integer(unsigned(pix_i(14 downto 10))));
        rgb_v.g := color_correction_matrix_c(to_integer(unsigned(pix_i(9 downto 5))));
        rgb_v.b := color_correction_matrix_c(to_integer(unsigned(pix_i(4 downto 0))));

        -- Dark correction (simple but wrong)
        --rgb_v.r := pix_i(14 downto 10) & "000";
        --rgb_v.g := pix_i(9  downto 5)  & "000";
        --rgb_v.b := pix_i(4  downto 0)  & "000";

        return rgb_v;
    end function frame_buffer_pixel_to_rgb;

begin

    ----------------------
    -- Clock generation --
    ----------------------

    ------------------------------------------------------------------------------
    --  Output     Output      Phase    Duty Cycle   Pk-to-Pk     Phase
    --   Clock     Freq (MHz)  (degrees)    (%)     Jitter (ps)  Error (ps)
    ------------------------------------------------------------------------------
    -- clk_out1___148.500______0.000______50.0______298.245____322.999
    -- clk_out2___742.500______0.000______50.0______232.529____322.999
    --
    ------------------------------------------------------------------------------
    -- Input Clock   Freq (MHz)    Input Jitter (UI)
    ------------------------------------------------------------------------------
    -- __primary_________100.000____________0.010
    clk_mmcm_hdmi_1080_1 : clk_mmcm_hdmi_1080 -- IP
        port map (
            clk_out1 => clk_hdmi_pixel_s, -- 148.5MHz (1080p)
            clk_out2 => clk_hdmi_s,       -- 742.5Mhz (5x) pray !
            clk_in1  => clk_i             -- 100MHz
            );

    --------------
    -- Game top --
    --------------

    dodonpachi_top_block : block
        signal rom_addr_68k_cache_s  : std_logic_vector(19 downto 0); -- TODO constant
        signal rom_addr_gfx_s        : std_logic_vector(31 downto 0); -- TODO constant
        signal tg68_pc_s             : std_logic_vector(31 downto 0); -- TODO constant
        signal tg68_pcw_s            : std_logic;
        signal pc_counter_s          : unsigned(tg68_addr_o'length downto 0);
    begin

        command_bus_s.layers_enabled <= layer_processor_enabled_i;

        dodonpachi_top_inst : entity work.dodonpachi_top
            port map (
                clk_i                 => clk_i,
                rst_i                 => rst_i,
                --
                rst_68k_i             => rst_68k_i,
                enable_68k_i          => enable_68k_i,
                --
                joystick_inputs_i     => joystick_inputs_i,
                --
                rom_addr_68k_cache_o  => rom_addr_68k_cache_s,
                rom_read_68k_cache_o  => rom_read_68k_cache_o,
                rom_valid_68k_cache_i => rom_valid_68k_cache_i,
                rom_data_68k_cache_i  => rom_data_68k_cache_i,
                --
                rom_addr_gfx_o        => rom_addr_gfx_s,
                tiny_burst_gfx_o      => tiny_burst_gfx_o,
                rom_burst_read_gfx_o  => rom_burst_read_gfx_o,
                rom_data_valid_gfx_i  => rom_data_valid_gfx_i,
                rom_data_gfx_i        => rom_data_gfx_i,
                rom_burst_done_gfx_i  => rom_burst_done_gfx_i,
                --
                frame_buffer_addr_o   => frame_buffer_addr_s,
                frame_buffer_data_o   => frame_buffer_data_s,
                frame_buffer_write_o  => frame_buffer_write_s,
                --
                --start_fb_dma_o        => open, --frame_buffer_dma_start_o,
                start_fb_dma_o        => frame_buffer_dma_start_o,
                --
                frame_i               => frame_s,
                --
                command_bus_i         => command_bus_s,
                lock_step_i           => lock_step_68k_i,
                step_i                => step_68k_i,
                mem_bus_68k_o         => mem_bus_68k_s,
                break_point_addr_i    => break_point_addr_i,
                break_point_enabled_i => break_point_enabled_i,
                --
                TG68_PC_o             => tg68_pc_s,
                TG68_PCW_o            => tg68_pcw_s,
                clk_68k_o             => clk_68k_s,
                miss_counter_o        => miss_counter_s,
                watch_bus_o           => watch_bus_s);

        --frame_buffer_dma_start_o <= '0';

        -- Address conversion
        rom_addr_68k_cache_o <= std_logic_vector(unsigned(base_addr_prog_rom_i) + unsigned(rom_addr_68k_cache_s));
        rom_addr_gfx_o       <= std_logic_vector(unsigned(base_addr_sprite_rom_i) + unsigned(rom_addr_gfx_s));

        -- This counter gives the execution trace address
        process (clk_68k_s) is
        begin
            if rising_edge(clk_68k_s) then
                if rst_68k_i = '1' then
                    pc_counter_s <= (others => '0');
                elsif (tg68_pcw_s = '1') then
                    pc_counter_s <= pc_counter_s + 1;
                end if;
            end if;
        end process;

        tg68_addr_o <= std_logic_vector(pc_counter_s(tg68_addr_o'length-1 downto 0));
        -- Use MSB bit to show which are the newest instructions
        tg68_pc_o   <= pc_counter_s(pc_counter_s'high) & tg68_pc_s(30 downto 0);
        tg68_pcw_o  <= tg68_pcw_s;
        clk_68k_o   <= clk_68k_s;

    end block dodonpachi_top_block;

    ------------------
    -- Frame Buffer --
    ------------------

    frame_buffer_block : block
        signal doutb_s                              : std_logic_vector(29 downto 0);
        signal frame_buffer_addr_new_geometry_reg_s : std_logic_vector(frame_buffer_addr_s'length-1 downto 0);
        signal frame_buffer_data_reg_s              : std_logic_vector(frame_buffer_data_s'length-1 downto 0);
        signal frame_buffer_write_reg_s             : std_logic;

        signal x_s           : unsigned(DDP_FRAME_BUFFER_ADDR_BITS_X-1 downto 0);
        signal reverse_x_s   : unsigned(DDP_FRAME_BUFFER_ADDR_BITS_X-1 downto 0);
        signal y_s           : unsigned(DDP_FRAME_BUFFER_ADDR_BITS_Y-1 downto 0);
        signal x_mult_s      : unsigned(16 downto 0);
        signal y_mult_s      : unsigned(16 downto 0);
        signal h_address_s   : unsigned(frame_buffer_addr_s'length-1 downto 0);
        signal v_address_s   : unsigned(frame_buffer_addr_s'length-1 downto 0);
    begin
        -- Framebuffer was 320x256 in order to have an aligned address with
        -- position [x,y] but is now converted to linear so the address is Y*320+X

        -- Convert the X & Y address to a linear one
        x_s           <= unsigned(frame_buffer_addr_s(frame_buffer_addr_s'high downto frame_buffer_addr_s'length-DDP_FRAME_BUFFER_ADDR_BITS_X));
        y_s           <= unsigned(frame_buffer_addr_s(DDP_FRAME_BUFFER_ADDR_BITS_Y-1 downto 0));
        reverse_x_s   <= to_unsigned(319,9) - x_s;
        x_mult_s      <= reverse_x_s * to_unsigned(240, 8);
        y_mult_s      <= y_s * to_unsigned(320, 9);
        h_address_s   <= y_mult_s + x_s;
        v_address_s   <= x_mult_s + y_s;

        -- Write requests are registered in order to have better timings
        process(clk_i) is
        begin
            if rising_edge(clk_i) then
                if rst_i = '1' then
                    frame_buffer_write_reg_s <= '0';
                else
                    frame_buffer_write_reg_s <= frame_buffer_write_s;
                end if;

                frame_buffer_data_reg_s <= frame_buffer_data_s;

                -- Horizontal (needs screen rotation)
                --frame_buffer_addr_new_geometry_reg_s <= std_logic_vector(h_address_s);
                -- Vertical
                frame_buffer_addr_new_geometry_reg_s <= std_logic_vector(v_address_s);
            end if;
        end process;

        frame_buffer_generate : if USE_FAKE_FRAME_BUFFER generate
            -- This is a fake frame buffer that ignores all writes and simply
            -- returns the address one cycle later upon read.
            fake_frame_buffer_inst : entity work.fake_frame_buffer
                port map (
                    clka   => clk_i,
                    wea(0) => frame_buffer_write_reg_s,
                    addra  => frame_buffer_addr_new_geometry_reg_s,
                    dina   => frame_buffer_data_reg_s,
                    clkb   => clk_i,
                    addrb  => frame_buffer_addr_i,
                    doutb  => doutb_s);
        else generate

            -- This is a temporary frame buffer that allows fast (single cycle) random
            -- access to any position. This is used so that the graphical pipeline can
            -- run at full speed. Once the image is generated it is transfered by DMA
            -- to the dual frame buffer in RAM.
            frame_buffer_bram_inst : frame_buffer_bram
                port map (
                    clka               => clk_i,
                    wea(0)             => frame_buffer_write_reg_s,
                    addra              => frame_buffer_addr_new_geometry_reg_s,
                    dina               => frame_buffer_data_reg_s,
                    clkb               => clk_i,
                    addrb              => frame_buffer_addr_i,
                    doutb              => doutb_s);
        end generate;

        -- Put the pixels two by two. Endianness can be changed here if needed
        --frame_buffer_data_o <= "0" & doutb_s(29 downto 15) & "0" & doutb_s(14 downto 0);
        frame_buffer_data_o <= "0" & doutb_s(14 downto 0) & "0" & doutb_s(29 downto 15);

    end block frame_buffer_block;

    -----------------------------
    -- 100MHz Clock Side Above --
    -----------------------------

    -- Some empty space

    --------------------------
    -- VGA Clock Side Below --
    --------------------------
    debug_display_block : block
        signal debug_values_s       : array_of_32b_vectors(3 downto 0);
        signal selected_debug_val_s : std_logic_vector(1 downto 0);
    begin

        process(clk_68k_s) is
        begin
            if rising_edge(clk_68k_s) then
                base_addr_s <= base_addr_prog_rom_i;
            end if;
        end process;

        debug_values_s(0) <= base_addr_s;
        debug_values_s(1) <= mem_bus_68k_s;
        debug_values_s(2) <= miss_counter_s;
        debug_values_s(3) <= x"1080" & watch_bus_s.ram_flags_1017a4;

        new_debug_display_1 : entity work.new_debug_display
            generic map (
                NUMBER_OF_REGS => 4)
            port map (
                value_clock_i => clk_68k_s,
                values_i      => debug_values_s,
                pixel_clock_i => clk_hdmi_pixel_s,
                show_val_i    => selected_debug_val_s,
                x_i           => std_logic_vector(next_pix_pos_s.x(5 downto 0)),
                y_i           => std_logic_vector(next_pix_pos_s.y(3 downto 0)),
                pixel_o       => debug_pixel_s);

        selected_debug_val_s <= "00" when (current_pos_s.y < 10) else
                                "01" when (current_pos_s.y < 26) else
                                "10" when (current_pos_s.y < 42) else
                                "11";

    end block debug_display_block;

    -- Scaler
    scaler_inst : entity work.scaler
        generic map (
            SCALE_3x3_EN => HDMI_COUNTER_ATTR_C.v_active_pixels >= 960)
        port map (
            reset_i            => rst_vga_init_i,
            clk_pixel_i        => clk_hdmi_pixel_s,
            video_on_i         => video_on_s,
            current_pos_i      => current_pos_s,
            debug_pixel_i      => debug_pixel_s,
            frame_fifo_data_i  => frame_fifo_data_i,
            frame_fifo_empty_i => frame_fifo_empty_i,
            frame_fifo_read_o  => frame_fifo_read_o,
            pixel_o            => rgb_s);

    -- The frame FIFO is sync'd to the HDMI pixel clock
    frame_fifo_clk_o <= clk_hdmi_pixel_s;

    -- Temporary block to alternate between frame buffers
    temp_hi_n_lo_frame_block : block
        signal old_video_on_reg_s        : std_logic;
        signal draw_hi_n_lo_frame_s      : std_logic;
        signal sync_draw_hi_n_lo_frame_s : std_logic;
    begin
        process(clk_hdmi_pixel_s) is
        begin
            if rising_edge(clk_hdmi_pixel_s) then
                old_video_on_reg_s <= video_on_s;
                if (old_video_on_reg_s = '1') and (video_on_s = '0') and (current_pos_s.y = HDMI_COUNTER_ATTR_C.v_active_pixels-1) then
                    draw_hi_n_lo_frame_s <= not draw_hi_n_lo_frame_s;
                end if;
            end if;
        end process;

        -- Timing related, clock domain crossing
        process(clk_i) is
        begin
            if rising_edge(clk_i) then
                sync_draw_hi_n_lo_frame_s <= draw_hi_n_lo_frame_s;
                draw_hi_n_lo_frame_o      <= sync_draw_hi_n_lo_frame_s;
                -- TODO : frame_s should maybe not cross that many clock domains,
                -- since it goes from clk_hdmi_pixel to clk_100Mhz to clk_68k ...
                -- it could go from clk_hdmi_pixel to clk_68k directly
                frame_s                   <= sync_draw_hi_n_lo_frame_s;
                frame_buffer_hi_nlo_o     <= not sync_draw_hi_n_lo_frame_s;
            end if;
        end process;
    end block;

    ----------
    -- HDMI --
    ----------
    hdmi_generic_out_inst : entity work.hdmi_generic_out
        generic map (
            COUNTER_ATTR_G      => HDMI_COUNTER_ATTR_C)
        port map (
            clk_hdmi_parallel_i => clk_hdmi_pixel_s,
            clk_hdmi_serial_i   => clk_hdmi_s,
            rst_i               => rst_vga_init_i,
            rgb_i               => rgb_s,
            next_pix_pos_o      => next_pix_pos_s,
            current_pos_o       => current_pos_s,
            video_on_o          => video_on_s,
            hdmi_tx_o           => hdmi_tx_s);

    -------------
    -- Outputs --
    -------------

    -- Single Ended, the differential buffers are in top.v (uppermost top level)
    hdmi_tx_clk_o <= hdmi_tx_s.clk;
    hdmi_tx_r_o   <= hdmi_tx_s.r;
    hdmi_tx_g_o   <= hdmi_tx_s.g;
    hdmi_tx_b_o   <= hdmi_tx_s.b;

end architecture struct;
