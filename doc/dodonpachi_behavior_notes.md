# Notes on the behaviour of Dodonpachi

## Initialization

The initialization sequence goes as follows

* Wait (NOP loop)
* RAM Init
   * Initialize Main RAM
   * Initialize Sprite RAM
   * Initialize Layer 0 RAM
   * Initialize Layer 1 RAM
   * Initialize Layer 2 RAM
* Wait (Decrement from 0xF000 to 0 loop)
* Jump into some subroutines, these seem to read the EEPROM (I believe to get information about the number of coins that where ever paid etc.). It also checks the input ports for both players (I believe to check if the test/coin1/coin2 buttons have been pressed).
* Set the video control registers
* ~~Goes into main program code (Here the cache gets a lot of misses compared to the initialization loops, which is normal, this also indicates a more elaborate code being run).~~
* Does a checksum on the program ROM, this is why there are a ton of cache misses. It reads from address `0x000000` to `0x0fffff`. A counter is used for the loop (from `0x7ffff` to `0`) because A0 is incremented twice this will span from `0x0 to 0x0fffff`. This can be seen in the assembly at address `0x0053c8` and beyond. It does this byte by byte using two sum counters D1 and D2. This is because the ROMs of Dodonpachi are on two 27c040 chips one for the high bytes, the other for the low bytes. So a checksum is computed for each one. The expected checksums are stored at `0x0052e2` and `0x0052e6` and are compared to the sums in D1 and D2. If the checksums are bad the code jumps at `0x0053f6`, this only sets a 1 at RAM address `0x100f6e` to indicate a corrupted ROM. Apparently it doesn't indicate which of the two program ROM chips is faulty, what a shame. If the checksums are OK the code continues at `0x0053fe`.
* The code will run until 0x571b2 where it waits for a bit to be set in a RAM location that is cleared by the routine, this means only the interrupt service routine can set that bît.

### Problems
The 68000 does use move.b instructions and my hardware does not account for byte accesses. This may be a problem, since I return 16bit words on the memory bus, I don't know if the CPU will mask it or if I am supposed to do that on the memory bus. ~~TODO: Check this~~ Done. Actually it seems ok, the memories are accessed with the same address and each time return the same 16bit word but the processor does select the upper or lower byte according to the parity of the address. So there are no unaligned accesses done. This was verified by running the checksum on the program ROM byte by byte, the resulting checksums (odd and even bytes) were both OK. However for byte writes my hardware needed to be fixed, the processor gives the address but one should ignore the LSB bit and use the LDS/UDS signals to select the word to write. The only thing that needs to be checked is that writing a byte to an even address (UDS active) is also shifted in the processor data out since we need to write the upper byte to memory (BigEndian).

### Notes

Note : Clear the cache counters around 1151 ms to check how it performs on "real" code. Cache is still above 99% efficient on real code. (99.29% on 200ms of "real" code), Even after the checksum (which visits the ROM linearly) the efficiency is over 99.4% !

Note : The checksum finishes around 3812 ms of simulation time.

Note : Video regs are written during all the initialization of the RAMs and during EEPROM/Input subroutines but not during the wait loops and stop being written after the video control regs have been set.

### Sprite RAM initialization

When the code starts running and the RAM has been initialized the Sprite RAM will be initialized. The sprite RAM is filled with `0x0000, 0x0000, 0x02A0, 0x02A0, 0x0101, 0x0000, 0x0000, 0x0000` and this pattern repeats. This is a sprite info line (8x16bits) check `sprite_info.md`. The fields that contain `0x02A0` are the `x_position` and `y_position`. For some reason Dodonpachi uses this pattern `0x02A0 = 0b0000001010100000` to indicate that there is no sprite at this line. This `0x0101` if the `tile_size_x:tile_size_y` parameters of the sprite. The last three fields are unuzed in Dodonpachi I believe. The two last fields are supposed to be `zoom_x` and `zoom_y` parameters. The whole Sprite RAM is set with those default parameters.

## Interrupts

There are two interrupts one video interrupt and one audio interrupt. The video interrupt is the V-blank and the audio interrupt is when the YMZ280b finished playing a sound ? [More info here](https://github.com/mamedev/mame/blob/master/src/mame/drivers/cave.cpp#L100)

## Others

### Sprite RAM size
The sprite RAM is spans from `0x400000` to `0x40ffff`.

I believe Dodonpachi only uses half of the Sprite RAM to draw actual sprites, `0x400000-0x403fff` for the first buffer and `0x404000-0x407fff` for the second buffer (1024 Sprite lines each). I tried drawing the contents of the addresses above `0x408000` however it doesn't seem to describe any sprites.

It alternates between the two buffers each frame, I believe there is a bit in the video registers that indicates which buffer should be drawn. This bit is at address `0x800008`.

writes to the other video registers seem to be to start the generate of a frame or to acknowledge something.
