# Dodonpachi Memory Map

[Extracted from MAME source code](https://github.com/mamedev/mame/blob/master/src/mame/drivers/cave.cpp#L515)

```
0x000000 - 0x0fffff | ROM           | 1MB   | Read-Only     
0x100000 - 0x10ffff | RAM           | 64kB  |               
0x300000 - 0x300003 | YMZ280b       | 2x16b |               
0x400000 - 0x40ffff | Sprite RAM    | 64kB  |               
0x500000 - 0x507fff | Layer 0 RAM   | 32kB  |               
0x600000 - 0x607fff | Layer 1 RAM   | 32kB  |               
0x700000 - 0x70ffff | Layer 2 RAM   | 64kB  |               
0x800000 - 0x80007f | Video Regs    | 128B  | Write-Only    
0x800000 - 0x800007 | IRQ Cause     | 4x16b | Read-Only     
0x900000 - 0x900005 | Vctrl 0       | 3x16b |               
0xa00000 - 0xa00005 | Vctrl 1       | 3x16b |               
0xb00000 - 0xb00005 | Vctrl 2       | 3x16b |               
0xc00000 - 0xc0ffff | Palette RAM   | 64kB  |               
0xd00000 - 0xd00001 | Inputs 0      | 16b   |               
0xd00000 - 0xd00003 | Inputs 1      | 16b   |               
0xe00000 - 0xe00001 | Serial EEPROM | 16b   | The 7 MSB bits are used : Coin lock-out1 | Coin lock-out2 | Coin Counter 1 | Coin Counter 0 | SDI | SCL | CS | unused bits

```